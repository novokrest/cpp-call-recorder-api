#ifndef CALL_RECORDER_API_MODELS_GetSettingsResponse_H_
#define CALL_RECORDER_API_MODELS_GetSettingsResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "App.h"
#include "GetSettingsResponseSettings.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetSettingsResponse;
typedef std::shared_ptr<GetSettingsResponse> GetSettingsResponsePtr;

class GetSettingsResponse : public Object
{
public:
    class Builder;

    GetSettingsResponse(
        const std::string& status,
        const App::Value& app,
        const long& credits,
        const GetSettingsResponseSettingsPtr& settings
    );
    GetSettingsResponse& operator=(const GetSettingsResponse& other);
    virtual ~GetSettingsResponse();

    const std::string& status() const;
    const App::Value& app() const;
    const long& credits() const;
    const GetSettingsResponseSettingsPtr& settings() const;

private:
    std::string m_status;
    App::Value m_app;
    long m_credits;
    GetSettingsResponseSettingsPtr m_settings;
};

class GetSettingsResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& app(const App::Value& app);
    Builder& credits(const long& credits);
    Builder& settings(const GetSettingsResponseSettingsPtr& settings);

    GetSettingsResponsePtr build() const;

private:
    std::string m_status;
    App::Value m_app;
    long m_credits;
    GetSettingsResponseSettingsPtr m_settings;
};

}
}
}
}

#endif
