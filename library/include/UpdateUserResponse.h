#ifndef CALL_RECORDER_API_MODELS_UpdateUserResponse_H_
#define CALL_RECORDER_API_MODELS_UpdateUserResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateUserResponse;
typedef std::shared_ptr<UpdateUserResponse> UpdateUserResponsePtr;

class UpdateUserResponse : public Object
{
public:
    class Builder;

    UpdateUserResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code
    );
    UpdateUserResponse& operator=(const UpdateUserResponse& other);
    virtual ~UpdateUserResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

class UpdateUserResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);

    UpdateUserResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

}
}
}
}

#endif
