#ifndef CALL_RECORDER_API_MODELS_DeleteMetaFilesResponse_H_
#define CALL_RECORDER_API_MODELS_DeleteMetaFilesResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class DeleteMetaFilesResponse;
typedef std::shared_ptr<DeleteMetaFilesResponse> DeleteMetaFilesResponsePtr;

class DeleteMetaFilesResponse : public Object
{
public:
    class Builder;

    DeleteMetaFilesResponse(
        const std::string& status,
        const std::string& msg
    );
    DeleteMetaFilesResponse& operator=(const DeleteMetaFilesResponse& other);
    virtual ~DeleteMetaFilesResponse();

    const std::string& status() const;
    const std::string& msg() const;

private:
    std::string m_status;
    std::string m_msg;
};

class DeleteMetaFilesResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);

    DeleteMetaFilesResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
};

}
}
}
}

#endif
