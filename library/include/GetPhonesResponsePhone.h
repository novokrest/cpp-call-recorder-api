#ifndef CALL_RECORDER_API_MODELS_GetPhonesResponsePhone_H_
#define CALL_RECORDER_API_MODELS_GetPhonesResponsePhone_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetPhonesResponsePhone;
typedef std::shared_ptr<GetPhonesResponsePhone> GetPhonesResponsePhonePtr;

class GetPhonesResponsePhone : public Object
{
public:
    class Builder;

    GetPhonesResponsePhone(
        const std::string& phoneNumber,
        const std::string& number,
        const std::string& prefix,
        const std::string& friendlyName,
        const std::string& flag,
        const std::string& city,
        const std::string& country
    );
    GetPhonesResponsePhone& operator=(const GetPhonesResponsePhone& other);
    virtual ~GetPhonesResponsePhone();

    const std::string& phoneNumber() const;
    const std::string& number() const;
    const std::string& prefix() const;
    const std::string& friendlyName() const;
    const std::string& flag() const;
    const std::string& city() const;
    const std::string& country() const;

private:
    std::string m_phoneNumber;
    std::string m_number;
    std::string m_prefix;
    std::string m_friendlyName;
    std::string m_flag;
    std::string m_city;
    std::string m_country;
};

class GetPhonesResponsePhone::Builder
{
public:
    Builder& phoneNumber(const std::string& phoneNumber);
    Builder& number(const std::string& number);
    Builder& prefix(const std::string& prefix);
    Builder& friendlyName(const std::string& friendlyName);
    Builder& flag(const std::string& flag);
    Builder& city(const std::string& city);
    Builder& country(const std::string& country);

    GetPhonesResponsePhonePtr build() const;

private:
    std::string m_phoneNumber;
    std::string m_number;
    std::string m_prefix;
    std::string m_friendlyName;
    std::string m_flag;
    std::string m_city;
    std::string m_country;
};

}
}
}
}

#endif
