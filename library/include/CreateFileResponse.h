#ifndef CALL_RECORDER_API_MODELS_CreateFileResponse_H_
#define CALL_RECORDER_API_MODELS_CreateFileResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class CreateFileResponse;
typedef std::shared_ptr<CreateFileResponse> CreateFileResponsePtr;

class CreateFileResponse : public Object
{
public:
    class Builder;

    CreateFileResponse(
        const std::string& status,
        const long& id,
        const std::string& msg
    );
    CreateFileResponse& operator=(const CreateFileResponse& other);
    virtual ~CreateFileResponse();

    const std::string& status() const;
    const long& id() const;
    const std::string& msg() const;

private:
    std::string m_status;
    long m_id;
    std::string m_msg;
};

class CreateFileResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& id(const long& id);
    Builder& msg(const std::string& msg);

    CreateFileResponsePtr build() const;

private:
    std::string m_status;
    long m_id;
    std::string m_msg;
};

}
}
}
}

#endif
