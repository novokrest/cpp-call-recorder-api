#ifndef CALL_RECORDER_API_MODELS_VerifyPhoneRequest_H_
#define CALL_RECORDER_API_MODELS_VerifyPhoneRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "App.h"
#include "DeviceType.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class VerifyPhoneRequest;
typedef std::shared_ptr<VerifyPhoneRequest> VerifyPhoneRequestPtr;

class VerifyPhoneRequest : public Object
{
public:
    class Builder;

    VerifyPhoneRequest(
        const std::string& token,
        const std::string& phone,
        const std::string& code,
        const std::string& mcc,
        const App::Value& app,
        const std::string& deviceToken,
        const std::string& deviceId,
        const DeviceType::Value& deviceType,
        const std::string& timeZone
    );
    VerifyPhoneRequest& operator=(const VerifyPhoneRequest& other);
    virtual ~VerifyPhoneRequest();

    const std::string& token() const;
    const std::string& phone() const;
    const std::string& code() const;
    const std::string& mcc() const;
    const App::Value& app() const;
    const std::string& deviceToken() const;
    const std::string& deviceId() const;
    const DeviceType::Value& deviceType() const;
    const std::string& timeZone() const;

private:
    std::string m_token;
    std::string m_phone;
    std::string m_code;
    std::string m_mcc;
    App::Value m_app;
    std::string m_deviceToken;
    std::string m_deviceId;
    DeviceType::Value m_deviceType;
    std::string m_timeZone;
};

class VerifyPhoneRequest::Builder
{
public:
    Builder& token(const std::string& token);
    Builder& phone(const std::string& phone);
    Builder& code(const std::string& code);
    Builder& mcc(const std::string& mcc);
    Builder& app(const App::Value& app);
    Builder& deviceToken(const std::string& deviceToken);
    Builder& deviceId(const std::string& deviceId);
    Builder& deviceType(const DeviceType::Value& deviceType);
    Builder& timeZone(const std::string& timeZone);

    VerifyPhoneRequestPtr build() const;

private:
    std::string m_token;
    std::string m_phone;
    std::string m_code;
    std::string m_mcc;
    App::Value m_app;
    std::string m_deviceToken;
    std::string m_deviceId;
    DeviceType::Value m_deviceType;
    std::string m_timeZone;
};

}
}
}
}

#endif
