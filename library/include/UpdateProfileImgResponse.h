#ifndef CALL_RECORDER_API_MODELS_UpdateProfileImgResponse_H_
#define CALL_RECORDER_API_MODELS_UpdateProfileImgResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateProfileImgResponse;
typedef std::shared_ptr<UpdateProfileImgResponse> UpdateProfileImgResponsePtr;

class UpdateProfileImgResponse : public Object
{
public:
    class Builder;

    UpdateProfileImgResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code,
        const std::string& file,
        const std::string& path
    );
    UpdateProfileImgResponse& operator=(const UpdateProfileImgResponse& other);
    virtual ~UpdateProfileImgResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;
    const std::string& file() const;
    const std::string& path() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
    std::string m_file;
    std::string m_path;
};

class UpdateProfileImgResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);
    Builder& file(const std::string& file);
    Builder& path(const std::string& path);

    UpdateProfileImgResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
    std::string m_file;
    std::string m_path;
};

}
}
}
}

#endif
