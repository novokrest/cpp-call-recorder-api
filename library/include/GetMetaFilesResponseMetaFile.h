#ifndef CALL_RECORDER_API_MODELS_GetMetaFilesResponseMetaFile_H_
#define CALL_RECORDER_API_MODELS_GetMetaFilesResponseMetaFile_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetMetaFilesResponseMetaFile;
typedef std::shared_ptr<GetMetaFilesResponseMetaFile> GetMetaFilesResponseMetaFilePtr;

class GetMetaFilesResponseMetaFile : public Object
{
public:
    class Builder;

    GetMetaFilesResponseMetaFile(
        const std::string& id,
        const std::string& parentId,
        const std::string& name,
        const std::string& file,
        const std::string& userId,
        const std::string& time
    );
    GetMetaFilesResponseMetaFile& operator=(const GetMetaFilesResponseMetaFile& other);
    virtual ~GetMetaFilesResponseMetaFile();

    const std::string& id() const;
    const std::string& parentId() const;
    const std::string& name() const;
    const std::string& file() const;
    const std::string& userId() const;
    const std::string& time() const;

private:
    std::string m_id;
    std::string m_parentId;
    std::string m_name;
    std::string m_file;
    std::string m_userId;
    std::string m_time;
};

class GetMetaFilesResponseMetaFile::Builder
{
public:
    Builder& id(const std::string& id);
    Builder& parentId(const std::string& parentId);
    Builder& name(const std::string& name);
    Builder& file(const std::string& file);
    Builder& userId(const std::string& userId);
    Builder& time(const std::string& time);

    GetMetaFilesResponseMetaFilePtr build() const;

private:
    std::string m_id;
    std::string m_parentId;
    std::string m_name;
    std::string m_file;
    std::string m_userId;
    std::string m_time;
};

}
}
}
}

#endif
