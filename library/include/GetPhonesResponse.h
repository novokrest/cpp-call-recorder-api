#ifndef CALL_RECORDER_API_MODELS_GetPhonesResponse_H_
#define CALL_RECORDER_API_MODELS_GetPhonesResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "GetPhonesResponsePhone.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetPhonesResponse;
typedef std::shared_ptr<GetPhonesResponse> GetPhonesResponsePtr;

class GetPhonesResponse : public Object
{
public:
    class Builder;

    GetPhonesResponse(const std::vector<GetPhonesResponsePhone>& phones);
    GetPhonesResponse& operator=(const GetPhonesResponse& other);
    virtual ~GetPhonesResponse();

    std::vector<GetPhonesResponsePhone> phones() const;

private:
    std::vector<GetPhonesResponsePhone> m_phones;
};

class GetPhonesResponse::Builder
{
public:
    Builder& phones(const std::vector<GetPhonesResponsePhone>& phones);

    GetPhonesResponsePtr build() const;

private:
    std::vector<GetPhonesResponsePhone> m_phones;
};

}
}
}
}

#endif
