#ifndef CALL_RECORDER_API_MODELS_UpdateStarResponse_H_
#define CALL_RECORDER_API_MODELS_UpdateStarResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateStarResponse;
typedef std::shared_ptr<UpdateStarResponse> UpdateStarResponsePtr;

class UpdateStarResponse : public Object
{
public:
    class Builder;

    UpdateStarResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code
    );
    UpdateStarResponse& operator=(const UpdateStarResponse& other);
    virtual ~UpdateStarResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

class UpdateStarResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);

    UpdateStarResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

}
}
}
}

#endif
