#ifndef CALL_RECORDER_API_MODELS_DeleteMetaFilesRequest_H_
#define CALL_RECORDER_API_MODELS_DeleteMetaFilesRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class DeleteMetaFilesRequest;
typedef std::shared_ptr<DeleteMetaFilesRequest> DeleteMetaFilesRequestPtr;

class DeleteMetaFilesRequest : public Object
{
public:
    class Builder;

    DeleteMetaFilesRequest(
        const std::string& apiKey,
        const std::vector<long>& ids,
        const long& parentId
    );
    DeleteMetaFilesRequest& operator=(const DeleteMetaFilesRequest& other);
    virtual ~DeleteMetaFilesRequest();

    const std::string& apiKey() const;
    const std::vector<long>& ids() const;
    const long& parentId() const;

private:
    std::string m_apiKey;
    std::vector<long> m_ids;
    long m_parentId;
};

class DeleteMetaFilesRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& ids(const std::vector<long>& ids);
    Builder& parentId(const long& parentId);

    DeleteMetaFilesRequestPtr build() const;

private:
    std::string m_apiKey;
    std::vector<long> m_ids;
    long m_parentId;
};

}
}
}
}

#endif
