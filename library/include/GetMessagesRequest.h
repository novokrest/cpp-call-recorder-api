#ifndef CALL_RECORDER_API_MODELS_GetMessagesRequest_H_
#define CALL_RECORDER_API_MODELS_GetMessagesRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetMessagesRequest;
typedef std::shared_ptr<GetMessagesRequest> GetMessagesRequestPtr;

class GetMessagesRequest : public Object
{
public:
    class Builder;

    GetMessagesRequest(
        const std::string& apiKey
    );
    GetMessagesRequest& operator=(const GetMessagesRequest& other);
    virtual ~GetMessagesRequest();

    const std::string& apiKey() const;

private:
    std::string m_apiKey;
};

class GetMessagesRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);

    GetMessagesRequestPtr build() const;

private:
    std::string m_apiKey;
};

}
}
}
}

#endif
