#ifndef CALL_RECORDER_API_MODELS_GetFoldersResponse_H_
#define CALL_RECORDER_API_MODELS_GetFoldersResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "GetFoldersResponseFolder.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetFoldersResponse;
typedef std::shared_ptr<GetFoldersResponse> GetFoldersResponsePtr;

class GetFoldersResponse : public Object
{
public:
    class Builder;

    GetFoldersResponse(
        const std::string& status,
        const std::string& msg,
        const std::vector<GetFoldersResponseFolder>& folders
    );
    GetFoldersResponse& operator=(const GetFoldersResponse& other);
    virtual ~GetFoldersResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::vector<GetFoldersResponseFolder>& folders() const;

private:
    std::string m_status;
    std::string m_msg;
    std::vector<GetFoldersResponseFolder> m_folders;
};

class GetFoldersResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& folders(const std::vector<GetFoldersResponseFolder>& folders);

    GetFoldersResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::vector<GetFoldersResponseFolder> m_folders;
};

}
}
}
}

#endif
