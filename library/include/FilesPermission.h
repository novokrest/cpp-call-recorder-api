#ifndef CALL_RECORDER_API_MODELS_FilesPermission_H_
#define CALL_RECORDER_API_MODELS_FilesPermission_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class FilesPermission
{
    public:
    enum class Value
    {
        PUBLIC,
        PRIVATE,
    };

    static std::string toString(const Value value);
    static Value fromString(const std::string& value);

    private:
        FilesPermission();
};

}
}
}
}

#endif
