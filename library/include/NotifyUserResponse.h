#ifndef CALL_RECORDER_API_MODELS_NotifyUserResponse_H_
#define CALL_RECORDER_API_MODELS_NotifyUserResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class NotifyUserResponse;
typedef std::shared_ptr<NotifyUserResponse> NotifyUserResponsePtr;

class NotifyUserResponse : public Object
{
public:
    class Builder;

    NotifyUserResponse(
        const std::string& status,
        const std::string& msg
    );
    NotifyUserResponse& operator=(const NotifyUserResponse& other);
    virtual ~NotifyUserResponse();

    const std::string& status() const;
    const std::string& msg() const;

private:
    std::string m_status;
    std::string m_msg;
};

class NotifyUserResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);

    NotifyUserResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
};

}
}
}
}

#endif
