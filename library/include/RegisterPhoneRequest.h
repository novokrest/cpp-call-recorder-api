#ifndef CALL_RECORDER_API_MODELS_RegisterPhoneRequest_H_
#define CALL_RECORDER_API_MODELS_RegisterPhoneRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class RegisterPhoneRequest;
typedef std::shared_ptr<RegisterPhoneRequest> RegisterPhoneRequestPtr;

class RegisterPhoneRequest : public Object
{
public:
    class Builder;

    RegisterPhoneRequest(
        const std::string& token,
        const std::string& phone
    );
    RegisterPhoneRequest& operator=(const RegisterPhoneRequest& other);
    virtual ~RegisterPhoneRequest();

    const std::string& token() const;
    const std::string& phone() const;

private:
    std::string m_token;
    std::string m_phone;
};

class RegisterPhoneRequest::Builder
{
public:
    Builder& token(const std::string& token);
    Builder& phone(const std::string& phone);

    RegisterPhoneRequestPtr build() const;

private:
    std::string m_token;
    std::string m_phone;
};

}
}
}
}

#endif
