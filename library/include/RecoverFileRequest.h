#ifndef CALL_RECORDER_API_MODELS_RecoverFileRequest_H_
#define CALL_RECORDER_API_MODELS_RecoverFileRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class RecoverFileRequest;
typedef std::shared_ptr<RecoverFileRequest> RecoverFileRequestPtr;

class RecoverFileRequest : public Object
{
public:
    class Builder;

    RecoverFileRequest(
        const std::string& apiKey,
        const long& id,
        const long& folderId
    );
    RecoverFileRequest& operator=(const RecoverFileRequest& other);
    virtual ~RecoverFileRequest();

    const std::string& apiKey() const;
    const long& id() const;
    const long& folderId() const;

private:
    std::string m_apiKey;
    long m_id;
    long m_folderId;
};

class RecoverFileRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& id(const long& id);
    Builder& folderId(const long& folderId);

    RecoverFileRequestPtr build() const;

private:
    std::string m_apiKey;
    long m_id;
    long m_folderId;
};

}
}
}
}

#endif
