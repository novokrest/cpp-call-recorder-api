#ifndef CALL_RECORDER_API_MODELS_GetFilesResponseFile_H_
#define CALL_RECORDER_API_MODELS_GetFilesResponseFile_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetFilesResponseFile;
typedef std::shared_ptr<GetFilesResponseFile> GetFilesResponseFilePtr;

class GetFilesResponseFile : public Object
{
public:
    class Builder;

    GetFilesResponseFile(
        const long& id,
        const std::string& accessNumber,
        const std::string& name,
        const std::string& fName,
        const std::string& lName,
        const std::string& email,
        const std::string& phone,
        const std::string& notes,
        const std::string& meta,
        const std::string& source,
        const std::string& url,
        const std::string& credits,
        const std::string& duration,
        const std::string& time,
        const std::string& shareUrl,
        const std::string& downloadUrl
    );
    GetFilesResponseFile& operator=(const GetFilesResponseFile& other);
    virtual ~GetFilesResponseFile();

    const long& id() const;
    const std::string& accessNumber() const;
    const std::string& name() const;
    const std::string& fName() const;
    const std::string& lName() const;
    const std::string& email() const;
    const std::string& phone() const;
    const std::string& notes() const;
    const std::string& meta() const;
    const std::string& source() const;
    const std::string& url() const;
    const std::string& credits() const;
    const std::string& duration() const;
    const std::string& time() const;
    const std::string& shareUrl() const;
    const std::string& downloadUrl() const;

private:
    long m_id;
    std::string m_accessNumber;
    std::string m_name;
    std::string m_fName;
    std::string m_lName;
    std::string m_email;
    std::string m_phone;
    std::string m_notes;
    std::string m_meta;
    std::string m_source;
    std::string m_url;
    std::string m_credits;
    std::string m_duration;
    std::string m_time;
    std::string m_shareUrl;
    std::string m_downloadUrl;
};

class GetFilesResponseFile::Builder
{
public:
    Builder& id(const long& id);
    Builder& accessNumber(const std::string& accessNumber);
    Builder& name(const std::string& name);
    Builder& fName(const std::string& fName);
    Builder& lName(const std::string& lName);
    Builder& email(const std::string& email);
    Builder& phone(const std::string& phone);
    Builder& notes(const std::string& notes);
    Builder& meta(const std::string& meta);
    Builder& source(const std::string& source);
    Builder& url(const std::string& url);
    Builder& credits(const std::string& credits);
    Builder& duration(const std::string& duration);
    Builder& time(const std::string& time);
    Builder& shareUrl(const std::string& shareUrl);
    Builder& downloadUrl(const std::string& downloadUrl);

    GetFilesResponseFilePtr build() const;

private:
    long m_id;
    std::string m_accessNumber;
    std::string m_name;
    std::string m_fName;
    std::string m_lName;
    std::string m_email;
    std::string m_phone;
    std::string m_notes;
    std::string m_meta;
    std::string m_source;
    std::string m_url;
    std::string m_credits;
    std::string m_duration;
    std::string m_time;
    std::string m_shareUrl;
    std::string m_downloadUrl;
};

}
}
}
}

#endif
