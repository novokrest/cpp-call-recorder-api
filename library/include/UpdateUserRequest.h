#ifndef CALL_RECORDER_API_MODELS_UpdateUserRequest_H_
#define CALL_RECORDER_API_MODELS_UpdateUserRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "App.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateUserRequest;
typedef std::shared_ptr<UpdateUserRequest> UpdateUserRequestPtr;

class UpdateUserRequest : public Object
{
public:
    class Builder;

    UpdateUserRequest(
        const std::string& apiKey,
        const App::Value& app,
        const std::string& timezone
    );
    UpdateUserRequest& operator=(const UpdateUserRequest& other);
    virtual ~UpdateUserRequest();

    const std::string& apiKey() const;
    const App::Value& app() const;
    const std::string& timezone() const;

private:
    std::string m_apiKey;
    App::Value m_app;
    std::string m_timezone;
};

class UpdateUserRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& app(const App::Value& app);
    Builder& timezone(const std::string& timezone);

    UpdateUserRequestPtr build() const;

private:
    std::string m_apiKey;
    App::Value m_app;
    std::string m_timezone;
};

}
}
}
}

#endif
