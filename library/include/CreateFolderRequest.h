#ifndef CALL_RECORDER_API_MODELS_CreateFolderRequest_H_
#define CALL_RECORDER_API_MODELS_CreateFolderRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class CreateFolderRequest;
typedef std::shared_ptr<CreateFolderRequest> CreateFolderRequestPtr;

class CreateFolderRequest : public Object
{
public:
    class Builder;

    CreateFolderRequest(
        const std::string& apiKey,
        const std::string& name,
        const std::string& pass
    );
    CreateFolderRequest& operator=(const CreateFolderRequest& other);
    virtual ~CreateFolderRequest();

    const std::string& apiKey() const;
    const std::string& name() const;
    const std::string& pass() const;

private:
    std::string m_apiKey;
    std::string m_name;
    std::string m_pass;
};

class CreateFolderRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& name(const std::string& name);
    Builder& pass(const std::string& pass);

    CreateFolderRequestPtr build() const;

private:
    std::string m_apiKey;
    std::string m_name;
    std::string m_pass;
};

}
}
}
}

#endif
