#ifndef CALL_RECORDER_API_MODELS_VerifyFolderPassRequest_H_
#define CALL_RECORDER_API_MODELS_VerifyFolderPassRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class VerifyFolderPassRequest;
typedef std::shared_ptr<VerifyFolderPassRequest> VerifyFolderPassRequestPtr;

class VerifyFolderPassRequest : public Object
{
public:
    class Builder;

    VerifyFolderPassRequest(
        const std::string& apiKey,
        const long& id,
        const std::string& pass
    );
    VerifyFolderPassRequest& operator=(const VerifyFolderPassRequest& other);
    virtual ~VerifyFolderPassRequest();

    const std::string& apiKey() const;
    const long& id() const;
    const std::string& pass() const;

private:
    std::string m_apiKey;
    long m_id;
    std::string m_pass;
};

class VerifyFolderPassRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& id(const long& id);
    Builder& pass(const std::string& pass);

    VerifyFolderPassRequestPtr build() const;

private:
    std::string m_apiKey;
    long m_id;
    std::string m_pass;
};

}
}
}
}

#endif
