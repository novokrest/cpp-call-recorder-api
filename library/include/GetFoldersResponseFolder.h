#ifndef CALL_RECORDER_API_MODELS_GetFoldersResponseFolder_H_
#define CALL_RECORDER_API_MODELS_GetFoldersResponseFolder_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetFoldersResponseFolder;
typedef std::shared_ptr<GetFoldersResponseFolder> GetFoldersResponseFolderPtr;

class GetFoldersResponseFolder : public Object
{
public:
    class Builder;

    GetFoldersResponseFolder(
        const std::string& id,
        const std::string& name,
        const std::string& created,
        const std::string& updated,
        const std::string& isStar,
        const std::string& orderId
    );
    GetFoldersResponseFolder& operator=(const GetFoldersResponseFolder& other);
    virtual ~GetFoldersResponseFolder();

    const std::string& id() const;
    const std::string& name() const;
    const std::string& created() const;
    const std::string& updated() const;
    const std::string& isStar() const;
    const std::string& orderId() const;

private:
    std::string m_id;
    std::string m_name;
    std::string m_created;
    std::string m_updated;
    std::string m_isStar;
    std::string m_orderId;
};

class GetFoldersResponseFolder::Builder
{
public:
    Builder& id(const std::string& id);
    Builder& name(const std::string& name);
    Builder& created(const std::string& created);
    Builder& updated(const std::string& updated);
    Builder& isStar(const std::string& isStar);
    Builder& orderId(const std::string& orderId);

    GetFoldersResponseFolderPtr build() const;

private:
    std::string m_id;
    std::string m_name;
    std::string m_created;
    std::string m_updated;
    std::string m_isStar;
    std::string m_orderId;
};

}
}
}
}

#endif
