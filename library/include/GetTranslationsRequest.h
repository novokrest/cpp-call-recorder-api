#ifndef CALL_RECORDER_API_MODELS_GetTranslationsRequest_H_
#define CALL_RECORDER_API_MODELS_GetTranslationsRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetTranslationsRequest;
typedef std::shared_ptr<GetTranslationsRequest> GetTranslationsRequestPtr;

class GetTranslationsRequest : public Object
{
public:
    class Builder;

    GetTranslationsRequest(
        const std::string& apiKey,
        const std::string& language
    );
    GetTranslationsRequest& operator=(const GetTranslationsRequest& other);
    virtual ~GetTranslationsRequest();

    const std::string& apiKey() const;
    const std::string& language() const;

private:
    std::string m_apiKey;
    std::string m_language;
};

class GetTranslationsRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& language(const std::string& language);

    GetTranslationsRequestPtr build() const;

private:
    std::string m_apiKey;
    std::string m_language;
};

}
}
}
}

#endif
