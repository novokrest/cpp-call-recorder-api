#ifndef CALL_RECORDER_API_MODELS_UpdateProfileResponse_H_
#define CALL_RECORDER_API_MODELS_UpdateProfileResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateProfileResponse;
typedef std::shared_ptr<UpdateProfileResponse> UpdateProfileResponsePtr;

class UpdateProfileResponse : public Object
{
public:
    class Builder;

    UpdateProfileResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code
    );
    UpdateProfileResponse& operator=(const UpdateProfileResponse& other);
    virtual ~UpdateProfileResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

class UpdateProfileResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);

    UpdateProfileResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

}
}
}
}

#endif
