#ifndef CALL_RECORDER_API_MODELS_DeleteFolderRequest_H_
#define CALL_RECORDER_API_MODELS_DeleteFolderRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class DeleteFolderRequest;
typedef std::shared_ptr<DeleteFolderRequest> DeleteFolderRequestPtr;

class DeleteFolderRequest : public Object
{
public:
    class Builder;

    DeleteFolderRequest(
        const std::string& apiKey,
        const long& id,
        const long& moveTo
    );
    DeleteFolderRequest& operator=(const DeleteFolderRequest& other);
    virtual ~DeleteFolderRequest();

    const std::string& apiKey() const;
    const long& id() const;
    const long& moveTo() const;

private:
    std::string m_apiKey;
    long m_id;
    long m_moveTo;
};

class DeleteFolderRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& id(const long& id);
    Builder& moveTo(const long& moveTo);

    DeleteFolderRequestPtr build() const;

private:
    std::string m_apiKey;
    long m_id;
    long m_moveTo;
};

}
}
}
}

#endif
