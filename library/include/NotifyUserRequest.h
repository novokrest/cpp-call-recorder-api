#ifndef CALL_RECORDER_API_MODELS_NotifyUserRequest_H_
#define CALL_RECORDER_API_MODELS_NotifyUserRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "DeviceType.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class NotifyUserRequest;
typedef std::shared_ptr<NotifyUserRequest> NotifyUserRequestPtr;

class NotifyUserRequest : public Object
{
public:
    class Builder;

    NotifyUserRequest(
        const std::string& apiKey,
        const std::string& title,
        const std::string& body,
        const DeviceType::Value& deviceType
    );
    NotifyUserRequest& operator=(const NotifyUserRequest& other);
    virtual ~NotifyUserRequest();

    const std::string& apiKey() const;
    const std::string& title() const;
    const std::string& body() const;
    const DeviceType::Value& deviceType() const;

private:
    std::string m_apiKey;
    std::string m_title;
    std::string m_body;
    DeviceType::Value m_deviceType;
};

class NotifyUserRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& title(const std::string& title);
    Builder& body(const std::string& body);
    Builder& deviceType(const DeviceType::Value& deviceType);

    NotifyUserRequestPtr build() const;

private:
    std::string m_apiKey;
    std::string m_title;
    std::string m_body;
    DeviceType::Value m_deviceType;
};

}
}
}
}

#endif
