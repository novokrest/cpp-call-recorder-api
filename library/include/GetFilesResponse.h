#ifndef CALL_RECORDER_API_MODELS_GetFilesResponse_H_
#define CALL_RECORDER_API_MODELS_GetFilesResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "GetFilesResponseFile.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetFilesResponse;
typedef std::shared_ptr<GetFilesResponse> GetFilesResponsePtr;

class GetFilesResponse : public Object
{
public:
    class Builder;

    GetFilesResponse(
        const std::string& status,
        const long& credits,
        const long& creditsTrans,
        const std::vector<GetFilesResponseFile>& files
    );
    GetFilesResponse& operator=(const GetFilesResponse& other);
    virtual ~GetFilesResponse();

    const std::string& status() const;
    const long& credits() const;
    const long& creditsTrans() const;
    const std::vector<GetFilesResponseFile>& files() const;

private:
    std::string m_status;
    long m_credits;
    long m_creditsTrans;
    std::vector<GetFilesResponseFile> m_files;
};

class GetFilesResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& credits(const long& credits);
    Builder& creditsTrans(const long& creditsTrans);
    Builder& files(const std::vector<GetFilesResponseFile>& files);

    GetFilesResponsePtr build() const;

private:
    std::string m_status;
    long m_credits;
    long m_creditsTrans;
    std::vector<GetFilesResponseFile> m_files;
};

}
}
}
}

#endif
