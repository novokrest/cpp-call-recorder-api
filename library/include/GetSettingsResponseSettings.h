#ifndef CALL_RECORDER_API_MODELS_GetSettingsResponseSettings_H_
#define CALL_RECORDER_API_MODELS_GetSettingsResponseSettings_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "FilesPermission.h"
#include "PlayBeep.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetSettingsResponseSettings;
typedef std::shared_ptr<GetSettingsResponseSettings> GetSettingsResponseSettingsPtr;

class GetSettingsResponseSettings : public Object
{
public:
    class Builder;

    GetSettingsResponseSettings(
        const PlayBeep::Value& playBeep,
        const FilesPermission::Value& filesPermission
    );
    GetSettingsResponseSettings& operator=(const GetSettingsResponseSettings& other);
    virtual ~GetSettingsResponseSettings();

    const PlayBeep::Value& playBeep() const;
    const FilesPermission::Value& filesPermission() const;

private:
    PlayBeep::Value m_playBeep;
    FilesPermission::Value m_filesPermission;
};

class GetSettingsResponseSettings::Builder
{
public:
    Builder& playBeep(const PlayBeep::Value& playBeep);
    Builder& filesPermission(const FilesPermission::Value& filesPermission);

    GetSettingsResponseSettingsPtr build() const;

private:
    PlayBeep::Value m_playBeep;
    FilesPermission::Value m_filesPermission;
};

}
}
}
}

#endif
