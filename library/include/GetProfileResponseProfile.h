#ifndef CALL_RECORDER_API_MODELS_GetProfileResponseProfile_H_
#define CALL_RECORDER_API_MODELS_GetProfileResponseProfile_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetProfileResponseProfile;
typedef std::shared_ptr<GetProfileResponseProfile> GetProfileResponseProfilePtr;

class GetProfileResponseProfile : public Object
{
public:
    class Builder;

    GetProfileResponseProfile(
        const std::string& fName,
        const std::string& lName,
        const std::string& email,
        const std::string& phone,
        const std::string& pic,
        const std::string& language,
        const int& isPublic,
        const int& playBeep,
        const long& maxLength,
        const std::string& timeZone,
        const long& time,
        const std::string& pin
    );
    GetProfileResponseProfile& operator=(const GetProfileResponseProfile& other);
    virtual ~GetProfileResponseProfile();

    const std::string& fName() const;
    const std::string& lName() const;
    const std::string& email() const;
    const std::string& phone() const;
    const std::string& pic() const;
    const std::string& language() const;
    const int& isPublic() const;
    const int& playBeep() const;
    const long& maxLength() const;
    const std::string& timeZone() const;
    const long& time() const;
    const std::string& pin() const;

private:
    std::string m_fName;
    std::string m_lName;
    std::string m_email;
    std::string m_phone;
    std::string m_pic;
    std::string m_language;
    int m_isPublic;
    int m_playBeep;
    long m_maxLength;
    std::string m_timeZone;
    long m_time;
    std::string m_pin;
};

class GetProfileResponseProfile::Builder
{
public:
    Builder& fName(const std::string& fName);
    Builder& lName(const std::string& lName);
    Builder& email(const std::string& email);
    Builder& phone(const std::string& phone);
    Builder& pic(const std::string& pic);
    Builder& language(const std::string& language);
    Builder& isPublic(const int& isPublic);
    Builder& playBeep(const int& playBeep);
    Builder& maxLength(const long& maxLength);
    Builder& timeZone(const std::string& timeZone);
    Builder& time(const long& time);
    Builder& pin(const std::string& pin);

    GetProfileResponseProfilePtr build() const;

private:
    std::string m_fName;
    std::string m_lName;
    std::string m_email;
    std::string m_phone;
    std::string m_pic;
    std::string m_language;
    int m_isPublic;
    int m_playBeep;
    long m_maxLength;
    std::string m_timeZone;
    long m_time;
    std::string m_pin;
};

}
}
}
}

#endif
