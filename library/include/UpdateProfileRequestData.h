#ifndef CALL_RECORDER_API_MODELS_UpdateProfileRequestData_H_
#define CALL_RECORDER_API_MODELS_UpdateProfileRequestData_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateProfileRequestData;
typedef std::shared_ptr<UpdateProfileRequestData> UpdateProfileRequestDataPtr;

class UpdateProfileRequestData : public Object
{
public:
    class Builder;

    UpdateProfileRequestData(
        const std::string& fName,
        const std::string& lName,
        const std::string& email,
        const bool& isPublic,
        const std::string& language
    );
    UpdateProfileRequestData& operator=(const UpdateProfileRequestData& other);
    virtual ~UpdateProfileRequestData();

    const std::string& fName() const;
    const std::string& lName() const;
    const std::string& email() const;
    const bool& isPublic() const;
    const std::string& language() const;

private:
    std::string m_fName;
    std::string m_lName;
    std::string m_email;
    bool m_isPublic;
    std::string m_language;
};

class UpdateProfileRequestData::Builder
{
public:
    Builder& fName(const std::string& fName);
    Builder& lName(const std::string& lName);
    Builder& email(const std::string& email);
    Builder& isPublic(const bool& isPublic);
    Builder& language(const std::string& language);

    UpdateProfileRequestDataPtr build() const;

private:
    std::string m_fName;
    std::string m_lName;
    std::string m_email;
    bool m_isPublic;
    std::string m_language;
};

}
}
}
}

#endif
