#ifndef CALL_RECORDER_API_MODELS_VerifyPhoneResponse_H_
#define CALL_RECORDER_API_MODELS_VerifyPhoneResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class VerifyPhoneResponse;
typedef std::shared_ptr<VerifyPhoneResponse> VerifyPhoneResponsePtr;

class VerifyPhoneResponse : public Object
{
public:
    class Builder;

    VerifyPhoneResponse(
        const std::string& status,
        const std::string& phone,
        const std::string& apiKey,
        const std::string& msg
    );
    VerifyPhoneResponse& operator=(const VerifyPhoneResponse& other);
    virtual ~VerifyPhoneResponse();

    const std::string& status() const;
    const std::string& phone() const;
    const std::string& apiKey() const;
    const std::string& msg() const;

private:
    std::string m_status;
    std::string m_phone;
    std::string m_apiKey;
    std::string m_msg;
};

class VerifyPhoneResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& phone(const std::string& phone);
    Builder& apiKey(const std::string& apiKey);
    Builder& msg(const std::string& msg);

    VerifyPhoneResponsePtr build() const;

private:
    std::string m_status;
    std::string m_phone;
    std::string m_apiKey;
    std::string m_msg;
};

}
}
}
}

#endif
