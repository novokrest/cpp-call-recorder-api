#ifndef CALL_RECORDER_API_MODELS_FileUtils_H_
#define CALL_RECORDER_API_MODELS_FileUtils_H_

#include "HttpContent.h"

#include <memory>
#include <string>

using namespace call::recorder::api::models;

namespace call {
namespace recorder {
namespace api {
namespace utils {

class FileUtils
{
    public:
    static std::shared_ptr<HttpContent> toHttpContent(const std::string& filePath);

    private:
        FileUtils() = delete;
        FileUtils(FileUtils& other) = delete;
        FileUtils(FileUtils&& other) = delete;
        ~FileUtils() = delete;
};

}
}
}
}

#endif

