#ifndef CALL_RECORDER_API_MODELS_Language_H_
#define CALL_RECORDER_API_MODELS_Language_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class Language;
typedef std::shared_ptr<Language> LanguagePtr;

class Language : public Object
{
public:
    class Builder;

    Language(
        const std::string& code,
        const std::string& name,
        const std::string& flag
    );
    Language& operator=(const Language& other);
    virtual ~Language();

    const std::string& code() const;
    const std::string& name() const;
    const std::string& flag() const;

private:
    std::string m_code;
    std::string m_name;
    std::string m_flag;
};

class Language::Builder
{
public:
    Builder& code(const std::string& code);
    Builder& name(const std::string& name);
    Builder& flag(const std::string& flag);

    LanguagePtr build() const;

private:
    std::string m_code;
    std::string m_name;
    std::string m_flag;
};

}
}
}
}

#endif
