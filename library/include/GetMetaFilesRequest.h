#ifndef CALL_RECORDER_API_MODELS_GetMetaFilesRequest_H_
#define CALL_RECORDER_API_MODELS_GetMetaFilesRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetMetaFilesRequest;
typedef std::shared_ptr<GetMetaFilesRequest> GetMetaFilesRequestPtr;

class GetMetaFilesRequest : public Object
{
public:
    class Builder;

    GetMetaFilesRequest(
        const std::string& apiKey,
        const long& parentId
    );
    GetMetaFilesRequest& operator=(const GetMetaFilesRequest& other);
    virtual ~GetMetaFilesRequest();

    const std::string& apiKey() const;
    const long& parentId() const;

private:
    std::string m_apiKey;
    long m_parentId;
};

class GetMetaFilesRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& parentId(const long& parentId);

    GetMetaFilesRequestPtr build() const;

private:
    std::string m_apiKey;
    long m_parentId;
};

}
}
}
}

#endif
