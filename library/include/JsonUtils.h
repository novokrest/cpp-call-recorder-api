#ifndef CALL_RECORDER_API_UTILS_JsonUtils_H_
#define CALL_RECORDER_API_UTILS_JsonUtils_H_

#include <cpprest/json.h>
#include <functional>
#include <vector>
#include <memory>
#include <map>

namespace call {
namespace recorder {
namespace api {
namespace utils {

template<typename T>
using JsonValueConverter = std::function<std::shared_ptr<T>(web::json::value)>;

template<typename T>
using ValueJsonConverter = std::function<web::json::value(const T&)>;

class JsonUtils
{
public:
    template<class T>
    static std::vector<T> toArray(web::json::array jarray, std::function<std::shared_ptr<T>(web::json::value)> converter)
    {
        std::vector<T> result;
        for (int i = 0; i < jarray.size(); ++i)
        {
            auto jvalue = jarray.at(i);
            result.push_back(*converter(jvalue));
        }
        return result;
    }
    static std::string toRawJson(const std::map<std::string, std::string>& fields);
    static std::string toRawJson(const std::map<std::string, web::json::value>& fields);
    static std::string toRawJson(const std::vector<std::string>& values);
    static std::string toRawJson(const std::vector<long>& values);
    
    template<class T>
    static std::string toRawJson(const std::vector<T>& values, std::function<web::json::value(const T&)> converter)
    {
        auto jarray = web::json::value::array(values.size());
        for (int i = 0; i < values.size(); ++i)
        {
            jarray[i] = converter(values[i]);
        }
        return serializeJson(jarray);
    }

    template<class T>
    static std::string toRawJson(const T& value, std::function<web::json::value(const T&)> converter)
    {
        return serializeJson(converter(value));
    }

    static std::string serializeJson(const web::json::value& value);
    static web::json::value toJsonArray(const std::vector<std::string>& values);
    static web::json::value toJson(const std::map<std::string, web::json::value>& fields);

};

}
}
}
}

#endif
