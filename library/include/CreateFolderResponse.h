#ifndef CALL_RECORDER_API_MODELS_CreateFolderResponse_H_
#define CALL_RECORDER_API_MODELS_CreateFolderResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class CreateFolderResponse;
typedef std::shared_ptr<CreateFolderResponse> CreateFolderResponsePtr;

class CreateFolderResponse : public Object
{
public:
    class Builder;

    CreateFolderResponse(
        const std::string& status,
        const std::string& msg,
        const long& id,
        const std::string& code
    );
    CreateFolderResponse& operator=(const CreateFolderResponse& other);
    virtual ~CreateFolderResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const long& id() const;
    const std::string& code() const;

private:
    std::string m_status;
    std::string m_msg;
    long m_id;
    std::string m_code;
};

class CreateFolderResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& id(const long& id);
    Builder& code(const std::string& code);

    CreateFolderResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    long m_id;
    std::string m_code;
};

}
}
}
}

#endif
