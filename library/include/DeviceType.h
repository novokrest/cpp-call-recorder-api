#ifndef CALL_RECORDER_API_MODELS_DeviceType_H_
#define CALL_RECORDER_API_MODELS_DeviceType_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class DeviceType
{
    public:
    enum class Value
    {
        ANDROID,
        IOS,
        MAC,
        WINDOWS,
        WEB,
        CUSTOM,
    };

    static std::string toString(const Value value);
    static Value fromString(const std::string& value);

    private:
        DeviceType();
};

}
}
}
}

#endif
