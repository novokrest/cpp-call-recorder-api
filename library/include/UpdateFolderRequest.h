#ifndef CALL_RECORDER_API_MODELS_UpdateFolderRequest_H_
#define CALL_RECORDER_API_MODELS_UpdateFolderRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateFolderRequest;
typedef std::shared_ptr<UpdateFolderRequest> UpdateFolderRequestPtr;

class UpdateFolderRequest : public Object
{
public:
    class Builder;

    UpdateFolderRequest(
        const std::string& apiKey,
        const long& id,
        const std::string& name,
        const std::string& pass,
        const bool& isPrivate
    );
    UpdateFolderRequest& operator=(const UpdateFolderRequest& other);
    virtual ~UpdateFolderRequest();

    const std::string& apiKey() const;
    const long& id() const;
    const std::string& name() const;
    const std::string& pass() const;
    const bool& isPrivate() const;

private:
    std::string m_apiKey;
    long m_id;
    std::string m_name;
    std::string m_pass;
    bool m_isPrivate;
};

class UpdateFolderRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& id(const long& id);
    Builder& name(const std::string& name);
    Builder& pass(const std::string& pass);
    Builder& isPrivate(const bool& isPrivate);

    UpdateFolderRequestPtr build() const;

private:
    std::string m_apiKey;
    long m_id;
    std::string m_name;
    std::string m_pass;
    bool m_isPrivate;
};

}
}
}
}

#endif
