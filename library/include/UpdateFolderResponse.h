#ifndef CALL_RECORDER_API_MODELS_UpdateFolderResponse_H_
#define CALL_RECORDER_API_MODELS_UpdateFolderResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateFolderResponse;
typedef std::shared_ptr<UpdateFolderResponse> UpdateFolderResponsePtr;

class UpdateFolderResponse : public Object
{
public:
    class Builder;

    UpdateFolderResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code
    );
    UpdateFolderResponse& operator=(const UpdateFolderResponse& other);
    virtual ~UpdateFolderResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

class UpdateFolderResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);

    UpdateFolderResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

}
}
}
}

#endif
