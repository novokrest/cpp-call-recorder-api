#ifndef CALL_RECORDER_API_MODELS_UpdateOrderRequestFolder_H_
#define CALL_RECORDER_API_MODELS_UpdateOrderRequestFolder_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateOrderRequestFolder;
typedef std::shared_ptr<UpdateOrderRequestFolder> UpdateOrderRequestFolderPtr;

class UpdateOrderRequestFolder : public Object
{
public:
    class Builder;

    UpdateOrderRequestFolder(
        const long& id,
        const long& orderId
    );
    UpdateOrderRequestFolder& operator=(const UpdateOrderRequestFolder& other);
    virtual ~UpdateOrderRequestFolder();

    const long& id() const;
    const long& orderId() const;

private:
    long m_id;
    long m_orderId;
};

class UpdateOrderRequestFolder::Builder
{
public:
    Builder& id(const long& id);
    Builder& orderId(const long& orderId);

    UpdateOrderRequestFolderPtr build() const;

private:
    long m_id;
    long m_orderId;
};

}
}
}
}

#endif
