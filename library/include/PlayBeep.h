#ifndef CALL_RECORDER_API_MODELS_PlayBeep_H_
#define CALL_RECORDER_API_MODELS_PlayBeep_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class PlayBeep
{
    public:
    enum class Value
    {
        YES,
        NO,
    };

    static std::string toString(const Value value);
    static Value fromString(const std::string& value);

    private:
        PlayBeep();
};

}
}
}
}

#endif
