#ifndef CALL_RECORDER_API_MODELS_GetPhonesRequest_H_
#define CALL_RECORDER_API_MODELS_GetPhonesRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetPhonesRequest;
typedef std::shared_ptr<GetPhonesRequest> GetPhonesRequestPtr;

class GetPhonesRequest : public Object
{
public:
    class Builder;

    GetPhonesRequest(
        const std::string& apiKey
    );
    GetPhonesRequest& operator=(const GetPhonesRequest& other);
    virtual ~GetPhonesRequest();

    const std::string& apiKey() const;

private:
    std::string m_apiKey;
};

class GetPhonesRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);

    GetPhonesRequestPtr build() const;

private:
    std::string m_apiKey;
};

}
}
}
}

#endif
