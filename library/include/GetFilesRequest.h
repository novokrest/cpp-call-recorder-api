#ifndef CALL_RECORDER_API_MODELS_GetFilesRequest_H_
#define CALL_RECORDER_API_MODELS_GetFilesRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetFilesRequest;
typedef std::shared_ptr<GetFilesRequest> GetFilesRequestPtr;

class GetFilesRequest : public Object
{
public:
    class Builder;

    GetFilesRequest(
        const std::string& apiKey,
        const std::string& page,
        const long& folderId,
        const std::string& source,
        const std::string& pass,
        const bool& reminder,
        const std::string& q,
        const long& id,
        const std::string& op
    );
    GetFilesRequest& operator=(const GetFilesRequest& other);
    virtual ~GetFilesRequest();

    const std::string& apiKey() const;
    const std::string& page() const;
    const long& folderId() const;
    const std::string& source() const;
    const std::string& pass() const;
    const bool& reminder() const;
    const std::string& q() const;
    const long& id() const;
    const std::string& op() const;

private:
    std::string m_apiKey;
    std::string m_page;
    long m_folderId;
    std::string m_source;
    std::string m_pass;
    bool m_reminder;
    std::string m_q;
    long m_id;
    std::string m_op;
};

class GetFilesRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& page(const std::string& page);
    Builder& folderId(const long& folderId);
    Builder& source(const std::string& source);
    Builder& pass(const std::string& pass);
    Builder& reminder(const bool& reminder);
    Builder& q(const std::string& q);
    Builder& id(const long& id);
    Builder& op(const std::string& op);

    GetFilesRequestPtr build() const;

private:
    std::string m_apiKey;
    std::string m_page;
    long m_folderId;
    std::string m_source;
    std::string m_pass;
    bool m_reminder;
    std::string m_q;
    long m_id;
    std::string m_op;
};

}
}
}
}

#endif
