#ifndef CALL_RECORDER_API_MODELS_UpdateStarRequest_H_
#define CALL_RECORDER_API_MODELS_UpdateStarRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateStarRequest;
typedef std::shared_ptr<UpdateStarRequest> UpdateStarRequestPtr;

class UpdateStarRequest : public Object
{
public:
    class Builder;

    UpdateStarRequest(
        const std::string& apiKey,
        const long& id,
        const int& star,
        const std::string& type
    );
    UpdateStarRequest& operator=(const UpdateStarRequest& other);
    virtual ~UpdateStarRequest();

    const std::string& apiKey() const;
    const long& id() const;
    const int& star() const;
    const std::string& type() const;

private:
    std::string m_apiKey;
    long m_id;
    int m_star;
    std::string m_type;
};

class UpdateStarRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& id(const long& id);
    Builder& star(const int& star);
    Builder& type(const std::string& type);

    UpdateStarRequestPtr build() const;

private:
    std::string m_apiKey;
    long m_id;
    int m_star;
    std::string m_type;
};

}
}
}
}

#endif
