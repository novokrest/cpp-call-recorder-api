#ifndef CALL_RECORDER_API_CallRecorderApi_H_
#define CALL_RECORDER_API_CallRecorderApi_H_

#include "ApiClient.h"

#include "App.h"
#include "BuyCreditsResponse.h"
#include "CloneFileResponse.h"
#include "CreateFileData.h"
#include "CreateFileResponse.h"
#include "CreateFolderResponse.h"
#include "DeleteFilesResponse.h"
#include "DeleteFolderResponse.h"
#include "DeleteMetaFilesResponse.h"
#include "DeviceType.h"
#include "FilesPermission.h"
#include "GetFilesResponse.h"
#include "GetFoldersResponse.h"
#include "GetLanguagesResponse.h"
#include "GetMessagesResponse.h"
#include "GetMetaFilesResponse.h"
#include "GetPhonesResponse.h"
#include "GetProfileResponse.h"
#include "GetSettingsResponse.h"
#include "GetTranslationsResponse.h"
#include "NotifyUserResponse.h"
#include "PlayBeep.h"
#include "RecoverFileResponse.h"
#include "RegisterPhoneResponse.h"
#include "UpdateDeviceTokenResponse.h"
#include "UpdateFileResponse.h"
#include "UpdateFolderResponse.h"
#include "UpdateOrderRequestFolder.h"
#include "UpdateOrderResponse.h"
#include "UpdateProfileImgResponse.h"
#include "UpdateProfileRequestData.h"
#include "UpdateProfileResponse.h"
#include "UpdateSettingsResponse.h"
#include "UpdateStarResponse.h"
#include "UpdateUserResponse.h"
#include "UploadMetaFileResponse.h"
#include "VerifyFolderPassResponse.h"
#include "VerifyPhoneResponse.h"

#include "BuyCreditsRequest.h"
#include "CloneFileRequest.h"
#include "CreateFileRequest.h"
#include "CreateFolderRequest.h"
#include "DeleteFilesRequest.h"
#include "DeleteFolderRequest.h"
#include "DeleteMetaFilesRequest.h"
#include "GetFilesRequest.h"
#include "GetFoldersRequest.h"
#include "GetLanguagesRequest.h"
#include "GetMetaFilesRequest.h"
#include "GetMessagesRequest.h"
#include "GetPhonesRequest.h"
#include "GetProfileRequest.h"
#include "GetSettingsRequest.h"
#include "GetTranslationsRequest.h"
#include "NotifyUserRequest.h"
#include "RecoverFileRequest.h"
#include "RegisterPhoneRequest.h"
#include "UpdateDeviceTokenRequest.h"
#include "UpdateFileRequest.h"
#include "UpdateFolderRequest.h"
#include "UpdateOrderRequest.h"
#include "UpdateProfileImgRequest.h"
#include "UpdateProfileRequest.h"
#include "UpdateSettingsRequest.h"
#include "UpdateStarRequest.h"
#include "UpdateUserRequest.h"
#include "UploadMetaFileRequest.h"
#include "VerifyFolderPassRequest.h"
#include "VerifyPhoneRequest.h"

#include <boost/optional.hpp>
#include <memory>
#include <string>

namespace call {
namespace recorder {
namespace api {

using namespace call::recorder::api::models;

class CallRecorderApi;
typedef std::shared_ptr<CallRecorderApi> CallRecorderApiPtr;

class  CallRecorderApi
{
public:
    static CallRecorderApiPtr create(const std::string& baseUrl);

    CallRecorderApi(std::shared_ptr<ApiClient> apiClient);
    virtual ~CallRecorderApi();

    BuyCreditsResponsePtr buyCredits(const BuyCreditsRequest& request);
    pplx::task<BuyCreditsResponsePtr> buyCreditsAsync(const BuyCreditsRequest& request);

    CloneFileResponsePtr cloneFile(const CloneFileRequest& request);
    pplx::task<CloneFileResponsePtr> cloneFileAsync(const CloneFileRequest& request);

    CreateFileResponsePtr createFile(const CreateFileRequest& request);
    pplx::task<CreateFileResponsePtr> createFileAsync(const CreateFileRequest& request);

    CreateFolderResponsePtr createFolder(const CreateFolderRequest& request);
    pplx::task<CreateFolderResponsePtr> createFolderAsync(const CreateFolderRequest& request);

    DeleteFilesResponsePtr deleteFiles(const DeleteFilesRequest& request);
    pplx::task<DeleteFilesResponsePtr> deleteFilesAsync(const DeleteFilesRequest& request);

    DeleteFolderResponsePtr deleteFolder(const DeleteFolderRequest& request);
    pplx::task<DeleteFolderResponsePtr> deleteFolderAsync(const DeleteFolderRequest& request);

    DeleteMetaFilesResponsePtr deleteMetaFiles(const DeleteMetaFilesRequest& request);
    pplx::task<DeleteMetaFilesResponsePtr> deleteMetaFilesAsync(const DeleteMetaFilesRequest& request);

    GetFilesResponsePtr getFiles(const GetFilesRequest& request);
    pplx::task<GetFilesResponsePtr> getFilesAsync(const GetFilesRequest& request);

    GetFoldersResponsePtr getFolders(const GetFoldersRequest& request);
    pplx::task<GetFoldersResponsePtr> getFoldersAsync(const GetFoldersRequest& request);

    GetLanguagesResponsePtr getLanguages(const GetLanguagesRequest& request);
    pplx::task<GetLanguagesResponsePtr> getLanguagesAsync(const GetLanguagesRequest& request);

    GetMetaFilesResponsePtr getMetaFiles(const GetMetaFilesRequest& request);
    pplx::task<GetMetaFilesResponsePtr> getMetaFilesAsync(const GetMetaFilesRequest& request);

    GetMessagesResponsePtr getMsgs(const GetMessagesRequest& request);
    pplx::task<GetMessagesResponsePtr> getMsgsAsync(const GetMessagesRequest& request);

    GetPhonesResponsePtr getPhones(const GetPhonesRequest& request);
    pplx::task<GetPhonesResponsePtr> getPhonesAsync(const GetPhonesRequest& request);

    GetProfileResponsePtr getProfile(const GetProfileRequest& request);
    pplx::task<GetProfileResponsePtr> getProfileAsync(const GetProfileRequest& request);

    GetSettingsResponsePtr getSettings(const GetSettingsRequest& request);
    pplx::task<GetSettingsResponsePtr> getSettingsAsync(const GetSettingsRequest& request);

    GetTranslationsResponsePtr getTranslations(const GetTranslationsRequest& request);
    pplx::task<GetTranslationsResponsePtr> getTranslationsAsync(const GetTranslationsRequest& request);

    NotifyUserResponsePtr notifyUserCustom(const NotifyUserRequest& request);
    pplx::task<NotifyUserResponsePtr> notifyUserCustomAsync(const NotifyUserRequest& request);

    RecoverFileResponsePtr recoverFile(const RecoverFileRequest& request);
    pplx::task<RecoverFileResponsePtr> recoverFileAsync(const RecoverFileRequest& request);

    RegisterPhoneResponsePtr registerPhone(const RegisterPhoneRequest& request);
    pplx::task<RegisterPhoneResponsePtr> registerPhoneAsync(const RegisterPhoneRequest& request);

    UpdateDeviceTokenResponsePtr updateDeviceToken(const UpdateDeviceTokenRequest& request);
    pplx::task<UpdateDeviceTokenResponsePtr> updateDeviceTokenAsync(const UpdateDeviceTokenRequest& request);

    UpdateFileResponsePtr updateFile(const UpdateFileRequest& request);
    pplx::task<UpdateFileResponsePtr> updateFileAsync(const UpdateFileRequest& request);

    UpdateFolderResponsePtr updateFolder(const UpdateFolderRequest& request);
    pplx::task<UpdateFolderResponsePtr> updateFolderAsync(const UpdateFolderRequest& request);

    UpdateOrderResponsePtr updateOrder(const UpdateOrderRequest& request);
    pplx::task<UpdateOrderResponsePtr> updateOrderAsync(const UpdateOrderRequest& request);

    UpdateProfileImgResponsePtr updateProfileImg(const UpdateProfileImgRequest& request);
    pplx::task<UpdateProfileImgResponsePtr> updateProfileImgAsync(const UpdateProfileImgRequest& request);

    UpdateProfileResponsePtr updateProfile(const UpdateProfileRequest& request);
    pplx::task<UpdateProfileResponsePtr> updateProfileAsync(const UpdateProfileRequest& request);

    UpdateSettingsResponsePtr updateSettings(const UpdateSettingsRequest& request);
    pplx::task<UpdateSettingsResponsePtr> updateSettingsAsync(const UpdateSettingsRequest& request);

    UpdateStarResponsePtr updateStar(const UpdateStarRequest& request);
    pplx::task<UpdateStarResponsePtr> updateStarAsync(const UpdateStarRequest& request);

    UpdateUserResponsePtr updateUser(const UpdateUserRequest& request);
    pplx::task<UpdateUserResponsePtr> updateUserAsync(const UpdateUserRequest& request);

    UploadMetaFileResponsePtr uploadMetaFile(const UploadMetaFileRequest& request);
    pplx::task<UploadMetaFileResponsePtr> uploadMetaFileAsync(const UploadMetaFileRequest& request);

    VerifyFolderPassResponsePtr verifyFolderPass(const VerifyFolderPassRequest& request);
    pplx::task<VerifyFolderPassResponsePtr> verifyFolderPassAsync(const VerifyFolderPassRequest& request);

    VerifyPhoneResponsePtr verifyPhone(const VerifyPhoneRequest& request);
    pplx::task<VerifyPhoneResponsePtr> verifyPhoneAsync(const VerifyPhoneRequest& request);


private:
    std::shared_ptr<ApiClient> m_ApiClient;
};

}
}
}

#endif /* CALL_RECORDER_API_CallRecorderApi_H_ */
