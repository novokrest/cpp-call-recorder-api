#ifndef CALL_RECORDER_API_MODELS_UpdateSettingsResponse_H_
#define CALL_RECORDER_API_MODELS_UpdateSettingsResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateSettingsResponse;
typedef std::shared_ptr<UpdateSettingsResponse> UpdateSettingsResponsePtr;

class UpdateSettingsResponse : public Object
{
public:
    class Builder;

    UpdateSettingsResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code
    );
    UpdateSettingsResponse& operator=(const UpdateSettingsResponse& other);
    virtual ~UpdateSettingsResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

class UpdateSettingsResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);

    UpdateSettingsResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

}
}
}
}

#endif
