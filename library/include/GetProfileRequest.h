#ifndef CALL_RECORDER_API_MODELS_GetProfileRequest_H_
#define CALL_RECORDER_API_MODELS_GetProfileRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetProfileRequest;
typedef std::shared_ptr<GetProfileRequest> GetProfileRequestPtr;

class GetProfileRequest : public Object
{
public:
    class Builder;

    GetProfileRequest(
        const std::string& apiKey
    );
    GetProfileRequest& operator=(const GetProfileRequest& other);
    virtual ~GetProfileRequest();

    const std::string& apiKey() const;

private:
    std::string m_apiKey;
};

class GetProfileRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);

    GetProfileRequestPtr build() const;

private:
    std::string m_apiKey;
};

}
}
}
}

#endif
