#ifndef CALL_RECORDER_API_MODELS_UploadMetaFileResponse_H_
#define CALL_RECORDER_API_MODELS_UploadMetaFileResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UploadMetaFileResponse;
typedef std::shared_ptr<UploadMetaFileResponse> UploadMetaFileResponsePtr;

class UploadMetaFileResponse : public Object
{
public:
    class Builder;

    UploadMetaFileResponse(
        const std::string& status,
        const std::string& msg,
        const long& parentId,
        const long& id
    );
    UploadMetaFileResponse& operator=(const UploadMetaFileResponse& other);
    virtual ~UploadMetaFileResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const long& parentId() const;
    const long& id() const;

private:
    std::string m_status;
    std::string m_msg;
    long m_parentId;
    long m_id;
};

class UploadMetaFileResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& parentId(const long& parentId);
    Builder& id(const long& id);

    UploadMetaFileResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    long m_parentId;
    long m_id;
};

}
}
}
}

#endif
