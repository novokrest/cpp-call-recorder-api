#ifndef CALL_RECORDER_API_MODELS_RecoverFileResponse_H_
#define CALL_RECORDER_API_MODELS_RecoverFileResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class RecoverFileResponse;
typedef std::shared_ptr<RecoverFileResponse> RecoverFileResponsePtr;

class RecoverFileResponse : public Object
{
public:
    class Builder;

    RecoverFileResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code
    );
    RecoverFileResponse& operator=(const RecoverFileResponse& other);
    virtual ~RecoverFileResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

class RecoverFileResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);

    RecoverFileResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

}
}
}
}

#endif
