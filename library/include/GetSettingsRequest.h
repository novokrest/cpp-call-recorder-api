#ifndef CALL_RECORDER_API_MODELS_GetSettingsRequest_H_
#define CALL_RECORDER_API_MODELS_GetSettingsRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetSettingsRequest;
typedef std::shared_ptr<GetSettingsRequest> GetSettingsRequestPtr;

class GetSettingsRequest : public Object
{
public:
    class Builder;

    GetSettingsRequest(
        const std::string& apiKey
    );
    GetSettingsRequest& operator=(const GetSettingsRequest& other);
    virtual ~GetSettingsRequest();

    const std::string& apiKey() const;

private:
    std::string m_apiKey;
};

class GetSettingsRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);

    GetSettingsRequestPtr build() const;

private:
    std::string m_apiKey;
};

}
}
}
}

#endif
