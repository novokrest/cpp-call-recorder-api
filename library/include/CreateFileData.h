#ifndef CALL_RECORDER_API_MODELS_CreateFileData_H_
#define CALL_RECORDER_API_MODELS_CreateFileData_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class CreateFileData;
typedef std::shared_ptr<CreateFileData> CreateFileDataPtr;

class CreateFileData : public Object
{
public:
    class Builder;

    CreateFileData(
        const std::string& name,
        const std::string& email,
        const std::string& phone,
        const std::string& lName,
        const std::string& fName,
        const std::string& notes,
        const std::string& tags,
        const std::vector<std::string>& meta,
        const std::string& source,
        const std::string& remindDays,
        const std::string& remindDate
    );
    CreateFileData& operator=(const CreateFileData& other);
    virtual ~CreateFileData();

    const std::string& name() const;
    const std::string& email() const;
    const std::string& phone() const;
    const std::string& lName() const;
    const std::string& fName() const;
    const std::string& notes() const;
    const std::string& tags() const;
    const std::vector<std::string>& meta() const;
    const std::string& source() const;
    const std::string& remindDays() const;
    const std::string& remindDate() const;

private:
    std::string m_name;
    std::string m_email;
    std::string m_phone;
    std::string m_lName;
    std::string m_fName;
    std::string m_notes;
    std::string m_tags;
    std::vector<std::string> m_meta;
    std::string m_source;
    std::string m_remindDays;
    std::string m_remindDate;
};

class CreateFileData::Builder
{
public:
    Builder& name(const std::string& name);
    Builder& email(const std::string& email);
    Builder& phone(const std::string& phone);
    Builder& lName(const std::string& lName);
    Builder& fName(const std::string& fName);
    Builder& notes(const std::string& notes);
    Builder& tags(const std::string& tags);
    Builder& meta(const std::vector<std::string>& meta);
    Builder& source(const std::string& source);
    Builder& remindDays(const std::string& remindDays);
    Builder& remindDate(const std::string& remindDate);

    CreateFileDataPtr build() const;

private:
    std::string m_name;
    std::string m_email;
    std::string m_phone;
    std::string m_lName;
    std::string m_fName;
    std::string m_notes;
    std::string m_tags;
    std::vector<std::string> m_meta;
    std::string m_source;
    std::string m_remindDays;
    std::string m_remindDate;
};

}
}
}
}

#endif
