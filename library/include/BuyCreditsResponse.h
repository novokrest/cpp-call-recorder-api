#ifndef CALL_RECORDER_API_MODELS_BuyCreditsResponse_H_
#define CALL_RECORDER_API_MODELS_BuyCreditsResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class BuyCreditsResponse;
typedef std::shared_ptr<BuyCreditsResponse> BuyCreditsResponsePtr;

class BuyCreditsResponse : public Object
{
public:
    class Builder;

    BuyCreditsResponse(
        const std::string& status,
        const std::string& msg
    );
    BuyCreditsResponse& operator=(const BuyCreditsResponse& other);
    virtual ~BuyCreditsResponse();

    const std::string& status() const;
    const std::string& msg() const;

private:
    std::string m_status;
    std::string m_msg;
};

class BuyCreditsResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);

    BuyCreditsResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
};

}
}
}
}

#endif
