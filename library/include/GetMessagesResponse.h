#ifndef CALL_RECORDER_API_MODELS_GetMessagesResponse_H_
#define CALL_RECORDER_API_MODELS_GetMessagesResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "GetMessagesResponseMsg.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetMessagesResponse;
typedef std::shared_ptr<GetMessagesResponse> GetMessagesResponsePtr;

class GetMessagesResponse : public Object
{
public:
    class Builder;

    GetMessagesResponse(
        const std::string& status,
        const std::vector<GetMessagesResponseMsg>& msgs
    );
    GetMessagesResponse& operator=(const GetMessagesResponse& other);
    virtual ~GetMessagesResponse();

    const std::string& status() const;
    const std::vector<GetMessagesResponseMsg>& msgs() const;

private:
    std::string m_status;
    std::vector<GetMessagesResponseMsg> m_msgs;
};

class GetMessagesResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msgs(const std::vector<GetMessagesResponseMsg>& msgs);

    GetMessagesResponsePtr build() const;

private:
    std::string m_status;
    std::vector<GetMessagesResponseMsg> m_msgs;
};

}
}
}
}

#endif
