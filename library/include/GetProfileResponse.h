#ifndef CALL_RECORDER_API_MODELS_GetProfileResponse_H_
#define CALL_RECORDER_API_MODELS_GetProfileResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "App.h"
#include "GetProfileResponseProfile.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetProfileResponse;
typedef std::shared_ptr<GetProfileResponse> GetProfileResponsePtr;

class GetProfileResponse : public Object
{
public:
    class Builder;

    GetProfileResponse(
        const std::string& status,
        const std::string& code,
        const GetProfileResponseProfilePtr& profile,
        const App::Value& app,
        const std::string& shareUrl,
        const std::string& rateUrl,
        const long& credits,
        const long& creditsTrans
    );
    GetProfileResponse& operator=(const GetProfileResponse& other);
    virtual ~GetProfileResponse();

    const std::string& status() const;
    const std::string& code() const;
    const GetProfileResponseProfilePtr& profile() const;
    const App::Value& app() const;
    const std::string& shareUrl() const;
    const std::string& rateUrl() const;
    const long& credits() const;
    const long& creditsTrans() const;

private:
    std::string m_status;
    std::string m_code;
    GetProfileResponseProfilePtr m_profile;
    App::Value m_app;
    std::string m_shareUrl;
    std::string m_rateUrl;
    long m_credits;
    long m_creditsTrans;
};

class GetProfileResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& code(const std::string& code);
    Builder& profile(const GetProfileResponseProfilePtr& profile);
    Builder& app(const App::Value& app);
    Builder& shareUrl(const std::string& shareUrl);
    Builder& rateUrl(const std::string& rateUrl);
    Builder& credits(const long& credits);
    Builder& creditsTrans(const long& creditsTrans);

    GetProfileResponsePtr build() const;

private:
    std::string m_status;
    std::string m_code;
    GetProfileResponseProfilePtr m_profile;
    App::Value m_app;
    std::string m_shareUrl;
    std::string m_rateUrl;
    long m_credits;
    long m_creditsTrans;
};

}
}
}
}

#endif
