#ifndef CALL_RECORDER_API_MODELS_UpdateFileResponse_H_
#define CALL_RECORDER_API_MODELS_UpdateFileResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateFileResponse;
typedef std::shared_ptr<UpdateFileResponse> UpdateFileResponsePtr;

class UpdateFileResponse : public Object
{
public:
    class Builder;

    UpdateFileResponse(
        const std::string& status,
        const std::string& msg
    );
    UpdateFileResponse& operator=(const UpdateFileResponse& other);
    virtual ~UpdateFileResponse();

    const std::string& status() const;
    const std::string& msg() const;

private:
    std::string m_status;
    std::string m_msg;
};

class UpdateFileResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);

    UpdateFileResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
};

}
}
}
}

#endif
