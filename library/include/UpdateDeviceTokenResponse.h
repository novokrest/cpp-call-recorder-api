#ifndef CALL_RECORDER_API_MODELS_UpdateDeviceTokenResponse_H_
#define CALL_RECORDER_API_MODELS_UpdateDeviceTokenResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateDeviceTokenResponse;
typedef std::shared_ptr<UpdateDeviceTokenResponse> UpdateDeviceTokenResponsePtr;

class UpdateDeviceTokenResponse : public Object
{
public:
    class Builder;

    UpdateDeviceTokenResponse(
        const std::string& status,
        const std::string& msg
    );
    UpdateDeviceTokenResponse& operator=(const UpdateDeviceTokenResponse& other);
    virtual ~UpdateDeviceTokenResponse();

    const std::string& status() const;
    const std::string& msg() const;

private:
    std::string m_status;
    std::string m_msg;
};

class UpdateDeviceTokenResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);

    UpdateDeviceTokenResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
};

}
}
}
}

#endif
