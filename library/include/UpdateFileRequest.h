#ifndef CALL_RECORDER_API_MODELS_UpdateFileRequest_H_
#define CALL_RECORDER_API_MODELS_UpdateFileRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateFileRequest;
typedef std::shared_ptr<UpdateFileRequest> UpdateFileRequestPtr;

class UpdateFileRequest : public Object
{
public:
    class Builder;

    UpdateFileRequest(
        const std::string& apiKey,
        const long& id,
        const std::string& fName,
        const std::string& lName,
        const std::string& notes,
        const std::string& email,
        const std::string& phone,
        const std::string& tags,
        const long& folderId,
        const std::string& name,
        const std::string& remindDays,
        const std::string& remindDate
    );
    UpdateFileRequest& operator=(const UpdateFileRequest& other);
    virtual ~UpdateFileRequest();

    const std::string& apiKey() const;
    const long& id() const;
    const std::string& fName() const;
    const std::string& lName() const;
    const std::string& notes() const;
    const std::string& email() const;
    const std::string& phone() const;
    const std::string& tags() const;
    const long& folderId() const;
    const std::string& name() const;
    const std::string& remindDays() const;
    const std::string& remindDate() const;

private:
    std::string m_apiKey;
    long m_id;
    std::string m_fName;
    std::string m_lName;
    std::string m_notes;
    std::string m_email;
    std::string m_phone;
    std::string m_tags;
    long m_folderId;
    std::string m_name;
    std::string m_remindDays;
    std::string m_remindDate;
};

class UpdateFileRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& id(const long& id);
    Builder& fName(const std::string& fName);
    Builder& lName(const std::string& lName);
    Builder& notes(const std::string& notes);
    Builder& email(const std::string& email);
    Builder& phone(const std::string& phone);
    Builder& tags(const std::string& tags);
    Builder& folderId(const long& folderId);
    Builder& name(const std::string& name);
    Builder& remindDays(const std::string& remindDays);
    Builder& remindDate(const std::string& remindDate);

    UpdateFileRequestPtr build() const;

private:
    std::string m_apiKey;
    long m_id;
    std::string m_fName;
    std::string m_lName;
    std::string m_notes;
    std::string m_email;
    std::string m_phone;
    std::string m_tags;
    long m_folderId;
    std::string m_name;
    std::string m_remindDays;
    std::string m_remindDate;
};

}
}
}
}

#endif
