#ifndef CALL_RECORDER_API_MODELS_CloneFileRequest_H_
#define CALL_RECORDER_API_MODELS_CloneFileRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class CloneFileRequest;
typedef std::shared_ptr<CloneFileRequest> CloneFileRequestPtr;

class CloneFileRequest : public Object
{
public:
    class Builder;

    CloneFileRequest(
        const std::string& apiKey,
        const long& id
    );
    CloneFileRequest& operator=(const CloneFileRequest& other);
    virtual ~CloneFileRequest();

    const std::string& apiKey() const;
    const long& id() const;

private:
    std::string m_apiKey;
    long m_id;
};

class CloneFileRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& id(const long& id);

    CloneFileRequestPtr build() const;

private:
    std::string m_apiKey;
    long m_id;
};

}
}
}
}

#endif
