#ifndef CALL_RECORDER_API_MODELS_GetMetaFilesResponse_H_
#define CALL_RECORDER_API_MODELS_GetMetaFilesResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "GetMetaFilesResponseMetaFile.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetMetaFilesResponse;
typedef std::shared_ptr<GetMetaFilesResponse> GetMetaFilesResponsePtr;

class GetMetaFilesResponse : public Object
{
public:
    class Builder;

    GetMetaFilesResponse(
        const std::string& status,
        const std::vector<GetMetaFilesResponseMetaFile>& metaFiles
    );
    GetMetaFilesResponse& operator=(const GetMetaFilesResponse& other);
    virtual ~GetMetaFilesResponse();

    const std::string& status() const;
    const std::vector<GetMetaFilesResponseMetaFile>& metaFiles() const;

private:
    std::string m_status;
    std::vector<GetMetaFilesResponseMetaFile> m_metaFiles;
};

class GetMetaFilesResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& metaFiles(const std::vector<GetMetaFilesResponseMetaFile>& metaFiles);

    GetMetaFilesResponsePtr build() const;

private:
    std::string m_status;
    std::vector<GetMetaFilesResponseMetaFile> m_metaFiles;
};

}
}
}
}

#endif
