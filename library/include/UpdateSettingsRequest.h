#ifndef CALL_RECORDER_API_MODELS_UpdateSettingsRequest_H_
#define CALL_RECORDER_API_MODELS_UpdateSettingsRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "FilesPermission.h"
#include "PlayBeep.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateSettingsRequest;
typedef std::shared_ptr<UpdateSettingsRequest> UpdateSettingsRequestPtr;

class UpdateSettingsRequest : public Object
{
public:
    class Builder;

    UpdateSettingsRequest(
        const std::string& apiKey,
        const PlayBeep::Value& playBeep,
        const FilesPermission::Value& filesPermission
    );
    UpdateSettingsRequest& operator=(const UpdateSettingsRequest& other);
    virtual ~UpdateSettingsRequest();

    const std::string& apiKey() const;
    const PlayBeep::Value& playBeep() const;
    const FilesPermission::Value& filesPermission() const;

private:
    std::string m_apiKey;
    PlayBeep::Value m_playBeep;
    FilesPermission::Value m_filesPermission;
};

class UpdateSettingsRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& playBeep(const PlayBeep::Value& playBeep);
    Builder& filesPermission(const FilesPermission::Value& filesPermission);

    UpdateSettingsRequestPtr build() const;

private:
    std::string m_apiKey;
    PlayBeep::Value m_playBeep;
    FilesPermission::Value m_filesPermission;
};

}
}
}
}

#endif
