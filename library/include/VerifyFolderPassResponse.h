#ifndef CALL_RECORDER_API_MODELS_VerifyFolderPassResponse_H_
#define CALL_RECORDER_API_MODELS_VerifyFolderPassResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class VerifyFolderPassResponse;
typedef std::shared_ptr<VerifyFolderPassResponse> VerifyFolderPassResponsePtr;

class VerifyFolderPassResponse : public Object
{
public:
    class Builder;

    VerifyFolderPassResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code
    );
    VerifyFolderPassResponse& operator=(const VerifyFolderPassResponse& other);
    virtual ~VerifyFolderPassResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

class VerifyFolderPassResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);

    VerifyFolderPassResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

}
}
}
}

#endif
