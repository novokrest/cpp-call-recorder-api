#ifndef CALL_RECORDER_API_MODELS_UpdateProfileRequest_H_
#define CALL_RECORDER_API_MODELS_UpdateProfileRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "UpdateProfileRequestData.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateProfileRequest;
typedef std::shared_ptr<UpdateProfileRequest> UpdateProfileRequestPtr;

class UpdateProfileRequest : public Object
{
public:
    class Builder;

    UpdateProfileRequest(
        const std::string& apiKey,
        const UpdateProfileRequestDataPtr& data
    );
    UpdateProfileRequest& operator=(const UpdateProfileRequest& other);
    virtual ~UpdateProfileRequest();

    const std::string& apiKey() const;
    const UpdateProfileRequestDataPtr& data() const;

private:
    std::string m_apiKey;
    UpdateProfileRequestDataPtr m_data;
};

class UpdateProfileRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& data(const UpdateProfileRequestDataPtr& data);

    UpdateProfileRequestPtr build() const;

private:
    std::string m_apiKey;
    UpdateProfileRequestDataPtr m_data;
};

}
}
}
}

#endif
