#ifndef CALL_RECORDER_API_MODELS_DeleteFilesResponse_H_
#define CALL_RECORDER_API_MODELS_DeleteFilesResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class DeleteFilesResponse;
typedef std::shared_ptr<DeleteFilesResponse> DeleteFilesResponsePtr;

class DeleteFilesResponse : public Object
{
public:
    class Builder;

    DeleteFilesResponse(
        const std::string& status,
        const std::string& msg
    );
    DeleteFilesResponse& operator=(const DeleteFilesResponse& other);
    virtual ~DeleteFilesResponse();

    const std::string& status() const;
    const std::string& msg() const;

private:
    std::string m_status;
    std::string m_msg;
};

class DeleteFilesResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);

    DeleteFilesResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
};

}
}
}
}

#endif
