#ifndef CALL_RECORDER_API_MODELS_RegisterPhoneResponse_H_
#define CALL_RECORDER_API_MODELS_RegisterPhoneResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class RegisterPhoneResponse;
typedef std::shared_ptr<RegisterPhoneResponse> RegisterPhoneResponsePtr;

class RegisterPhoneResponse : public Object
{
public:
    class Builder;

    RegisterPhoneResponse(
        const std::string& status,
        const std::string& phone,
        const std::string& code,
        const std::string& msg
    );
    RegisterPhoneResponse& operator=(const RegisterPhoneResponse& other);
    virtual ~RegisterPhoneResponse();

    const std::string& status() const;
    const std::string& phone() const;
    const std::string& code() const;
    const std::string& msg() const;

private:
    std::string m_status;
    std::string m_phone;
    std::string m_code;
    std::string m_msg;
};

class RegisterPhoneResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& phone(const std::string& phone);
    Builder& code(const std::string& code);
    Builder& msg(const std::string& msg);

    RegisterPhoneResponsePtr build() const;

private:
    std::string m_status;
    std::string m_phone;
    std::string m_code;
    std::string m_msg;
};

}
}
}
}

#endif
