#ifndef CALL_RECORDER_API_MODELS_UpdateOrderRequest_H_
#define CALL_RECORDER_API_MODELS_UpdateOrderRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "UpdateOrderRequestFolder.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateOrderRequest;
typedef std::shared_ptr<UpdateOrderRequest> UpdateOrderRequestPtr;

class UpdateOrderRequest : public Object
{
public:
    class Builder;

    UpdateOrderRequest(
        const std::string& apiKey,
        const std::vector<UpdateOrderRequestFolder>& folders
    );
    UpdateOrderRequest& operator=(const UpdateOrderRequest& other);
    virtual ~UpdateOrderRequest();

    const std::string& apiKey() const;
    const std::vector<UpdateOrderRequestFolder>& folders() const;

private:
    std::string m_apiKey;
    std::vector<UpdateOrderRequestFolder> m_folders;
};

class UpdateOrderRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& folders(const std::vector<UpdateOrderRequestFolder>& folders);

    UpdateOrderRequestPtr build() const;

private:
    std::string m_apiKey;
    std::vector<UpdateOrderRequestFolder> m_folders;
};

}
}
}
}

#endif
