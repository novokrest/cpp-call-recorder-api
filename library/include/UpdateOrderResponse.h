#ifndef CALL_RECORDER_API_MODELS_UpdateOrderResponse_H_
#define CALL_RECORDER_API_MODELS_UpdateOrderResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateOrderResponse;
typedef std::shared_ptr<UpdateOrderResponse> UpdateOrderResponsePtr;

class UpdateOrderResponse : public Object
{
public:
    class Builder;

    UpdateOrderResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code
    );
    UpdateOrderResponse& operator=(const UpdateOrderResponse& other);
    virtual ~UpdateOrderResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

class UpdateOrderResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);

    UpdateOrderResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
};

}
}
}
}

#endif
