#ifndef CALL_RECORDER_API_MODELS_CreateFileRequest_H_
#define CALL_RECORDER_API_MODELS_CreateFileRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "CreateFileData.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class CreateFileRequest;
typedef std::shared_ptr<CreateFileRequest> CreateFileRequestPtr;

class CreateFileRequest : public Object
{
public:
    class Builder;

    CreateFileRequest(
        const std::string& apiKey,
        const std::string& filePath,
        const CreateFileDataPtr& data
    );
    CreateFileRequest& operator=(const CreateFileRequest& other);
    virtual ~CreateFileRequest();

    const std::string& apiKey() const;
    const std::string& filePath() const;
    const CreateFileDataPtr& data() const;

private:
    std::string m_apiKey;
    std::string m_filePath;
    CreateFileDataPtr m_data;
};

class CreateFileRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& filePath(const std::string& file);
    Builder& data(const CreateFileDataPtr& data);

    CreateFileRequestPtr build() const;

private:
    std::string m_apiKey;
    std::string m_filePath;
    CreateFileDataPtr m_data;
};

}
}
}
}

#endif
