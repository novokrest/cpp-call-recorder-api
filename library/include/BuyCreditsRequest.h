#ifndef CALL_RECORDER_API_MODELS_BuyCreditsRequest_H_
#define CALL_RECORDER_API_MODELS_BuyCreditsRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "DeviceType.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class BuyCreditsRequest;
typedef std::shared_ptr<BuyCreditsRequest> BuyCreditsRequestPtr;

class BuyCreditsRequest : public Object
{
public:
    class Builder;

    BuyCreditsRequest(
        const std::string& apiKey,
        const long& amount,
        const std::string& receipt,
        const long& productId,
        const DeviceType::Value& deviceType
    );
    BuyCreditsRequest& operator=(const BuyCreditsRequest& other);
    virtual ~BuyCreditsRequest();

    const std::string& apiKey() const;
    const long& amount() const;
    const std::string& receipt() const;
    const long& productId() const;
    const DeviceType::Value& deviceType() const;

private:
    std::string m_apiKey;
    long m_amount;
    std::string m_receipt;
    long m_productId;
    DeviceType::Value m_deviceType;
};

class BuyCreditsRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& amount(const long& amount);
    Builder& receipt(const std::string& receipt);
    Builder& productId(const long& productId);
    Builder& deviceType(const DeviceType::Value& deviceType);

    BuyCreditsRequestPtr build() const;

private:
    std::string m_apiKey;
    long m_amount;
    std::string m_receipt;
    long m_productId;
    DeviceType::Value m_deviceType;
};

}
}
}
}

#endif
