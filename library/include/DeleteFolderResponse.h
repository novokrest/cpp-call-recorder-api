#ifndef CALL_RECORDER_API_MODELS_DeleteFolderResponse_H_
#define CALL_RECORDER_API_MODELS_DeleteFolderResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class DeleteFolderResponse;
typedef std::shared_ptr<DeleteFolderResponse> DeleteFolderResponsePtr;

class DeleteFolderResponse : public Object
{
public:
    class Builder;

    DeleteFolderResponse(
        const std::string& status,
        const std::string& msg
    );
    DeleteFolderResponse& operator=(const DeleteFolderResponse& other);
    virtual ~DeleteFolderResponse();

    const std::string& status() const;
    const std::string& msg() const;

private:
    std::string m_status;
    std::string m_msg;
};

class DeleteFolderResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);

    DeleteFolderResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
};

}
}
}
}

#endif
