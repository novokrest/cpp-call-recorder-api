#ifndef CALL_RECORDER_API_MODELS_UpdateProfileImgRequest_H_
#define CALL_RECORDER_API_MODELS_UpdateProfileImgRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateProfileImgRequest;
typedef std::shared_ptr<UpdateProfileImgRequest> UpdateProfileImgRequestPtr;

class UpdateProfileImgRequest : public Object
{
public:
    class Builder;

    UpdateProfileImgRequest(
        const std::string& apiKey,
        const std::string& filePath
    );
    UpdateProfileImgRequest& operator=(const UpdateProfileImgRequest& other);
    virtual ~UpdateProfileImgRequest();

    const std::string& apiKey() const;
    const std::string& filePath() const;

private:
    std::string m_apiKey;
    std::string m_filePath;
};

class UpdateProfileImgRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& filePath(const std::string& filePath);

    UpdateProfileImgRequestPtr build() const;

private:
    std::string m_apiKey;
    std::string m_filePath;
};

}
}
}
}

#endif
