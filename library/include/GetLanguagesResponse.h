#ifndef CALL_RECORDER_API_MODELS_GetLanguagesResponse_H_
#define CALL_RECORDER_API_MODELS_GetLanguagesResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "Language.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetLanguagesResponse;
typedef std::shared_ptr<GetLanguagesResponse> GetLanguagesResponsePtr;

class GetLanguagesResponse : public Object
{
public:
    class Builder;

    GetLanguagesResponse(
        const std::string& status,
        const std::string& msg,
        const std::vector<Language>& languages
    );
    GetLanguagesResponse& operator=(const GetLanguagesResponse& other);
    virtual ~GetLanguagesResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::vector<Language>& languages() const;

private:
    std::string m_status;
    std::string m_msg;
    std::vector<Language> m_languages;
};

class GetLanguagesResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& languages(const std::vector<Language>& languages);

    GetLanguagesResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::vector<Language> m_languages;
};

}
}
}
}

#endif
