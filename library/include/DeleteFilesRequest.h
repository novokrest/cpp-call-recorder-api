#ifndef CALL_RECORDER_API_MODELS_DeleteFilesRequest_H_
#define CALL_RECORDER_API_MODELS_DeleteFilesRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class DeleteFilesRequest;
typedef std::shared_ptr<DeleteFilesRequest> DeleteFilesRequestPtr;

class DeleteFilesRequest : public Object
{
public:
    class Builder;

    DeleteFilesRequest(
        const std::string& apiKey,
        const std::vector<long>& ids,
        const std::string& action
    );
    DeleteFilesRequest& operator=(const DeleteFilesRequest& other);
    virtual ~DeleteFilesRequest();

    const std::string& apiKey() const;
    const std::vector<long>& ids() const;
    const std::string& action() const;

private:
    std::string m_apiKey;
    std::vector<long> m_ids;
    std::string m_action;
};

class DeleteFilesRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& ids(const std::vector<long>& ids);
    Builder& action(const std::string& action);

    DeleteFilesRequestPtr build() const;

private:
    std::string m_apiKey;
    std::vector<long> m_ids;
    std::string m_action;
};

}
}
}
}

#endif
