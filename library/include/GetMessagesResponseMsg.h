#ifndef CALL_RECORDER_API_MODELS_GetMessagesResponseMsg_H_
#define CALL_RECORDER_API_MODELS_GetMessagesResponseMsg_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetMessagesResponseMsg;
typedef std::shared_ptr<GetMessagesResponseMsg> GetMessagesResponseMsgPtr;

class GetMessagesResponseMsg : public Object
{
public:
    class Builder;

    GetMessagesResponseMsg(
        const std::string& id,
        const std::string& title,
        const std::string& body,
        const std::string& time
    );
    GetMessagesResponseMsg& operator=(const GetMessagesResponseMsg& other);
    virtual ~GetMessagesResponseMsg();

    const std::string& id() const;
    const std::string& title() const;
    const std::string& body() const;
    const std::string& time() const;

private:
    std::string m_id;
    std::string m_title;
    std::string m_body;
    std::string m_time;
};

class GetMessagesResponseMsg::Builder
{
public:
    Builder& id(const std::string& id);
    Builder& title(const std::string& title);
    Builder& body(const std::string& body);
    Builder& time(const std::string& time);

    GetMessagesResponseMsgPtr build() const;

private:
    std::string m_id;
    std::string m_title;
    std::string m_body;
    std::string m_time;
};

}
}
}
}

#endif
