#ifndef CALL_RECORDER_API_MODELS_UploadMetaFileRequest_H_
#define CALL_RECORDER_API_MODELS_UploadMetaFileRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UploadMetaFileRequest;
typedef std::shared_ptr<UploadMetaFileRequest> UploadMetaFileRequestPtr;

class UploadMetaFileRequest : public Object
{
public:
    class Builder;

    UploadMetaFileRequest(
        const std::string& apiKey,
        const std::string& filePath,
        const std::string& name,
        const long& parentId,
        const long& id
    );
    UploadMetaFileRequest& operator=(const UploadMetaFileRequest& other);
    virtual ~UploadMetaFileRequest();

    const std::string& apiKey() const;
    const std::string& filePath() const;
    const std::string& name() const;
    const long& parentId() const;
    const long& id() const;

private:
    std::string m_apiKey;
    std::string m_filePath;
    std::string m_name;
    long m_parentId;
    long m_id;
};

class UploadMetaFileRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& filePath(const std::string& filePath);
    Builder& name(const std::string& name);
    Builder& parentId(const long& parentId);
    Builder& id(const long& id);

    UploadMetaFileRequestPtr build() const;

private:
    std::string m_apiKey;
    std::string m_filePath;
    std::string m_name;
    long m_parentId;
    long m_id;
};

}
}
}
}

#endif
