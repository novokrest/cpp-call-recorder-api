#ifndef CALL_RECORDER_API_MODELS_CloneFileResponse_H_
#define CALL_RECORDER_API_MODELS_CloneFileResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class CloneFileResponse;
typedef std::shared_ptr<CloneFileResponse> CloneFileResponsePtr;

class CloneFileResponse : public Object
{
public:
    class Builder;

    CloneFileResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code,
        const long& id
    );
    CloneFileResponse& operator=(const CloneFileResponse& other);
    virtual ~CloneFileResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;
    const long& id() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
    long m_id;
};

class CloneFileResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);
    Builder& id(const long& id);

    CloneFileResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
    long m_id;
};

}
}
}
}

#endif
