#ifndef CALL_RECORDER_API_MODELS_App_H_
#define CALL_RECORDER_API_MODELS_App_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class App
{
    public:
    enum class Value
    {
        FREE,
        REM,
        REC,
    };

    static std::string toString(const Value value);
    static Value fromString(const std::string& value);

    private:
        App();
};

}
}
}
}

#endif
