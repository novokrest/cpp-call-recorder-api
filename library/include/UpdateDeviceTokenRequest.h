#ifndef CALL_RECORDER_API_MODELS_UpdateDeviceTokenRequest_H_
#define CALL_RECORDER_API_MODELS_UpdateDeviceTokenRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"
#include "DeviceType.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class UpdateDeviceTokenRequest;
typedef std::shared_ptr<UpdateDeviceTokenRequest> UpdateDeviceTokenRequestPtr;

class UpdateDeviceTokenRequest : public Object
{
public:
    class Builder;

    UpdateDeviceTokenRequest(
        const std::string& apiKey,
        const std::string& deviceToken,
        const DeviceType::Value& deviceType
    );
    UpdateDeviceTokenRequest& operator=(const UpdateDeviceTokenRequest& other);
    virtual ~UpdateDeviceTokenRequest();

    const std::string& apiKey() const;
    const std::string& deviceToken() const;
    const DeviceType::Value& deviceType() const;

private:
    std::string m_apiKey;
    std::string m_deviceToken;
    DeviceType::Value m_deviceType;
};

class UpdateDeviceTokenRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);
    Builder& deviceToken(const std::string& deviceToken);
    Builder& deviceType(const DeviceType::Value& deviceType);

    UpdateDeviceTokenRequestPtr build() const;

private:
    std::string m_apiKey;
    std::string m_deviceToken;
    DeviceType::Value m_deviceType;
};

}
}
}
}

#endif
