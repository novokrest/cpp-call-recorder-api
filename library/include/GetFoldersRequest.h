#ifndef CALL_RECORDER_API_MODELS_GetFoldersRequest_H_
#define CALL_RECORDER_API_MODELS_GetFoldersRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetFoldersRequest;
typedef std::shared_ptr<GetFoldersRequest> GetFoldersRequestPtr;

class GetFoldersRequest : public Object
{
public:
    class Builder;

    GetFoldersRequest(
        const std::string& apiKey
    );
    GetFoldersRequest& operator=(const GetFoldersRequest& other);
    virtual ~GetFoldersRequest();

    const std::string& apiKey() const;

private:
    std::string m_apiKey;
};

class GetFoldersRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);

    GetFoldersRequestPtr build() const;

private:
    std::string m_apiKey;
};

}
}
}
}

#endif
