#ifndef CALL_RECORDER_API_MODELS_GetTranslationsResponse_H_
#define CALL_RECORDER_API_MODELS_GetTranslationsResponse_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetTranslationsResponse;
typedef std::shared_ptr<GetTranslationsResponse> GetTranslationsResponsePtr;

class GetTranslationsResponse : public Object
{
public:
    class Builder;

    GetTranslationsResponse(
        const std::string& status,
        const std::string& msg,
        const std::string& code,
        const std::map<std::string, std::string>& translation
    );
    GetTranslationsResponse& operator=(const GetTranslationsResponse& other);
    virtual ~GetTranslationsResponse();

    const std::string& status() const;
    const std::string& msg() const;
    const std::string& code() const;
    const std::map<std::string, std::string>& translation() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
    std::map<std::string, std::string> m_translation;
};

class GetTranslationsResponse::Builder
{
public:
    Builder& status(const std::string& status);
    Builder& msg(const std::string& msg);
    Builder& code(const std::string& code);
    Builder& translation(const std::map<std::string, std::string>& translation);

    GetTranslationsResponsePtr build() const;

private:
    std::string m_status;
    std::string m_msg;
    std::string m_code;
    std::map<std::string, std::string> m_translation;
};

}
}
}
}

#endif
