#ifndef CALL_RECORDER_API_MODELS_GetLanguagesRequest_H_
#define CALL_RECORDER_API_MODELS_GetLanguagesRequest_H_

#include <string>
#include <memory>
#include <vector>

#include "Object.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

class GetLanguagesRequest;
typedef std::shared_ptr<GetLanguagesRequest> GetLanguagesRequestPtr;

class GetLanguagesRequest : public Object
{
public:
    class Builder;

    GetLanguagesRequest(
        const std::string& apiKey
    );
    GetLanguagesRequest& operator=(const GetLanguagesRequest& other);
    virtual ~GetLanguagesRequest();

    const std::string& apiKey() const;

private:
    std::string m_apiKey;
};

class GetLanguagesRequest::Builder
{
public:
    Builder& apiKey(const std::string& apiKey);

    GetLanguagesRequestPtr build() const;

private:
    std::string m_apiKey;
};

}
}
}
}

#endif
