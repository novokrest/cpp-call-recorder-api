#include "RecoverFileResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

RecoverFileResponse::RecoverFileResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code
) : m_status(status), m_msg(msg), m_code(code)
{

}

RecoverFileResponse& RecoverFileResponse::operator=(const RecoverFileResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    return *this;
}

RecoverFileResponse::~RecoverFileResponse()
{

}

const std::string& RecoverFileResponse::status() const
{
    return m_status;
}
const std::string& RecoverFileResponse::msg() const
{
    return m_msg;
}
const std::string& RecoverFileResponse::code() const
{
    return m_code;
}

RecoverFileResponse::Builder& RecoverFileResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
RecoverFileResponse::Builder& RecoverFileResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
RecoverFileResponse::Builder& RecoverFileResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}

RecoverFileResponsePtr RecoverFileResponse::Builder::build() const
{
    return std::make_shared<RecoverFileResponse>(
        m_status,
        m_msg,
        m_code
    );
}

}
}
}
}
