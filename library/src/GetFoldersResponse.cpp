#include "GetFoldersResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetFoldersResponse::GetFoldersResponse(
    const std::string& status,
    const std::string& msg,
    const std::vector<GetFoldersResponseFolder>& folders
) : m_status(status), m_msg(msg), m_folders(folders)
{

}

GetFoldersResponse& GetFoldersResponse::operator=(const GetFoldersResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_folders = other.m_folders;
    return *this;
}

GetFoldersResponse::~GetFoldersResponse()
{

}

const std::string& GetFoldersResponse::status() const
{
    return m_status;
}
const std::string& GetFoldersResponse::msg() const
{
    return m_msg;
}
const std::vector<GetFoldersResponseFolder>& GetFoldersResponse::folders() const
{
    return m_folders;
}

GetFoldersResponse::Builder& GetFoldersResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
GetFoldersResponse::Builder& GetFoldersResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
GetFoldersResponse::Builder& GetFoldersResponse::Builder::folders(const std::vector<GetFoldersResponseFolder>& folders)
{
    m_folders = folders;
    return *this;
}

GetFoldersResponsePtr GetFoldersResponse::Builder::build() const
{
    return std::make_shared<GetFoldersResponse>(
        m_status,
        m_msg,
        m_folders
    );
}

}
}
}
}
