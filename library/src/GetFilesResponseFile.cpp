#include "GetFilesResponseFile.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetFilesResponseFile::GetFilesResponseFile(
    const long& id,
    const std::string& accessNumber,
    const std::string& name,
    const std::string& fName,
    const std::string& lName,
    const std::string& email,
    const std::string& phone,
    const std::string& notes,
    const std::string& meta,
    const std::string& source,
    const std::string& url,
    const std::string& credits,
    const std::string& duration,
    const std::string& time,
    const std::string& shareUrl,
    const std::string& downloadUrl
) : m_id(id), m_accessNumber(accessNumber), m_name(name), m_fName(fName), m_lName(lName), m_email(email), m_phone(phone), m_notes(notes), m_meta(meta), m_source(source), m_url(url), m_credits(credits), m_duration(duration), m_time(time), m_shareUrl(shareUrl), m_downloadUrl(downloadUrl)
{

}

GetFilesResponseFile& GetFilesResponseFile::operator=(const GetFilesResponseFile& other)
{
    this->m_id = other.m_id;
    this->m_accessNumber = other.m_accessNumber;
    this->m_name = other.m_name;
    this->m_fName = other.m_fName;
    this->m_lName = other.m_lName;
    this->m_email = other.m_email;
    this->m_phone = other.m_phone;
    this->m_notes = other.m_notes;
    this->m_meta = other.m_meta;
    this->m_source = other.m_source;
    this->m_url = other.m_url;
    this->m_credits = other.m_credits;
    this->m_duration = other.m_duration;
    this->m_time = other.m_time;
    this->m_shareUrl = other.m_shareUrl;
    this->m_downloadUrl = other.m_downloadUrl;
    return *this;
}

GetFilesResponseFile::~GetFilesResponseFile()
{

}

const long& GetFilesResponseFile::id() const
{
    return m_id;
}
const std::string& GetFilesResponseFile::accessNumber() const
{
    return m_accessNumber;
}
const std::string& GetFilesResponseFile::name() const
{
    return m_name;
}
const std::string& GetFilesResponseFile::fName() const
{
    return m_fName;
}
const std::string& GetFilesResponseFile::lName() const
{
    return m_lName;
}
const std::string& GetFilesResponseFile::email() const
{
    return m_email;
}
const std::string& GetFilesResponseFile::phone() const
{
    return m_phone;
}
const std::string& GetFilesResponseFile::notes() const
{
    return m_notes;
}
const std::string& GetFilesResponseFile::meta() const
{
    return m_meta;
}
const std::string& GetFilesResponseFile::source() const
{
    return m_source;
}
const std::string& GetFilesResponseFile::url() const
{
    return m_url;
}
const std::string& GetFilesResponseFile::credits() const
{
    return m_credits;
}
const std::string& GetFilesResponseFile::duration() const
{
    return m_duration;
}
const std::string& GetFilesResponseFile::time() const
{
    return m_time;
}
const std::string& GetFilesResponseFile::shareUrl() const
{
    return m_shareUrl;
}
const std::string& GetFilesResponseFile::downloadUrl() const
{
    return m_downloadUrl;
}

GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::accessNumber(const std::string& accessNumber)
{
    m_accessNumber = accessNumber;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::name(const std::string& name)
{
    m_name = name;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::fName(const std::string& fName)
{
    m_fName = fName;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::lName(const std::string& lName)
{
    m_lName = lName;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::email(const std::string& email)
{
    m_email = email;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::phone(const std::string& phone)
{
    m_phone = phone;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::notes(const std::string& notes)
{
    m_notes = notes;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::meta(const std::string& meta)
{
    m_meta = meta;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::source(const std::string& source)
{
    m_source = source;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::url(const std::string& url)
{
    m_url = url;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::credits(const std::string& credits)
{
    m_credits = credits;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::duration(const std::string& duration)
{
    m_duration = duration;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::time(const std::string& time)
{
    m_time = time;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::shareUrl(const std::string& shareUrl)
{
    m_shareUrl = shareUrl;
    return *this;
}
GetFilesResponseFile::Builder& GetFilesResponseFile::Builder::downloadUrl(const std::string& downloadUrl)
{
    m_downloadUrl = downloadUrl;
    return *this;
}

GetFilesResponseFilePtr GetFilesResponseFile::Builder::build() const
{
    return std::make_shared<GetFilesResponseFile>(
        m_id,
        m_accessNumber,
        m_name,
        m_fName,
        m_lName,
        m_email,
        m_phone,
        m_notes,
        m_meta,
        m_source,
        m_url,
        m_credits,
        m_duration,
        m_time,
        m_shareUrl,
        m_downloadUrl
    );
}

}
}
}
}
