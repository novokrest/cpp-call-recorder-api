#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "CallRecorderApi.h"

#include <iostream>

using namespace call::recorder::api;

//static std::string URL = "http://localhost:9000";
static std::string URL = "https://app2.virtualbrix.net";
static std::string API_TOKEN = "55942ee3894f51000530894";
static std::string PHONE_NUMBER = "+16463742122";
static std::string DEVICE_TOKEN = "871284c348e04a9cacab8aca6b2f3c9a";
static std::string FOLDER_PASS = "1235";
static std::string SUCCESS_STATUS = "ok";

static std::string retrieveApiKey(CallRecorderApiPtr& api);

static long createFolder(CallRecorderApiPtr& api, const std::string& apiKey)
{
    auto request = CreateFolderRequest::Builder{}
        .apiKey(apiKey)
        .name("test-folder")
        .pass(FOLDER_PASS)
        .build();
    auto response = api->createFolder(*request);
    return response->id();
}

static long uploadMetaFile(CallRecorderApiPtr& api, const std::string& apiKey, const long fileId)
{
    auto request = UploadMetaFileRequest::Builder{}
        .apiKey(apiKey)
        .filePath("pict.png")
        .name("test-meta")
        .parentId(fileId)
        .build();
    auto response = api->uploadMetaFile(*request);
    return response->id();
}

static long createFile(CallRecorderApiPtr& api, const std::string& apiKey)
{
    auto request = CreateFileRequest::Builder{}
        .apiKey(apiKey)
        .filePath("audio.mp3")
        .data(CreateFileData::Builder{}
            .name("test-file")
            .notes("test-notes")
            .remindDays("10")
            .remindDate("2019-09-03T21:11:51.824121+03:00")
            .build())
        .build();
    CreateFileResponsePtr response = api->createFile(*request);
    return response->id();
}

static long createFile(CallRecorderApiPtr& api)
{
    std::string testApiKey = retrieveApiKey(api);
    return createFile(api, testApiKey);
}

static std::string getVerificationCode(CallRecorderApiPtr& api)
{
    RegisterPhoneRequestPtr request = RegisterPhoneRequest::Builder{}
        .token(API_TOKEN)
        .phone(PHONE_NUMBER)
        .build();
    std::cout << "Registering phone: " << std::endl;
    RegisterPhoneResponsePtr registerPhoneResponse = api->registerPhone(*request);
    std::string code = registerPhoneResponse->code();
    std::cout << "Verification code: " << code << std::endl;
    return code;
}

static std::string retrieveApiKey(CallRecorderApiPtr& api)
{
    std::string code = getVerificationCode(api);

    VerifyPhoneRequestPtr request = VerifyPhoneRequest::Builder{}
        .token(API_TOKEN)
        .phone(PHONE_NUMBER)
        .code(code)
        .mcc("300")
        .app(App::Value::REC)
        .deviceType(DeviceType::Value::IOS)
        .deviceToken(DEVICE_TOKEN)
        .deviceId(DEVICE_TOKEN)
        .timeZone("10")
        .build();
    std::cout << "Verify phone: " << std::endl;
    VerifyPhoneResponsePtr verifyPhoneResponse = api->verifyPhone(*request);
    std::string apiKey = verifyPhoneResponse->apiKey();
    std::cout << "Verified: " << apiKey << std::endl;

    return apiKey;
}


SCENARIO("should send request to /buy_credits successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        BuyCreditsRequestPtr request = BuyCreditsRequest::Builder{}
            .apiKey(testApiKey)
            .amount(100)
            .receipt("test")
            .deviceType(DeviceType::Value::IOS)
            .productId(1)
            .build();

        WHEN("call to /buy_credits") {
            try
            {
                api->buyCredits(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            BuyCreditsResponsePtr response = api->buyCredits(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
            }
        }
    }
} 

SCENARIO("should send request to /clone_file successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long fileId = createFile(api, testApiKey);
        CloneFileRequestPtr request = CloneFileRequest::Builder{}
            .apiKey(testApiKey)
            .id(fileId)
            .build();

        WHEN("call to /clone_file") {
            try
            {
                api->cloneFile(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            CloneFileResponsePtr response = api->cloneFile(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
                REQUIRE(response->code() == "file_cloned");
                REQUIRE(response->id() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /create_file successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        auto request = CreateFileRequest::Builder{}
            .apiKey(testApiKey)
            .filePath("audio.mp3")
            .data(CreateFileData::Builder{}
                .name("test-file")
                .notes("test-notes")
                .remindDays("10")
                .remindDate("2019-10-11")
                .build())
            .build();

        WHEN("call to /create_file") {
            try
            {
                api->createFile(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            CreateFileResponsePtr response = api->createFile(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->id() > 0);
                REQUIRE(response->msg().find("Success") != std::string::npos);
            }
        }
    }
} 

SCENARIO("should send request to /create_folder successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        CreateFolderRequestPtr request = CreateFolderRequest::Builder{}
            .apiKey(testApiKey)
            .name("test-folder")
            .pass(FOLDER_PASS)
            .build();

        WHEN("call to /create_folder") {
            try
            {
                api->createFolder(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            CreateFolderResponsePtr response = api->createFolder(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
                REQUIRE(response->id() > 0);
                REQUIRE(response->code().length() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /delete_files successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long fileId = createFile(api, testApiKey);
        DeleteFilesRequestPtr request = DeleteFilesRequest::Builder{}
            .apiKey(testApiKey)
            .ids(std::vector<long>{ fileId })
            .build();

        WHEN("call to /delete_files") {
            DeleteFilesResponsePtr response;
            try
            {
                response = api->deleteFiles(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
            }
        }
    }
} 

SCENARIO("should send request to /delete_folder successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long folderId = createFolder(api, testApiKey);

        DeleteFolderRequestPtr request = DeleteFolderRequest::Builder{}
            .apiKey(testApiKey)
            .id(folderId)
            .build();

        WHEN("call to /delete_folder") {
            DeleteFolderResponsePtr response;
            try
            {
                response = api->deleteFolder(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
            }
        }
    }
} 

SCENARIO("should send request to /delete_meta_files successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long testFileId = createFile(api, testApiKey);
        long testMetaFileId = uploadMetaFile(api, testApiKey, testFileId);
        DeleteMetaFilesRequestPtr request = DeleteMetaFilesRequest::Builder{}
            .apiKey(testApiKey)
            .ids(std::vector<long> { testMetaFileId })
            .parentId(testFileId)
            .build();

        WHEN("call to /delete_meta_files") {
            DeleteMetaFilesResponsePtr response;
            try
            {
                response = api->deleteMetaFiles(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("deleted") != std::string::npos);
            }
        }
    }
} 

SCENARIO("should send request to /get_files successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        GetFilesRequestPtr request = GetFilesRequest::Builder{}
            .apiKey(testApiKey)
            .page("0")
            .folderId(0)
            .source("all")
            .pass("0")
            .reminder(false)
            .q("hello")
            .id(0)
            .op("greater")
            .build();

        WHEN("call to /get_files") {
            GetFilesResponsePtr response;
            try
            {
                response = api->getFiles(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->credits() == 0);
                REQUIRE(response->creditsTrans() == 0);
                REQUIRE(response->files().size() == 0);
            }
        }
    }
} 

SCENARIO("should send request to /get_folders successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        GetFoldersRequestPtr request = GetFoldersRequest::Builder{}
            .apiKey(testApiKey)
            .build();

        WHEN("call to /get_folders") {
            GetFoldersResponsePtr response;
            try
            {
                response = api->getFolders(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
                REQUIRE(response->folders().size() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /get_languages successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        GetLanguagesRequestPtr request = GetLanguagesRequest::Builder{}
            .apiKey(testApiKey)
            .build();

        WHEN("call to /get_languages") {
            GetLanguagesResponsePtr response;
            try
            {
                response = api->getLanguages(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().length() == 0);
                REQUIRE(response->languages().size() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /get_meta_files successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long testFileId = createFile(api, testApiKey);
        long testMetaFileId = uploadMetaFile(api, testApiKey, testFileId);
        GetMetaFilesRequestPtr request = GetMetaFilesRequest::Builder{}
            .apiKey(testApiKey)
            .parentId(testFileId)
            .build();

        WHEN("call to /get_meta_files") {
            GetMetaFilesResponsePtr response;
            try
            {
                response = api->getMetaFiles(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->metaFiles().size() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /get_msgs successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        GetMessagesRequestPtr request = GetMessagesRequest::Builder{}
            .apiKey(testApiKey)
            .build();

        WHEN("call to /get_msgs") {
            GetMessagesResponsePtr response;
            try
            {
                response = api->getMsgs(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msgs().size() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /get_phones successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        GetPhonesRequestPtr request = GetPhonesRequest::Builder{}
            .apiKey(testApiKey)
            .build();

        WHEN("call to /get_phones") {
            GetPhonesResponsePtr response;
            try
            {
                response = api->getPhones(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            THEN("registered successfully") {
                REQUIRE(response->phones().size() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /get_profile successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        GetProfileRequestPtr request = GetProfileRequest::Builder{}
            .apiKey(testApiKey)
            .build();

        WHEN("call to /get_profile") {
            try
            {
                api->getProfile(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            GetProfileResponsePtr response = api->getProfile(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->code() == "user_profile");
                REQUIRE(response->profile() != nullptr);                
            }
        }
    }
} 

SCENARIO("should send request to /get_settings successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        GetSettingsRequestPtr request = GetSettingsRequest::Builder{}
            .apiKey(testApiKey)
            .build();

        WHEN("call to /get_settings") {
            try
            {
                api->getSettings(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            GetSettingsResponsePtr response = api->getSettings(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->app() == App::Value::REC);
                REQUIRE(response->credits() == 0);
                REQUIRE(response->settings() != nullptr);
                REQUIRE(response->settings()->playBeep() == PlayBeep::Value::NO);
                REQUIRE(response->settings()->filesPermission() == FilesPermission::Value::PRIVATE);
            }
        }
    }
} 

SCENARIO("should send request to /get_translations successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        GetTranslationsRequestPtr request = GetTranslationsRequest::Builder{}
            .apiKey(testApiKey)
            .language("en_US")
            .build();

        WHEN("call to /get_translations") {
            try
            {
                api->getTranslations(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            GetTranslationsResponsePtr response = api->getTranslations(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->translation().size() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /notify_user_custom successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        NotifyUserRequestPtr request = NotifyUserRequest::Builder{}
            .apiKey(testApiKey)
            .title("test-title")
            .body("test-body")
            .deviceType(DeviceType::Value::IOS)
            .build();

        WHEN("call to /notify_user_custom") {
            try
            {
                api->notifyUserCustom(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            NotifyUserResponsePtr response = api->notifyUserCustom(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().length() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /recover_file successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long testFileId = createFile(api, testApiKey);
        RecoverFileRequestPtr request = RecoverFileRequest::Builder{}
            .apiKey(testApiKey)
            .id(testFileId)
            .build();

        WHEN("call to /recover_file") {
            try
            {
                api->recoverFile(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            RecoverFileResponsePtr response = api->recoverFile(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
                REQUIRE(response->code() == "file_recovered");
            }
        }
    }
} 

SCENARIO("should send request to /register_phone successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        RegisterPhoneRequestPtr request = RegisterPhoneRequest::Builder{}
            .token(API_TOKEN)
            .phone(PHONE_NUMBER)
            .build();

        WHEN("call to /register_phone") {
            RegisterPhoneResponsePtr response;
            try
            {
                response = api->registerPhone(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->phone() == PHONE_NUMBER);
                REQUIRE(response->code().length() > 0);
                REQUIRE(response->msg().find("Verification Code Sent") != std::string::npos);
            }
        }
    }
} 

SCENARIO("should send request to /update_device_token successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        UpdateDeviceTokenRequestPtr request = UpdateDeviceTokenRequest::Builder{}
            .apiKey(testApiKey)
            .deviceToken(DEVICE_TOKEN)
            .deviceType(DeviceType::Value::IOS)
            .build();

        WHEN("call to /update_device_token") {
            try
            {
                api->updateDeviceToken(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            UpdateDeviceTokenResponsePtr response = api->updateDeviceToken(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
            }
        }
    }
} 

SCENARIO("should send request to /update_file successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long testFileId = createFile(api, testApiKey);
        long testFolderId = createFolder(api, testApiKey);
        UpdateFileRequestPtr request = UpdateFileRequest::Builder{}
            .apiKey(testApiKey)
            .id(testFileId)
            .fName("f")
            .lName("l")
            .notes("n")
            .email("e@g.com")
            .phone(PHONE_NUMBER)
            .tags("t")
            .folderId(testFolderId)
            .name("n")
            .remindDays("2")
            .remindDate("2019-10-11")
            .build();

        WHEN("call to /update_file") {
            try
            {
                api->updateFile(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            UpdateFileResponsePtr response = api->updateFile(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
            }
        }
    }
} 

SCENARIO("should send request to /update_folder successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long testFolderId = createFolder(api, testApiKey);
        UpdateFolderRequestPtr request = UpdateFolderRequest::Builder{}
            .apiKey(testApiKey)
            .id(testFolderId)
            .name("test-folder")
            .pass(FOLDER_PASS)
            .isPrivate(false)
            .build();

        WHEN("call to /update_folder") {
            try
            {
                api->updateFolder(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            UpdateFolderResponsePtr response = api->updateFolder(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Folder Update") != std::string::npos);
                REQUIRE(response->code().length() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /update_order successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long testFolderId = createFolder(api, testApiKey);
        std::vector<UpdateOrderRequestFolder> folders {
                    *UpdateOrderRequestFolder::Builder{}
                        .id(testFolderId)
                        .build()
            };
        UpdateOrderRequestPtr request = UpdateOrderRequest::Builder{}
            .apiKey(testApiKey)
            .folders(folders)
            .build();

        WHEN("call to /update_order") {
            try
            {
                api->updateOrder(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            UpdateOrderResponsePtr response = api->updateOrder(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Order Updated") != std::string::npos);
                REQUIRE(response->code() == "order_updated");
            }
        }
    }
} 

SCENARIO("should send request to /update_profile_img successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        UpdateProfileImgRequestPtr request = UpdateProfileImgRequest::Builder{}
            .apiKey(testApiKey)
            .filePath("pict.png")
            .build();

        WHEN("call to /update_profile_img") {
            try
            {
                api->updateProfileImg(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            UpdateProfileImgResponsePtr response = api->updateProfileImg(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Profile Picture Updated") != std::string::npos);
                REQUIRE(response->code() == "profile_pic_updated");
                REQUIRE(response->file().find(".png") != std::string::npos);
                REQUIRE(response->path().length() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /update_profile successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        UpdateProfileRequestPtr request = UpdateProfileRequest::Builder{}
            .apiKey(testApiKey)
            .data(UpdateProfileRequestData::Builder{}
                    .fName("testFName")
                    .lName("testLName")
                    .email("test@mail.com")
                    .isPublic(true)
                    .language("en_us")                    
                    .build())
            .build();

        WHEN("call to /update_profile") {
            try
            {
                api->updateProfile(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            UpdateProfileResponsePtr response = api->updateProfile(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Profile Updated") != std::string::npos);
                REQUIRE(response->code() == "profile_updated");
            }
        }
    }
} 

SCENARIO("should send request to /update_settings successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        UpdateSettingsRequestPtr request = UpdateSettingsRequest::Builder{}
            .apiKey(testApiKey)
            .playBeep(PlayBeep::Value::NO)
            .filesPermission(FilesPermission::Value::PRIVATE)
            .build();

        WHEN("call to /update_settings") {
            try
            {
                api->updateSettings(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            UpdateSettingsResponsePtr response = api->updateSettings(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
                REQUIRE(response->code() == "settings_updated");
            }
        }
    }
} 

SCENARIO("should send request to /update_star successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long testFileId = createFile(api, testApiKey);
        UpdateStarRequestPtr request = UpdateStarRequest::Builder{}
            .apiKey(testApiKey)
            .id(testFileId)
            .star(true)
            .type("file")
            .build();

        WHEN("call to /update_star") {
            try
            {
                api->updateStar(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            UpdateStarResponsePtr response = api->updateStar(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
                REQUIRE(response->code() == "star_updated");
            }
        }
    }
} 

SCENARIO("should send request to /update_user successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        UpdateUserRequestPtr request = UpdateUserRequest::Builder{}
            .apiKey(testApiKey)
            .app(App::Value::FREE)
            .timezone("10")
            .build();

        WHEN("call to /update_user") {
            try
            {
                api->updateUser(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            UpdateUserResponsePtr response = api->updateUser(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().length() > 0);
                REQUIRE(response->code().length() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /upload_meta_file successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long testFileId = createFile(api, testApiKey);
        UploadMetaFileRequestPtr request = UploadMetaFileRequest::Builder{}
            .apiKey(testApiKey)
            .filePath("pict.png")
            .name("test-meta")
            .parentId(testFileId)
            .build();

        WHEN("call to /upload_meta_file") {
            try
            {
                api->uploadMetaFile(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            UploadMetaFileResponsePtr response = api->uploadMetaFile(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Success") != std::string::npos);
                REQUIRE(response->parentId() == testFileId);
                REQUIRE(response->id() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /verify_folder_pass successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testApiKey = retrieveApiKey(api);
        long folderId = createFolder(api, testApiKey);
        VerifyFolderPassRequestPtr request = VerifyFolderPassRequest::Builder{}
            .apiKey(testApiKey)
            .id(folderId)
            .pass(FOLDER_PASS)
            .build();

        WHEN("call to /verify_folder_pass") {
            try
            {
                api->verifyFolderPass(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            VerifyFolderPassResponsePtr response = api->verifyFolderPass(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->msg().find("Password is Correct") != std::string::npos);
                REQUIRE(response->code().length() > 0);
            }
        }
    }
} 

SCENARIO("should send request to /verify_phone successfully", "[api][.][.]") {
    GIVEN("client") {
        CallRecorderApiPtr api = CallRecorderApi::create(URL);
        std::string testVerifyPhoneCode = getVerificationCode(api);
        VerifyPhoneRequestPtr request = VerifyPhoneRequest::Builder{}
            .token(API_TOKEN)
            .phone(PHONE_NUMBER)
            .code(testVerifyPhoneCode)
            .mcc("300")
            .app(App::Value::REC)
            .deviceType(DeviceType::Value::IOS)
            .deviceToken(DEVICE_TOKEN)
            .deviceId(DEVICE_TOKEN)
            .timeZone("10")
            .build();

        WHEN("call to /verify_phone") {
            try
            {
                api->verifyPhone(*request);
            }
            catch (const ApiException& e)
            {
                std::string content(std::istreambuf_iterator<char>(*e.getContent()), {});
                std::cout << "Content: " << content << std::endl;
            }

            VerifyPhoneResponsePtr response = api->verifyPhone(*request);

            THEN("registered successfully") {
                REQUIRE(response->status() == SUCCESS_STATUS);
                REQUIRE(response->phone() == PHONE_NUMBER);
                REQUIRE(response->apiKey().length() > 0);
                REQUIRE(response->msg().find("Success") != std::string::npos);
            }
        }
    }
} 


