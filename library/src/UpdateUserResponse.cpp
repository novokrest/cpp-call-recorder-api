#include "UpdateUserResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateUserResponse::UpdateUserResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code
) : m_status(status), m_msg(msg), m_code(code)
{

}

UpdateUserResponse& UpdateUserResponse::operator=(const UpdateUserResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    return *this;
}

UpdateUserResponse::~UpdateUserResponse()
{

}

const std::string& UpdateUserResponse::status() const
{
    return m_status;
}
const std::string& UpdateUserResponse::msg() const
{
    return m_msg;
}
const std::string& UpdateUserResponse::code() const
{
    return m_code;
}

UpdateUserResponse::Builder& UpdateUserResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
UpdateUserResponse::Builder& UpdateUserResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
UpdateUserResponse::Builder& UpdateUserResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}

UpdateUserResponsePtr UpdateUserResponse::Builder::build() const
{
    return std::make_shared<UpdateUserResponse>(
        m_status,
        m_msg,
        m_code
    );
}

}
}
}
}
