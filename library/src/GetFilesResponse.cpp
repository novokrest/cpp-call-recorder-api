#include "GetFilesResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetFilesResponse::GetFilesResponse(
    const std::string& status,
    const long& credits,
    const long& creditsTrans,
    const std::vector<GetFilesResponseFile>& files
) : m_status(status), m_credits(credits), m_creditsTrans(creditsTrans), m_files(files)
{

}

GetFilesResponse& GetFilesResponse::operator=(const GetFilesResponse& other)
{
    this->m_status = other.m_status;
    this->m_credits = other.m_credits;
    this->m_creditsTrans = other.m_creditsTrans;
    this->m_files = other.m_files;
    return *this;
}

GetFilesResponse::~GetFilesResponse()
{

}

const std::string& GetFilesResponse::status() const
{
    return m_status;
}
const long& GetFilesResponse::credits() const
{
    return m_credits;
}
const long& GetFilesResponse::creditsTrans() const
{
    return m_creditsTrans;
}
const std::vector<GetFilesResponseFile>& GetFilesResponse::files() const
{
    return m_files;
}

GetFilesResponse::Builder& GetFilesResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
GetFilesResponse::Builder& GetFilesResponse::Builder::credits(const long& credits)
{
    m_credits = credits;
    return *this;
}
GetFilesResponse::Builder& GetFilesResponse::Builder::creditsTrans(const long& creditsTrans)
{
    m_creditsTrans = creditsTrans;
    return *this;
}
GetFilesResponse::Builder& GetFilesResponse::Builder::files(const std::vector<GetFilesResponseFile>& files)
{
    m_files = files;
    return *this;
}

GetFilesResponsePtr GetFilesResponse::Builder::build() const
{
    return std::make_shared<GetFilesResponse>(
        m_status,
        m_credits,
        m_creditsTrans,
        m_files
    );
}

}
}
}
}
