#include "UpdateProfileRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateProfileRequest::UpdateProfileRequest(
    const std::string& apiKey,
    const UpdateProfileRequestDataPtr& data
) : m_apiKey(apiKey), m_data(data)
{

}

UpdateProfileRequest& UpdateProfileRequest::operator=(const UpdateProfileRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_data = other.m_data;
    return *this;
}

UpdateProfileRequest::~UpdateProfileRequest()
{

}

const std::string& UpdateProfileRequest::apiKey() const
{
    return m_apiKey;
}
const UpdateProfileRequestDataPtr& UpdateProfileRequest::data() const
{
    return m_data;
}

UpdateProfileRequest::Builder& UpdateProfileRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
UpdateProfileRequest::Builder& UpdateProfileRequest::Builder::data(const UpdateProfileRequestDataPtr& data)
{
    m_data = data;
    return *this;
}

UpdateProfileRequestPtr UpdateProfileRequest::Builder::build() const
{
    return std::make_shared<UpdateProfileRequest>(
        m_apiKey,
        m_data
    );
}

}
}
}
}
