#include "UpdateStarRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateStarRequest::UpdateStarRequest(
    const std::string& apiKey,
    const long& id,
    const int& star,
    const std::string& type
) : m_apiKey(apiKey), m_id(id), m_star(star), m_type(type)
{

}

UpdateStarRequest& UpdateStarRequest::operator=(const UpdateStarRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_id = other.m_id;
    this->m_star = other.m_star;
    this->m_type = other.m_type;
    return *this;
}

UpdateStarRequest::~UpdateStarRequest()
{

}

const std::string& UpdateStarRequest::apiKey() const
{
    return m_apiKey;
}
const long& UpdateStarRequest::id() const
{
    return m_id;
}
const int& UpdateStarRequest::star() const
{
    return m_star;
}
const std::string& UpdateStarRequest::type() const
{
    return m_type;
}

UpdateStarRequest::Builder& UpdateStarRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
UpdateStarRequest::Builder& UpdateStarRequest::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
UpdateStarRequest::Builder& UpdateStarRequest::Builder::star(const int& star)
{
    m_star = star;
    return *this;
}
UpdateStarRequest::Builder& UpdateStarRequest::Builder::type(const std::string& type)
{
    m_type = type;
    return *this;
}

UpdateStarRequestPtr UpdateStarRequest::Builder::build() const
{
    return std::make_shared<UpdateStarRequest>(
        m_apiKey,
        m_id,
        m_star,
        m_type
    );
}

}
}
}
}
