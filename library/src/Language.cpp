#include "Language.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

Language::Language(
    const std::string& code,
    const std::string& name,
    const std::string& flag
) : m_code(code), m_name(name), m_flag(flag)
{

}

Language& Language::operator=(const Language& other)
{
    this->m_code = other.m_code;
    this->m_name = other.m_name;
    this->m_flag = other.m_flag;
    return *this;
}

Language::~Language()
{

}

const std::string& Language::code() const
{
    return m_code;
}
const std::string& Language::name() const
{
    return m_name;
}
const std::string& Language::flag() const
{
    return m_flag;
}

Language::Builder& Language::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}
Language::Builder& Language::Builder::name(const std::string& name)
{
    m_name = name;
    return *this;
}
Language::Builder& Language::Builder::flag(const std::string& flag)
{
    m_flag = flag;
    return *this;
}

LanguagePtr Language::Builder::build() const
{
    return std::make_shared<Language>(
        m_code,
        m_name,
        m_flag
    );
}

}
}
}
}
