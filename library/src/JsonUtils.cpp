#include "JsonUtils.h"

#include <cpprest/json.h>

namespace call {
namespace recorder {
namespace api {
namespace utils {

std::string JsonUtils::serializeJson(const web::json::value& value)
{
    utility::stringstream_t rawss;
    value.serialize(rawss);
    return rawss.str();
}

std::string JsonUtils::toRawJson(const std::map<std::string, web::json::value>& fields)
{
    web::json::value value = web::json::value();
    for (auto it = fields.begin(); it != fields.end(); ++it)
    {
        value[U(it->first)] = it->second;
    }
    return serializeJson(value);
}

std::string JsonUtils::toRawJson(const std::map<std::string, std::string>& fields)
{
    web::json::value value = web::json::value::parse(U("{}"));
    for (std::map<std::string, std::string>::const_iterator it = fields.begin(); it != fields.end(); ++it)
    {
        value[U(it->first)] = web::json::value(it->second);
    }
    return serializeJson(value);
}

std::string JsonUtils::toRawJson(const std::vector<std::string>& values)
{
    std::vector<web::json::value> jvalues;
    for (auto it = values.begin(); it != values.end(); ++it)
    {
        jvalues.push_back(web::json::value(*it));
    }
    web::json::value jarray = web::json::value::array(jvalues);
    return serializeJson(jarray);
}

std::string JsonUtils::toRawJson(const std::vector<long>& values)
{
    std::vector<web::json::value> jvalues;
    for (auto it = values.begin(); it != values.end(); ++it)
    {
        jvalues.push_back(web::json::value::number((int64_t)*it));
    }
    web::json::value jarray = web::json::value::array(jvalues);
    return serializeJson(jarray);
}

web::json::value JsonUtils::toJsonArray(const std::vector<std::string>& values)
{
    std::vector<web::json::value> jvalues;
    for (auto it = values.begin(); it != values.end(); ++it)
    {
        jvalues.push_back(web::json::value(*it));
    }
    return web::json::value::array(jvalues);
}

web::json::value JsonUtils::toJson(const std::map<std::string, web::json::value>& fields)
{
    web::json::value value = web::json::value();
    for (auto it = fields.begin(); it != fields.end(); ++it)
    {
        value[U(it->first)] = it->second;
    }
    return value;
}

}
}
}
}

