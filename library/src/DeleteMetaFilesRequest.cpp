#include "DeleteMetaFilesRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

DeleteMetaFilesRequest::DeleteMetaFilesRequest(
    const std::string& apiKey,
    const std::vector<long>& ids,
    const long& parentId
) : m_apiKey(apiKey), m_ids(ids), m_parentId(parentId)
{

}

DeleteMetaFilesRequest& DeleteMetaFilesRequest::operator=(const DeleteMetaFilesRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_ids = other.m_ids;
    this->m_parentId = other.m_parentId;
    return *this;
}

DeleteMetaFilesRequest::~DeleteMetaFilesRequest()
{

}

const std::string& DeleteMetaFilesRequest::apiKey() const
{
    return m_apiKey;
}
const std::vector<long>& DeleteMetaFilesRequest::ids() const
{
    return m_ids;
}
const long& DeleteMetaFilesRequest::parentId() const
{
    return m_parentId;
}

DeleteMetaFilesRequest::Builder& DeleteMetaFilesRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
DeleteMetaFilesRequest::Builder& DeleteMetaFilesRequest::Builder::ids(const std::vector<long>& ids)
{
    m_ids = ids;
    return *this;
}
DeleteMetaFilesRequest::Builder& DeleteMetaFilesRequest::Builder::parentId(const long& parentId)
{
    m_parentId = parentId;
    return *this;
}

DeleteMetaFilesRequestPtr DeleteMetaFilesRequest::Builder::build() const
{
    return std::make_shared<DeleteMetaFilesRequest>(
        m_apiKey,
        m_ids,
        m_parentId
    );
}

}
}
}
}
