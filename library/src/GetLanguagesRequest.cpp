#include "GetLanguagesRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetLanguagesRequest::GetLanguagesRequest(
    const std::string& apiKey
) : m_apiKey(apiKey)
{

}

GetLanguagesRequest& GetLanguagesRequest::operator=(const GetLanguagesRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    return *this;
}

GetLanguagesRequest::~GetLanguagesRequest()
{

}

const std::string& GetLanguagesRequest::apiKey() const
{
    return m_apiKey;
}

GetLanguagesRequest::Builder& GetLanguagesRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}

GetLanguagesRequestPtr GetLanguagesRequest::Builder::build() const
{
    return std::make_shared<GetLanguagesRequest>(
        m_apiKey
    );
}

}
}
}
}
