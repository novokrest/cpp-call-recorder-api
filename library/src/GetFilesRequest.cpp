#include "GetFilesRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetFilesRequest::GetFilesRequest(
    const std::string& apiKey,
    const std::string& page,
    const long& folderId,
    const std::string& source,
    const std::string& pass,
    const bool& reminder,
    const std::string& q,
    const long& id,
    const std::string& op
) : m_apiKey(apiKey), m_page(page), m_folderId(folderId), m_source(source), m_pass(pass), m_reminder(reminder), m_q(q), m_id(id), m_op(op)
{

}

GetFilesRequest& GetFilesRequest::operator=(const GetFilesRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_page = other.m_page;
    this->m_folderId = other.m_folderId;
    this->m_source = other.m_source;
    this->m_pass = other.m_pass;
    this->m_reminder = other.m_reminder;
    this->m_q = other.m_q;
    this->m_id = other.m_id;
    this->m_op = other.m_op;
    return *this;
}

GetFilesRequest::~GetFilesRequest()
{

}

const std::string& GetFilesRequest::apiKey() const
{
    return m_apiKey;
}
const std::string& GetFilesRequest::page() const
{
    return m_page;
}
const long& GetFilesRequest::folderId() const
{
    return m_folderId;
}
const std::string& GetFilesRequest::source() const
{
    return m_source;
}
const std::string& GetFilesRequest::pass() const
{
    return m_pass;
}
const bool& GetFilesRequest::reminder() const
{
    return m_reminder;
}
const std::string& GetFilesRequest::q() const
{
    return m_q;
}
const long& GetFilesRequest::id() const
{
    return m_id;
}
const std::string& GetFilesRequest::op() const
{
    return m_op;
}

GetFilesRequest::Builder& GetFilesRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
GetFilesRequest::Builder& GetFilesRequest::Builder::page(const std::string& page)
{
    m_page = page;
    return *this;
}
GetFilesRequest::Builder& GetFilesRequest::Builder::folderId(const long& folderId)
{
    m_folderId = folderId;
    return *this;
}
GetFilesRequest::Builder& GetFilesRequest::Builder::source(const std::string& source)
{
    m_source = source;
    return *this;
}
GetFilesRequest::Builder& GetFilesRequest::Builder::pass(const std::string& pass)
{
    m_pass = pass;
    return *this;
}
GetFilesRequest::Builder& GetFilesRequest::Builder::reminder(const bool& reminder)
{
    m_reminder = reminder;
    return *this;
}
GetFilesRequest::Builder& GetFilesRequest::Builder::q(const std::string& q)
{
    m_q = q;
    return *this;
}
GetFilesRequest::Builder& GetFilesRequest::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
GetFilesRequest::Builder& GetFilesRequest::Builder::op(const std::string& op)
{
    m_op = op;
    return *this;
}

GetFilesRequestPtr GetFilesRequest::Builder::build() const
{
    return std::make_shared<GetFilesRequest>(
        m_apiKey,
        m_page,
        m_folderId,
        m_source,
        m_pass,
        m_reminder,
        m_q,
        m_id,
        m_op
    );
}

}
}
}
}
