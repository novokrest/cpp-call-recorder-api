#include "CreateFolderResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

CreateFolderResponse::CreateFolderResponse(
    const std::string& status,
    const std::string& msg,
    const long& id,
    const std::string& code
) : m_status(status), m_msg(msg), m_id(id), m_code(code)
{

}

CreateFolderResponse& CreateFolderResponse::operator=(const CreateFolderResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_id = other.m_id;
    this->m_code = other.m_code;
    return *this;
}

CreateFolderResponse::~CreateFolderResponse()
{

}

const std::string& CreateFolderResponse::status() const
{
    return m_status;
}
const std::string& CreateFolderResponse::msg() const
{
    return m_msg;
}
const long& CreateFolderResponse::id() const
{
    return m_id;
}
const std::string& CreateFolderResponse::code() const
{
    return m_code;
}

CreateFolderResponse::Builder& CreateFolderResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
CreateFolderResponse::Builder& CreateFolderResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
CreateFolderResponse::Builder& CreateFolderResponse::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
CreateFolderResponse::Builder& CreateFolderResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}

CreateFolderResponsePtr CreateFolderResponse::Builder::build() const
{
    return std::make_shared<CreateFolderResponse>(
        m_status,
        m_msg,
        m_id,
        m_code
    );
}

}
}
}
}
