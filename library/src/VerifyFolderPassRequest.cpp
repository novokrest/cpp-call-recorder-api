#include "VerifyFolderPassRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

VerifyFolderPassRequest::VerifyFolderPassRequest(
    const std::string& apiKey,
    const long& id,
    const std::string& pass
) : m_apiKey(apiKey), m_id(id), m_pass(pass)
{

}

VerifyFolderPassRequest& VerifyFolderPassRequest::operator=(const VerifyFolderPassRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_id = other.m_id;
    this->m_pass = other.m_pass;
    return *this;
}

VerifyFolderPassRequest::~VerifyFolderPassRequest()
{

}

const std::string& VerifyFolderPassRequest::apiKey() const
{
    return m_apiKey;
}
const long& VerifyFolderPassRequest::id() const
{
    return m_id;
}
const std::string& VerifyFolderPassRequest::pass() const
{
    return m_pass;
}

VerifyFolderPassRequest::Builder& VerifyFolderPassRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
VerifyFolderPassRequest::Builder& VerifyFolderPassRequest::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
VerifyFolderPassRequest::Builder& VerifyFolderPassRequest::Builder::pass(const std::string& pass)
{
    m_pass = pass;
    return *this;
}

VerifyFolderPassRequestPtr VerifyFolderPassRequest::Builder::build() const
{
    return std::make_shared<VerifyFolderPassRequest>(
        m_apiKey,
        m_id,
        m_pass
    );
}

}
}
}
}
