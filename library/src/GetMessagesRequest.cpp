#include "GetMessagesRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetMessagesRequest::GetMessagesRequest(
    const std::string& apiKey
) : m_apiKey(apiKey)
{

}

GetMessagesRequest& GetMessagesRequest::operator=(const GetMessagesRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    return *this;
}

GetMessagesRequest::~GetMessagesRequest()
{

}

const std::string& GetMessagesRequest::apiKey() const
{
    return m_apiKey;
}

GetMessagesRequest::Builder& GetMessagesRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}

GetMessagesRequestPtr GetMessagesRequest::Builder::build() const
{
    return std::make_shared<GetMessagesRequest>(
        m_apiKey
    );
}

}
}
}
}
