#include "GetSettingsResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetSettingsResponse::GetSettingsResponse(
    const std::string& status,
    const App::Value& app,
    const long& credits,
    const GetSettingsResponseSettingsPtr& settings
) : m_status(status), m_app(app), m_credits(credits), m_settings(settings)
{

}

GetSettingsResponse& GetSettingsResponse::operator=(const GetSettingsResponse& other)
{
    this->m_status = other.m_status;
    this->m_app = other.m_app;
    this->m_credits = other.m_credits;
    this->m_settings = other.m_settings;
    return *this;
}

GetSettingsResponse::~GetSettingsResponse()
{

}

const std::string& GetSettingsResponse::status() const
{
    return m_status;
}
const App::Value& GetSettingsResponse::app() const
{
    return m_app;
}
const long& GetSettingsResponse::credits() const
{
    return m_credits;
}
const GetSettingsResponseSettingsPtr& GetSettingsResponse::settings() const
{
    return m_settings;
}

GetSettingsResponse::Builder& GetSettingsResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
GetSettingsResponse::Builder& GetSettingsResponse::Builder::app(const App::Value& app)
{
    m_app = app;
    return *this;
}
GetSettingsResponse::Builder& GetSettingsResponse::Builder::credits(const long& credits)
{
    m_credits = credits;
    return *this;
}
GetSettingsResponse::Builder& GetSettingsResponse::Builder::settings(const GetSettingsResponseSettingsPtr& settings)
{
    m_settings = settings;
    return *this;
}

GetSettingsResponsePtr GetSettingsResponse::Builder::build() const
{
    return std::make_shared<GetSettingsResponse>(
        m_status,
        m_app,
        m_credits,
        m_settings
    );
}

}
}
}
}
