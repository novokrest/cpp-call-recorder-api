#include "DeleteFolderResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

DeleteFolderResponse::DeleteFolderResponse(
    const std::string& status,
    const std::string& msg
) : m_status(status), m_msg(msg)
{

}

DeleteFolderResponse& DeleteFolderResponse::operator=(const DeleteFolderResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    return *this;
}

DeleteFolderResponse::~DeleteFolderResponse()
{

}

const std::string& DeleteFolderResponse::status() const
{
    return m_status;
}
const std::string& DeleteFolderResponse::msg() const
{
    return m_msg;
}

DeleteFolderResponse::Builder& DeleteFolderResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
DeleteFolderResponse::Builder& DeleteFolderResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}

DeleteFolderResponsePtr DeleteFolderResponse::Builder::build() const
{
    return std::make_shared<DeleteFolderResponse>(
        m_status,
        m_msg
    );
}

}
}
}
}
