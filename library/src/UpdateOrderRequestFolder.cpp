#include "UpdateOrderRequestFolder.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateOrderRequestFolder::UpdateOrderRequestFolder(
    const long& id,
    const long& orderId
) : m_id(id), m_orderId(orderId)
{

}

UpdateOrderRequestFolder& UpdateOrderRequestFolder::operator=(const UpdateOrderRequestFolder& other)
{
    this->m_id = other.m_id;
    this->m_orderId = other.m_orderId;
    return *this;
}

UpdateOrderRequestFolder::~UpdateOrderRequestFolder()
{

}

const long& UpdateOrderRequestFolder::id() const
{
    return m_id;
}
const long& UpdateOrderRequestFolder::orderId() const
{
    return m_orderId;
}

UpdateOrderRequestFolder::Builder& UpdateOrderRequestFolder::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
UpdateOrderRequestFolder::Builder& UpdateOrderRequestFolder::Builder::orderId(const long& orderId)
{
    m_orderId = orderId;
    return *this;
}

UpdateOrderRequestFolderPtr UpdateOrderRequestFolder::Builder::build() const
{
    return std::make_shared<UpdateOrderRequestFolder>(
        m_id,
        m_orderId
    );
}

}
}
}
}
