#include "CloneFileResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

CloneFileResponse::CloneFileResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code,
    const long& id
) : m_status(status), m_msg(msg), m_code(code), m_id(id)
{

}

CloneFileResponse& CloneFileResponse::operator=(const CloneFileResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    this->m_id = other.m_id;
    return *this;
}

CloneFileResponse::~CloneFileResponse()
{

}

const std::string& CloneFileResponse::status() const
{
    return m_status;
}
const std::string& CloneFileResponse::msg() const
{
    return m_msg;
}
const std::string& CloneFileResponse::code() const
{
    return m_code;
}
const long& CloneFileResponse::id() const
{
    return m_id;
}

CloneFileResponse::Builder& CloneFileResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
CloneFileResponse::Builder& CloneFileResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
CloneFileResponse::Builder& CloneFileResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}
CloneFileResponse::Builder& CloneFileResponse::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}

CloneFileResponsePtr CloneFileResponse::Builder::build() const
{
    return std::make_shared<CloneFileResponse>(
        m_status,
        m_msg,
        m_code,
        m_id
    );
}

}
}
}
}
