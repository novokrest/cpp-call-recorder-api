#include "GetTranslationsRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetTranslationsRequest::GetTranslationsRequest(
    const std::string& apiKey,
    const std::string& language
) : m_apiKey(apiKey), m_language(language)
{

}

GetTranslationsRequest& GetTranslationsRequest::operator=(const GetTranslationsRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_language = other.m_language;
    return *this;
}

GetTranslationsRequest::~GetTranslationsRequest()
{

}

const std::string& GetTranslationsRequest::apiKey() const
{
    return m_apiKey;
}
const std::string& GetTranslationsRequest::language() const
{
    return m_language;
}

GetTranslationsRequest::Builder& GetTranslationsRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
GetTranslationsRequest::Builder& GetTranslationsRequest::Builder::language(const std::string& language)
{
    m_language = language;
    return *this;
}

GetTranslationsRequestPtr GetTranslationsRequest::Builder::build() const
{
    return std::make_shared<GetTranslationsRequest>(
        m_apiKey,
        m_language
    );
}

}
}
}
}
