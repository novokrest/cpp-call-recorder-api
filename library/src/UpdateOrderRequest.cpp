#include "UpdateOrderRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateOrderRequest::UpdateOrderRequest(
    const std::string& apiKey,
    const std::vector<UpdateOrderRequestFolder>& folders
) : m_apiKey(apiKey), m_folders(folders)
{

}

UpdateOrderRequest& UpdateOrderRequest::operator=(const UpdateOrderRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_folders = other.m_folders;
    return *this;
}

UpdateOrderRequest::~UpdateOrderRequest()
{

}

const std::string& UpdateOrderRequest::apiKey() const
{
    return m_apiKey;
}
const std::vector<UpdateOrderRequestFolder>& UpdateOrderRequest::folders() const
{
    return m_folders;
}

UpdateOrderRequest::Builder& UpdateOrderRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
UpdateOrderRequest::Builder& UpdateOrderRequest::Builder::folders(const std::vector<UpdateOrderRequestFolder>& folders)
{
    m_folders = folders;
    return *this;
}

UpdateOrderRequestPtr UpdateOrderRequest::Builder::build() const
{
    return std::make_shared<UpdateOrderRequest>(
        m_apiKey,
        m_folders
    );
}

}
}
}
}
