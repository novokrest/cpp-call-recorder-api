#include "GetFoldersRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetFoldersRequest::GetFoldersRequest(
    const std::string& apiKey
) : m_apiKey(apiKey)
{

}

GetFoldersRequest& GetFoldersRequest::operator=(const GetFoldersRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    return *this;
}

GetFoldersRequest::~GetFoldersRequest()
{

}

const std::string& GetFoldersRequest::apiKey() const
{
    return m_apiKey;
}

GetFoldersRequest::Builder& GetFoldersRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}

GetFoldersRequestPtr GetFoldersRequest::Builder::build() const
{
    return std::make_shared<GetFoldersRequest>(
        m_apiKey
    );
}

}
}
}
}
