#include "CreateFileData.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

CreateFileData::CreateFileData(
    const std::string& name,
    const std::string& email,
    const std::string& phone,
    const std::string& lName,
    const std::string& fName,
    const std::string& notes,
    const std::string& tags,
    const std::vector<std::string>& meta,
    const std::string& source,
    const std::string& remindDays,
    const std::string& remindDate
) : m_name(name), m_email(email), m_phone(phone), m_lName(lName), m_fName(fName), m_notes(notes), m_tags(tags), m_meta(meta), m_source(source), m_remindDays(remindDays), m_remindDate(remindDate)
{

}

CreateFileData& CreateFileData::operator=(const CreateFileData& other)
{
    this->m_name = other.m_name;
    this->m_email = other.m_email;
    this->m_phone = other.m_phone;
    this->m_lName = other.m_lName;
    this->m_fName = other.m_fName;
    this->m_notes = other.m_notes;
    this->m_tags = other.m_tags;
    this->m_meta = other.m_meta;
    this->m_source = other.m_source;
    this->m_remindDays = other.m_remindDays;
    this->m_remindDate = other.m_remindDate;
    return *this;
}

CreateFileData::~CreateFileData()
{

}

const std::string& CreateFileData::name() const
{
    return m_name;
}
const std::string& CreateFileData::email() const
{
    return m_email;
}
const std::string& CreateFileData::phone() const
{
    return m_phone;
}
const std::string& CreateFileData::lName() const
{
    return m_lName;
}
const std::string& CreateFileData::fName() const
{
    return m_fName;
}
const std::string& CreateFileData::notes() const
{
    return m_notes;
}
const std::string& CreateFileData::tags() const
{
    return m_tags;
}
const std::vector<std::string>& CreateFileData::meta() const
{
    return m_meta;
}
const std::string& CreateFileData::source() const
{
    return m_source;
}
const std::string& CreateFileData::remindDays() const
{
    return m_remindDays;
}
const std::string& CreateFileData::remindDate() const
{
    return m_remindDate;
}

CreateFileData::Builder& CreateFileData::Builder::name(const std::string& name)
{
    m_name = name;
    return *this;
}
CreateFileData::Builder& CreateFileData::Builder::email(const std::string& email)
{
    m_email = email;
    return *this;
}
CreateFileData::Builder& CreateFileData::Builder::phone(const std::string& phone)
{
    m_phone = phone;
    return *this;
}
CreateFileData::Builder& CreateFileData::Builder::lName(const std::string& lName)
{
    m_lName = lName;
    return *this;
}
CreateFileData::Builder& CreateFileData::Builder::fName(const std::string& fName)
{
    m_fName = fName;
    return *this;
}
CreateFileData::Builder& CreateFileData::Builder::notes(const std::string& notes)
{
    m_notes = notes;
    return *this;
}
CreateFileData::Builder& CreateFileData::Builder::tags(const std::string& tags)
{
    m_tags = tags;
    return *this;
}
CreateFileData::Builder& CreateFileData::Builder::meta(const std::vector<std::string>& meta)
{
    m_meta = meta;
    return *this;
}
CreateFileData::Builder& CreateFileData::Builder::source(const std::string& source)
{
    m_source = source;
    return *this;
}
CreateFileData::Builder& CreateFileData::Builder::remindDays(const std::string& remindDays)
{
    m_remindDays = remindDays;
    return *this;
}
CreateFileData::Builder& CreateFileData::Builder::remindDate(const std::string& remindDate)
{
    m_remindDate = remindDate;
    return *this;
}

CreateFileDataPtr CreateFileData::Builder::build() const
{
    return std::make_shared<CreateFileData>(
        m_name,
        m_email,
        m_phone,
        m_lName,
        m_fName,
        m_notes,
        m_tags,
        m_meta,
        m_source,
        m_remindDays,
        m_remindDate
    );
}

}
}
}
}
