#include "GetMetaFilesResponseMetaFile.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetMetaFilesResponseMetaFile::GetMetaFilesResponseMetaFile(
    const std::string& id,
    const std::string& parentId,
    const std::string& name,
    const std::string& file,
    const std::string& userId,
    const std::string& time
) : m_id(id), m_parentId(parentId), m_name(name), m_file(file), m_userId(userId), m_time(time)
{

}

GetMetaFilesResponseMetaFile& GetMetaFilesResponseMetaFile::operator=(const GetMetaFilesResponseMetaFile& other)
{
    this->m_id = other.m_id;
    this->m_parentId = other.m_parentId;
    this->m_name = other.m_name;
    this->m_file = other.m_file;
    this->m_userId = other.m_userId;
    this->m_time = other.m_time;
    return *this;
}

GetMetaFilesResponseMetaFile::~GetMetaFilesResponseMetaFile()
{

}

const std::string& GetMetaFilesResponseMetaFile::id() const
{
    return m_id;
}
const std::string& GetMetaFilesResponseMetaFile::parentId() const
{
    return m_parentId;
}
const std::string& GetMetaFilesResponseMetaFile::name() const
{
    return m_name;
}
const std::string& GetMetaFilesResponseMetaFile::file() const
{
    return m_file;
}
const std::string& GetMetaFilesResponseMetaFile::userId() const
{
    return m_userId;
}
const std::string& GetMetaFilesResponseMetaFile::time() const
{
    return m_time;
}

GetMetaFilesResponseMetaFile::Builder& GetMetaFilesResponseMetaFile::Builder::id(const std::string& id)
{
    m_id = id;
    return *this;
}
GetMetaFilesResponseMetaFile::Builder& GetMetaFilesResponseMetaFile::Builder::parentId(const std::string& parentId)
{
    m_parentId = parentId;
    return *this;
}
GetMetaFilesResponseMetaFile::Builder& GetMetaFilesResponseMetaFile::Builder::name(const std::string& name)
{
    m_name = name;
    return *this;
}
GetMetaFilesResponseMetaFile::Builder& GetMetaFilesResponseMetaFile::Builder::file(const std::string& file)
{
    m_file = file;
    return *this;
}
GetMetaFilesResponseMetaFile::Builder& GetMetaFilesResponseMetaFile::Builder::userId(const std::string& userId)
{
    m_userId = userId;
    return *this;
}
GetMetaFilesResponseMetaFile::Builder& GetMetaFilesResponseMetaFile::Builder::time(const std::string& time)
{
    m_time = time;
    return *this;
}

GetMetaFilesResponseMetaFilePtr GetMetaFilesResponseMetaFile::Builder::build() const
{
    return std::make_shared<GetMetaFilesResponseMetaFile>(
        m_id,
        m_parentId,
        m_name,
        m_file,
        m_userId,
        m_time
    );
}

}
}
}
}
