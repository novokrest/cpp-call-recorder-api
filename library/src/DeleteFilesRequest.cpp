#include "DeleteFilesRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

DeleteFilesRequest::DeleteFilesRequest(
    const std::string& apiKey,
    const std::vector<long>& ids,
    const std::string& action
) : m_apiKey(apiKey), m_ids(ids), m_action(action)
{

}

DeleteFilesRequest& DeleteFilesRequest::operator=(const DeleteFilesRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_ids = other.m_ids;
    this->m_action = other.m_action;
    return *this;
}

DeleteFilesRequest::~DeleteFilesRequest()
{

}

const std::string& DeleteFilesRequest::apiKey() const
{
    return m_apiKey;
}
const std::vector<long>& DeleteFilesRequest::ids() const
{
    return m_ids;
}
const std::string& DeleteFilesRequest::action() const
{
    return m_action;
}

DeleteFilesRequest::Builder& DeleteFilesRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
DeleteFilesRequest::Builder& DeleteFilesRequest::Builder::ids(const std::vector<long>& ids)
{
    m_ids = ids;
    return *this;
}
DeleteFilesRequest::Builder& DeleteFilesRequest::Builder::action(const std::string& action)
{
    m_action = action;
    return *this;
}

DeleteFilesRequestPtr DeleteFilesRequest::Builder::build() const
{
    return std::make_shared<DeleteFilesRequest>(
        m_apiKey,
        m_ids,
        m_action
    );
}

}
}
}
}
