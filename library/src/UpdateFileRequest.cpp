#include "UpdateFileRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateFileRequest::UpdateFileRequest(
    const std::string& apiKey,
    const long& id,
    const std::string& fName,
    const std::string& lName,
    const std::string& notes,
    const std::string& email,
    const std::string& phone,
    const std::string& tags,
    const long& folderId,
    const std::string& name,
    const std::string& remindDays,
    const std::string& remindDate
) : m_apiKey(apiKey), m_id(id), m_fName(fName), m_lName(lName), m_notes(notes), m_email(email), m_phone(phone), m_tags(tags), m_folderId(folderId), m_name(name), m_remindDays(remindDays), m_remindDate(remindDate)
{

}

UpdateFileRequest& UpdateFileRequest::operator=(const UpdateFileRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_id = other.m_id;
    this->m_fName = other.m_fName;
    this->m_lName = other.m_lName;
    this->m_notes = other.m_notes;
    this->m_email = other.m_email;
    this->m_phone = other.m_phone;
    this->m_tags = other.m_tags;
    this->m_folderId = other.m_folderId;
    this->m_name = other.m_name;
    this->m_remindDays = other.m_remindDays;
    this->m_remindDate = other.m_remindDate;
    return *this;
}

UpdateFileRequest::~UpdateFileRequest()
{

}

const std::string& UpdateFileRequest::apiKey() const
{
    return m_apiKey;
}
const long& UpdateFileRequest::id() const
{
    return m_id;
}
const std::string& UpdateFileRequest::fName() const
{
    return m_fName;
}
const std::string& UpdateFileRequest::lName() const
{
    return m_lName;
}
const std::string& UpdateFileRequest::notes() const
{
    return m_notes;
}
const std::string& UpdateFileRequest::email() const
{
    return m_email;
}
const std::string& UpdateFileRequest::phone() const
{
    return m_phone;
}
const std::string& UpdateFileRequest::tags() const
{
    return m_tags;
}
const long& UpdateFileRequest::folderId() const
{
    return m_folderId;
}
const std::string& UpdateFileRequest::name() const
{
    return m_name;
}
const std::string& UpdateFileRequest::remindDays() const
{
    return m_remindDays;
}
const std::string& UpdateFileRequest::remindDate() const
{
    return m_remindDate;
}

UpdateFileRequest::Builder& UpdateFileRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::fName(const std::string& fName)
{
    m_fName = fName;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::lName(const std::string& lName)
{
    m_lName = lName;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::notes(const std::string& notes)
{
    m_notes = notes;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::email(const std::string& email)
{
    m_email = email;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::phone(const std::string& phone)
{
    m_phone = phone;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::tags(const std::string& tags)
{
    m_tags = tags;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::folderId(const long& folderId)
{
    m_folderId = folderId;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::name(const std::string& name)
{
    m_name = name;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::remindDays(const std::string& remindDays)
{
    m_remindDays = remindDays;
    return *this;
}
UpdateFileRequest::Builder& UpdateFileRequest::Builder::remindDate(const std::string& remindDate)
{
    m_remindDate = remindDate;
    return *this;
}

UpdateFileRequestPtr UpdateFileRequest::Builder::build() const
{
    return std::make_shared<UpdateFileRequest>(
        m_apiKey,
        m_id,
        m_fName,
        m_lName,
        m_notes,
        m_email,
        m_phone,
        m_tags,
        m_folderId,
        m_name,
        m_remindDays,
        m_remindDate
    );
}

}
}
}
}
