#include "UploadMetaFileRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UploadMetaFileRequest::UploadMetaFileRequest(
    const std::string& apiKey,
    const std::string& filePath,
    const std::string& name,
    const long& parentId,
    const long& id
) : m_apiKey(apiKey), m_filePath(filePath), m_name(name), m_parentId(parentId), m_id(id)
{

}

UploadMetaFileRequest& UploadMetaFileRequest::operator=(const UploadMetaFileRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_filePath = other.m_filePath;
    this->m_name = other.m_name;
    this->m_parentId = other.m_parentId;
    this->m_id = other.m_id;
    return *this;
}

UploadMetaFileRequest::~UploadMetaFileRequest()
{

}

const std::string& UploadMetaFileRequest::apiKey() const
{
    return m_apiKey;
}
const std::string& UploadMetaFileRequest::filePath() const
{
    return m_filePath;
}
const std::string& UploadMetaFileRequest::name() const
{
    return m_name;
}
const long& UploadMetaFileRequest::parentId() const
{
    return m_parentId;
}
const long& UploadMetaFileRequest::id() const
{
    return m_id;
}

UploadMetaFileRequest::Builder& UploadMetaFileRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
UploadMetaFileRequest::Builder& UploadMetaFileRequest::Builder::filePath(const std::string& filePath)
{
    m_filePath = filePath;
    return *this;
}
UploadMetaFileRequest::Builder& UploadMetaFileRequest::Builder::name(const std::string& name)
{
    m_name = name;
    return *this;
}
UploadMetaFileRequest::Builder& UploadMetaFileRequest::Builder::parentId(const long& parentId)
{
    m_parentId = parentId;
    return *this;
}
UploadMetaFileRequest::Builder& UploadMetaFileRequest::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}

UploadMetaFileRequestPtr UploadMetaFileRequest::Builder::build() const
{
    return std::make_shared<UploadMetaFileRequest>(
        m_apiKey,
        m_filePath,
        m_name,
        m_parentId,
        m_id
    );
}

}
}
}
}
