#include "UpdateDeviceTokenResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateDeviceTokenResponse::UpdateDeviceTokenResponse(
    const std::string& status,
    const std::string& msg
) : m_status(status), m_msg(msg)
{

}

UpdateDeviceTokenResponse& UpdateDeviceTokenResponse::operator=(const UpdateDeviceTokenResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    return *this;
}

UpdateDeviceTokenResponse::~UpdateDeviceTokenResponse()
{

}

const std::string& UpdateDeviceTokenResponse::status() const
{
    return m_status;
}
const std::string& UpdateDeviceTokenResponse::msg() const
{
    return m_msg;
}

UpdateDeviceTokenResponse::Builder& UpdateDeviceTokenResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
UpdateDeviceTokenResponse::Builder& UpdateDeviceTokenResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}

UpdateDeviceTokenResponsePtr UpdateDeviceTokenResponse::Builder::build() const
{
    return std::make_shared<UpdateDeviceTokenResponse>(
        m_status,
        m_msg
    );
}

}
}
}
}
