#include "UpdateProfileImgResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateProfileImgResponse::UpdateProfileImgResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code,
    const std::string& file,
    const std::string& path
) : m_status(status), m_msg(msg), m_code(code), m_file(file), m_path(path)
{

}

UpdateProfileImgResponse& UpdateProfileImgResponse::operator=(const UpdateProfileImgResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    this->m_file = other.m_file;
    this->m_path = other.m_path;
    return *this;
}

UpdateProfileImgResponse::~UpdateProfileImgResponse()
{

}

const std::string& UpdateProfileImgResponse::status() const
{
    return m_status;
}
const std::string& UpdateProfileImgResponse::msg() const
{
    return m_msg;
}
const std::string& UpdateProfileImgResponse::code() const
{
    return m_code;
}
const std::string& UpdateProfileImgResponse::file() const
{
    return m_file;
}
const std::string& UpdateProfileImgResponse::path() const
{
    return m_path;
}

UpdateProfileImgResponse::Builder& UpdateProfileImgResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
UpdateProfileImgResponse::Builder& UpdateProfileImgResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
UpdateProfileImgResponse::Builder& UpdateProfileImgResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}
UpdateProfileImgResponse::Builder& UpdateProfileImgResponse::Builder::file(const std::string& file)
{
    m_file = file;
    return *this;
}
UpdateProfileImgResponse::Builder& UpdateProfileImgResponse::Builder::path(const std::string& path)
{
    m_path = path;
    return *this;
}

UpdateProfileImgResponsePtr UpdateProfileImgResponse::Builder::build() const
{
    return std::make_shared<UpdateProfileImgResponse>(
        m_status,
        m_msg,
        m_code,
        m_file,
        m_path
    );
}

}
}
}
}
