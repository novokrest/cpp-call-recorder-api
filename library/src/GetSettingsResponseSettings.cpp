#include "GetSettingsResponseSettings.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetSettingsResponseSettings::GetSettingsResponseSettings(
    const PlayBeep::Value& playBeep,
    const FilesPermission::Value& filesPermission
) : m_playBeep(playBeep), m_filesPermission(filesPermission)
{

}

GetSettingsResponseSettings& GetSettingsResponseSettings::operator=(const GetSettingsResponseSettings& other)
{
    this->m_playBeep = other.m_playBeep;
    this->m_filesPermission = other.m_filesPermission;
    return *this;
}

GetSettingsResponseSettings::~GetSettingsResponseSettings()
{

}

const PlayBeep::Value& GetSettingsResponseSettings::playBeep() const
{
    return m_playBeep;
}
const FilesPermission::Value& GetSettingsResponseSettings::filesPermission() const
{
    return m_filesPermission;
}

GetSettingsResponseSettings::Builder& GetSettingsResponseSettings::Builder::playBeep(const PlayBeep::Value& playBeep)
{
    m_playBeep = playBeep;
    return *this;
}
GetSettingsResponseSettings::Builder& GetSettingsResponseSettings::Builder::filesPermission(const FilesPermission::Value& filesPermission)
{
    m_filesPermission = filesPermission;
    return *this;
}

GetSettingsResponseSettingsPtr GetSettingsResponseSettings::Builder::build() const
{
    return std::make_shared<GetSettingsResponseSettings>(
        m_playBeep,
        m_filesPermission
    );
}

}
}
}
}
