#include "DeleteFolderRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

DeleteFolderRequest::DeleteFolderRequest(
    const std::string& apiKey,
    const long& id,
    const long& moveTo
) : m_apiKey(apiKey), m_id(id), m_moveTo(moveTo)
{

}

DeleteFolderRequest& DeleteFolderRequest::operator=(const DeleteFolderRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_id = other.m_id;
    this->m_moveTo = other.m_moveTo;
    return *this;
}

DeleteFolderRequest::~DeleteFolderRequest()
{

}

const std::string& DeleteFolderRequest::apiKey() const
{
    return m_apiKey;
}
const long& DeleteFolderRequest::id() const
{
    return m_id;
}
const long& DeleteFolderRequest::moveTo() const
{
    return m_moveTo;
}

DeleteFolderRequest::Builder& DeleteFolderRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
DeleteFolderRequest::Builder& DeleteFolderRequest::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
DeleteFolderRequest::Builder& DeleteFolderRequest::Builder::moveTo(const long& moveTo)
{
    m_moveTo = moveTo;
    return *this;
}

DeleteFolderRequestPtr DeleteFolderRequest::Builder::build() const
{
    return std::make_shared<DeleteFolderRequest>(
        m_apiKey,
        m_id,
        m_moveTo
    );
}

}
}
}
}
