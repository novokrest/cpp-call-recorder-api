#include "UpdateFolderRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateFolderRequest::UpdateFolderRequest(
    const std::string& apiKey,
    const long& id,
    const std::string& name,
    const std::string& pass,
    const bool& isPrivate
) : m_apiKey(apiKey), m_id(id), m_name(name), m_pass(pass), m_isPrivate(isPrivate)
{

}

UpdateFolderRequest& UpdateFolderRequest::operator=(const UpdateFolderRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_id = other.m_id;
    this->m_name = other.m_name;
    this->m_pass = other.m_pass;
    this->m_isPrivate = other.m_isPrivate;
    return *this;
}

UpdateFolderRequest::~UpdateFolderRequest()
{

}

const std::string& UpdateFolderRequest::apiKey() const
{
    return m_apiKey;
}
const long& UpdateFolderRequest::id() const
{
    return m_id;
}
const std::string& UpdateFolderRequest::name() const
{
    return m_name;
}
const std::string& UpdateFolderRequest::pass() const
{
    return m_pass;
}
const bool& UpdateFolderRequest::isPrivate() const
{
    return m_isPrivate;
}

UpdateFolderRequest::Builder& UpdateFolderRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
UpdateFolderRequest::Builder& UpdateFolderRequest::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
UpdateFolderRequest::Builder& UpdateFolderRequest::Builder::name(const std::string& name)
{
    m_name = name;
    return *this;
}
UpdateFolderRequest::Builder& UpdateFolderRequest::Builder::pass(const std::string& pass)
{
    m_pass = pass;
    return *this;
}
UpdateFolderRequest::Builder& UpdateFolderRequest::Builder::isPrivate(const bool& isPrivate)
{
    m_isPrivate = isPrivate;
    return *this;
}

UpdateFolderRequestPtr UpdateFolderRequest::Builder::build() const
{
    return std::make_shared<UpdateFolderRequest>(
        m_apiKey,
        m_id,
        m_name,
        m_pass,
        m_isPrivate
    );
}

}
}
}
}
