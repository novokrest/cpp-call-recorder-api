#include "DeviceType.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

std::string DeviceType::toString(const DeviceType::Value value)
{
    const std::map<DeviceType::Value, std::string> DeviceTypeValueToStringMap {
        {
            DeviceType::Value::ANDROID,
            "android"
        },
        {
            DeviceType::Value::IOS,
            "ios"
        },
        {
            DeviceType::Value::MAC,
            "mac"
        },
        {
            DeviceType::Value::WINDOWS,
            "windows"
        },
        {
            DeviceType::Value::WEB,
            "web"
        },
        {
            DeviceType::Value::CUSTOM,
            "custom"
        },
    };

    auto it = DeviceTypeValueToStringMap.find(value);
    return it->second;
}

DeviceType::Value DeviceType::fromString(const std::string& value)
{
    const std::map<std::string, DeviceType::Value> StringToDeviceTypeValueMap {
        {
            "android",
            DeviceType::Value::ANDROID
        },
        {
            "ios",
            DeviceType::Value::IOS
        },
        {
            "mac",
            DeviceType::Value::MAC
        },
        {
            "windows",
            DeviceType::Value::WINDOWS
        },
        {
            "web",
            DeviceType::Value::WEB
        },
        {
            "custom",
            DeviceType::Value::CUSTOM
        },
    };

    auto it = StringToDeviceTypeValueMap.find(value);
    return it->second;
}

}
}
}
}
