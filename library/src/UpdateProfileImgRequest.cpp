#include "UpdateProfileImgRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateProfileImgRequest::UpdateProfileImgRequest(
    const std::string& apiKey,
    const std::string& filePath
) : m_apiKey(apiKey), m_filePath(filePath)
{

}

UpdateProfileImgRequest& UpdateProfileImgRequest::operator=(const UpdateProfileImgRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_filePath = other.m_filePath;
    return *this;
}

UpdateProfileImgRequest::~UpdateProfileImgRequest()
{

}

const std::string& UpdateProfileImgRequest::apiKey() const
{
    return m_apiKey;
}
const std::string& UpdateProfileImgRequest::filePath() const
{
    return m_filePath;
}

UpdateProfileImgRequest::Builder& UpdateProfileImgRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
UpdateProfileImgRequest::Builder& UpdateProfileImgRequest::Builder::filePath(const std::string& filePath)
{
    m_filePath = filePath;
    return *this;
}

UpdateProfileImgRequestPtr UpdateProfileImgRequest::Builder::build() const
{
    return std::make_shared<UpdateProfileImgRequest>(
        m_apiKey,
        m_filePath
    );
}

}
}
}
}
