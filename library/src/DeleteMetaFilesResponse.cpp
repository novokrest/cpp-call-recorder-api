#include "DeleteMetaFilesResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

DeleteMetaFilesResponse::DeleteMetaFilesResponse(
    const std::string& status,
    const std::string& msg
) : m_status(status), m_msg(msg)
{

}

DeleteMetaFilesResponse& DeleteMetaFilesResponse::operator=(const DeleteMetaFilesResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    return *this;
}

DeleteMetaFilesResponse::~DeleteMetaFilesResponse()
{

}

const std::string& DeleteMetaFilesResponse::status() const
{
    return m_status;
}
const std::string& DeleteMetaFilesResponse::msg() const
{
    return m_msg;
}

DeleteMetaFilesResponse::Builder& DeleteMetaFilesResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
DeleteMetaFilesResponse::Builder& DeleteMetaFilesResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}

DeleteMetaFilesResponsePtr DeleteMetaFilesResponse::Builder::build() const
{
    return std::make_shared<DeleteMetaFilesResponse>(
        m_status,
        m_msg
    );
}

}
}
}
}
