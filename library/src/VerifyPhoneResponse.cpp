#include "VerifyPhoneResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

VerifyPhoneResponse::VerifyPhoneResponse(
    const std::string& status,
    const std::string& phone,
    const std::string& apiKey,
    const std::string& msg
) : m_status(status), m_phone(phone), m_apiKey(apiKey), m_msg(msg)
{

}

VerifyPhoneResponse& VerifyPhoneResponse::operator=(const VerifyPhoneResponse& other)
{
    this->m_status = other.m_status;
    this->m_phone = other.m_phone;
    this->m_apiKey = other.m_apiKey;
    this->m_msg = other.m_msg;
    return *this;
}

VerifyPhoneResponse::~VerifyPhoneResponse()
{

}

const std::string& VerifyPhoneResponse::status() const
{
    return m_status;
}
const std::string& VerifyPhoneResponse::phone() const
{
    return m_phone;
}
const std::string& VerifyPhoneResponse::apiKey() const
{
    return m_apiKey;
}
const std::string& VerifyPhoneResponse::msg() const
{
    return m_msg;
}

VerifyPhoneResponse::Builder& VerifyPhoneResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
VerifyPhoneResponse::Builder& VerifyPhoneResponse::Builder::phone(const std::string& phone)
{
    m_phone = phone;
    return *this;
}
VerifyPhoneResponse::Builder& VerifyPhoneResponse::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
VerifyPhoneResponse::Builder& VerifyPhoneResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}

VerifyPhoneResponsePtr VerifyPhoneResponse::Builder::build() const
{
    return std::make_shared<VerifyPhoneResponse>(
        m_status,
        m_phone,
        m_apiKey,
        m_msg
    );
}

}
}
}
}
