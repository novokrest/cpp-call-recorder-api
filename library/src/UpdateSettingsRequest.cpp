#include "UpdateSettingsRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateSettingsRequest::UpdateSettingsRequest(
    const std::string& apiKey,
    const PlayBeep::Value& playBeep,
    const FilesPermission::Value& filesPermission
) : m_apiKey(apiKey), m_playBeep(playBeep), m_filesPermission(filesPermission)
{

}

UpdateSettingsRequest& UpdateSettingsRequest::operator=(const UpdateSettingsRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_playBeep = other.m_playBeep;
    this->m_filesPermission = other.m_filesPermission;
    return *this;
}

UpdateSettingsRequest::~UpdateSettingsRequest()
{

}

const std::string& UpdateSettingsRequest::apiKey() const
{
    return m_apiKey;
}
const PlayBeep::Value& UpdateSettingsRequest::playBeep() const
{
    return m_playBeep;
}
const FilesPermission::Value& UpdateSettingsRequest::filesPermission() const
{
    return m_filesPermission;
}

UpdateSettingsRequest::Builder& UpdateSettingsRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
UpdateSettingsRequest::Builder& UpdateSettingsRequest::Builder::playBeep(const PlayBeep::Value& playBeep)
{
    m_playBeep = playBeep;
    return *this;
}
UpdateSettingsRequest::Builder& UpdateSettingsRequest::Builder::filesPermission(const FilesPermission::Value& filesPermission)
{
    m_filesPermission = filesPermission;
    return *this;
}

UpdateSettingsRequestPtr UpdateSettingsRequest::Builder::build() const
{
    return std::make_shared<UpdateSettingsRequest>(
        m_apiKey,
        m_playBeep,
        m_filesPermission
    );
}

}
}
}
}
