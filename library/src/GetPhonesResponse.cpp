#include "GetPhonesResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetPhonesResponse::GetPhonesResponse(const std::vector<GetPhonesResponsePhone>& phones)
    : m_phones(phones)
{

}

GetPhonesResponse& GetPhonesResponse::operator=(const GetPhonesResponse& other)
{
    m_phones = other.m_phones;
    return *this;
}

GetPhonesResponse::~GetPhonesResponse()
{

}

std::vector<GetPhonesResponsePhone> GetPhonesResponse::phones() const
{
    return m_phones;
}

GetPhonesResponse::Builder& GetPhonesResponse::Builder::phones(const std::vector<GetPhonesResponsePhone>& phones)
{
    m_phones = phones;
    return *this;
}

GetPhonesResponsePtr GetPhonesResponse::Builder::build() const
{
    return std::make_shared<GetPhonesResponse>(m_phones);
}

}
}
}
}
