#include "BuyCreditsResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

BuyCreditsResponse::BuyCreditsResponse(
    const std::string& status,
    const std::string& msg
) : m_status(status), m_msg(msg)
{

}

BuyCreditsResponse& BuyCreditsResponse::operator=(const BuyCreditsResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    return *this;
}

BuyCreditsResponse::~BuyCreditsResponse()
{

}

const std::string& BuyCreditsResponse::status() const
{
    return m_status;
}
const std::string& BuyCreditsResponse::msg() const
{
    return m_msg;
}

BuyCreditsResponse::Builder& BuyCreditsResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
BuyCreditsResponse::Builder& BuyCreditsResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}

BuyCreditsResponsePtr BuyCreditsResponse::Builder::build() const
{
    return std::make_shared<BuyCreditsResponse>(
        m_status,
        m_msg
    );
}

}
}
}
}
