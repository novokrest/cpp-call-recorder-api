#include "CreateFileRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

CreateFileRequest::CreateFileRequest(
    const std::string& apiKey,
    const std::string& filePath,
    const CreateFileDataPtr& data
) : m_apiKey(apiKey), m_filePath(filePath), m_data(data)
{

}

CreateFileRequest& CreateFileRequest::operator=(const CreateFileRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_filePath = other.m_filePath;
    this->m_data = other.m_data;
    return *this;
}

CreateFileRequest::~CreateFileRequest()
{

}

const std::string& CreateFileRequest::apiKey() const
{
    return m_apiKey;
}
const std::string& CreateFileRequest::filePath() const
{
    return m_filePath;
}
const CreateFileDataPtr& CreateFileRequest::data() const
{
    return m_data;
}

CreateFileRequest::Builder& CreateFileRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
CreateFileRequest::Builder& CreateFileRequest::Builder::filePath(const std::string& filePath)
{
    m_filePath = filePath;
    return *this;
}
CreateFileRequest::Builder& CreateFileRequest::Builder::data(const CreateFileDataPtr& data)
{
    m_data = data;
    return *this;
}

CreateFileRequestPtr CreateFileRequest::Builder::build() const
{
    return std::make_shared<CreateFileRequest>(
        m_apiKey,
        m_filePath,
        m_data
    );
}

}
}
}
}
