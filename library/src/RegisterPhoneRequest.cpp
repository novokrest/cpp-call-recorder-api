#include "RegisterPhoneRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

RegisterPhoneRequest::RegisterPhoneRequest(
    const std::string& token,
    const std::string& phone
) : m_token(token), m_phone(phone)
{

}

RegisterPhoneRequest& RegisterPhoneRequest::operator=(const RegisterPhoneRequest& other)
{
    this->m_token = other.m_token;
    this->m_phone = other.m_phone;
    return *this;
}

RegisterPhoneRequest::~RegisterPhoneRequest()
{

}

const std::string& RegisterPhoneRequest::token() const
{
    return m_token;
}
const std::string& RegisterPhoneRequest::phone() const
{
    return m_phone;
}

RegisterPhoneRequest::Builder& RegisterPhoneRequest::Builder::token(const std::string& token)
{
    m_token = token;
    return *this;
}
RegisterPhoneRequest::Builder& RegisterPhoneRequest::Builder::phone(const std::string& phone)
{
    m_phone = phone;
    return *this;
}

RegisterPhoneRequestPtr RegisterPhoneRequest::Builder::build() const
{
    return std::make_shared<RegisterPhoneRequest>(
        m_token,
        m_phone
    );
}

}
}
}
}
