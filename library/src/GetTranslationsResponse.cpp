#include "GetTranslationsResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetTranslationsResponse::GetTranslationsResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code,
    const std::map<std::string, std::string>& translation
) : m_status(status), m_msg(msg), m_code(code), m_translation(translation)
{

}

GetTranslationsResponse& GetTranslationsResponse::operator=(const GetTranslationsResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    this->m_translation = other.m_translation;
    return *this;
}

GetTranslationsResponse::~GetTranslationsResponse()
{

}

const std::string& GetTranslationsResponse::status() const
{
    return m_status;
}
const std::string& GetTranslationsResponse::msg() const
{
    return m_msg;
}
const std::string& GetTranslationsResponse::code() const
{
    return m_code;
}
const std::map<std::string, std::string>& GetTranslationsResponse::translation() const
{
    return m_translation;
}

GetTranslationsResponse::Builder& GetTranslationsResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
GetTranslationsResponse::Builder& GetTranslationsResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
GetTranslationsResponse::Builder& GetTranslationsResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}
GetTranslationsResponse::Builder& GetTranslationsResponse::Builder::translation(const std::map<std::string, std::string>& translation)
{
    m_translation = translation;
    return *this;
}

GetTranslationsResponsePtr GetTranslationsResponse::Builder::build() const
{
    return std::make_shared<GetTranslationsResponse>(
        m_status,
        m_msg,
        m_code,
        m_translation
    );
}

}
}
}
}
