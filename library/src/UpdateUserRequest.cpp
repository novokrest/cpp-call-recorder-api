#include "UpdateUserRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateUserRequest::UpdateUserRequest(
    const std::string& apiKey,
    const App::Value& app,
    const std::string& timezone
) : m_apiKey(apiKey), m_app(app), m_timezone(timezone)
{

}

UpdateUserRequest& UpdateUserRequest::operator=(const UpdateUserRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_app = other.m_app;
    this->m_timezone = other.m_timezone;
    return *this;
}

UpdateUserRequest::~UpdateUserRequest()
{

}

const std::string& UpdateUserRequest::apiKey() const
{
    return m_apiKey;
}
const App::Value& UpdateUserRequest::app() const
{
    return m_app;
}
const std::string& UpdateUserRequest::timezone() const
{
    return m_timezone;
}

UpdateUserRequest::Builder& UpdateUserRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
UpdateUserRequest::Builder& UpdateUserRequest::Builder::app(const App::Value& app)
{
    m_app = app;
    return *this;
}
UpdateUserRequest::Builder& UpdateUserRequest::Builder::timezone(const std::string& timezone)
{
    m_timezone = timezone;
    return *this;
}

UpdateUserRequestPtr UpdateUserRequest::Builder::build() const
{
    return std::make_shared<UpdateUserRequest>(
        m_apiKey,
        m_app,
        m_timezone
    );
}

}
}
}
}
