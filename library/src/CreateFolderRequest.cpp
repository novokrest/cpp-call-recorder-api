#include "CreateFolderRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

CreateFolderRequest::CreateFolderRequest(
    const std::string& apiKey,
    const std::string& name,
    const std::string& pass
) : m_apiKey(apiKey), m_name(name), m_pass(pass)
{

}

CreateFolderRequest& CreateFolderRequest::operator=(const CreateFolderRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_name = other.m_name;
    this->m_pass = other.m_pass;
    return *this;
}

CreateFolderRequest::~CreateFolderRequest()
{

}

const std::string& CreateFolderRequest::apiKey() const
{
    return m_apiKey;
}
const std::string& CreateFolderRequest::name() const
{
    return m_name;
}
const std::string& CreateFolderRequest::pass() const
{
    return m_pass;
}

CreateFolderRequest::Builder& CreateFolderRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
CreateFolderRequest::Builder& CreateFolderRequest::Builder::name(const std::string& name)
{
    m_name = name;
    return *this;
}
CreateFolderRequest::Builder& CreateFolderRequest::Builder::pass(const std::string& pass)
{
    m_pass = pass;
    return *this;
}

CreateFolderRequestPtr CreateFolderRequest::Builder::build() const
{
    return std::make_shared<CreateFolderRequest>(
        m_apiKey,
        m_name,
        m_pass
    );
}

}
}
}
}
