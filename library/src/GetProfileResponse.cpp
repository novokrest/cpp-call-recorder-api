#include "GetProfileResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetProfileResponse::GetProfileResponse(
    const std::string& status,
    const std::string& code,
    const GetProfileResponseProfilePtr& profile,
    const App::Value& app,
    const std::string& shareUrl,
    const std::string& rateUrl,
    const long& credits,
    const long& creditsTrans
) : m_status(status), m_code(code), m_profile(profile), m_app(app), m_shareUrl(shareUrl), m_rateUrl(rateUrl), m_credits(credits), m_creditsTrans(creditsTrans)
{

}

GetProfileResponse& GetProfileResponse::operator=(const GetProfileResponse& other)
{
    this->m_status = other.m_status;
    this->m_code = other.m_code;
    this->m_profile = other.m_profile;
    this->m_app = other.m_app;
    this->m_shareUrl = other.m_shareUrl;
    this->m_rateUrl = other.m_rateUrl;
    this->m_credits = other.m_credits;
    this->m_creditsTrans = other.m_creditsTrans;
    return *this;
}

GetProfileResponse::~GetProfileResponse()
{

}

const std::string& GetProfileResponse::status() const
{
    return m_status;
}
const std::string& GetProfileResponse::code() const
{
    return m_code;
}
const GetProfileResponseProfilePtr& GetProfileResponse::profile() const
{
    return m_profile;
}
const App::Value& GetProfileResponse::app() const
{
    return m_app;
}
const std::string& GetProfileResponse::shareUrl() const
{
    return m_shareUrl;
}
const std::string& GetProfileResponse::rateUrl() const
{
    return m_rateUrl;
}
const long& GetProfileResponse::credits() const
{
    return m_credits;
}
const long& GetProfileResponse::creditsTrans() const
{
    return m_creditsTrans;
}

GetProfileResponse::Builder& GetProfileResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
GetProfileResponse::Builder& GetProfileResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}
GetProfileResponse::Builder& GetProfileResponse::Builder::profile(const GetProfileResponseProfilePtr& profile)
{
    m_profile = profile;
    return *this;
}
GetProfileResponse::Builder& GetProfileResponse::Builder::app(const App::Value& app)
{
    m_app = app;
    return *this;
}
GetProfileResponse::Builder& GetProfileResponse::Builder::shareUrl(const std::string& shareUrl)
{
    m_shareUrl = shareUrl;
    return *this;
}
GetProfileResponse::Builder& GetProfileResponse::Builder::rateUrl(const std::string& rateUrl)
{
    m_rateUrl = rateUrl;
    return *this;
}
GetProfileResponse::Builder& GetProfileResponse::Builder::credits(const long& credits)
{
    m_credits = credits;
    return *this;
}
GetProfileResponse::Builder& GetProfileResponse::Builder::creditsTrans(const long& creditsTrans)
{
    m_creditsTrans = creditsTrans;
    return *this;
}

GetProfileResponsePtr GetProfileResponse::Builder::build() const
{
    return std::make_shared<GetProfileResponse>(
        m_status,
        m_code,
        m_profile,
        m_app,
        m_shareUrl,
        m_rateUrl,
        m_credits,
        m_creditsTrans
    );
}

}
}
}
}
