#include "NotifyUserRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

NotifyUserRequest::NotifyUserRequest(
    const std::string& apiKey,
    const std::string& title,
    const std::string& body,
    const DeviceType::Value& deviceType
) : m_apiKey(apiKey), m_title(title), m_body(body), m_deviceType(deviceType)
{

}

NotifyUserRequest& NotifyUserRequest::operator=(const NotifyUserRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_title = other.m_title;
    this->m_body = other.m_body;
    this->m_deviceType = other.m_deviceType;
    return *this;
}

NotifyUserRequest::~NotifyUserRequest()
{

}

const std::string& NotifyUserRequest::apiKey() const
{
    return m_apiKey;
}
const std::string& NotifyUserRequest::title() const
{
    return m_title;
}
const std::string& NotifyUserRequest::body() const
{
    return m_body;
}
const DeviceType::Value& NotifyUserRequest::deviceType() const
{
    return m_deviceType;
}

NotifyUserRequest::Builder& NotifyUserRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
NotifyUserRequest::Builder& NotifyUserRequest::Builder::title(const std::string& title)
{
    m_title = title;
    return *this;
}
NotifyUserRequest::Builder& NotifyUserRequest::Builder::body(const std::string& body)
{
    m_body = body;
    return *this;
}
NotifyUserRequest::Builder& NotifyUserRequest::Builder::deviceType(const DeviceType::Value& deviceType)
{
    m_deviceType = deviceType;
    return *this;
}

NotifyUserRequestPtr NotifyUserRequest::Builder::build() const
{
    return std::make_shared<NotifyUserRequest>(
        m_apiKey,
        m_title,
        m_body,
        m_deviceType
    );
}

}
}
}
}
