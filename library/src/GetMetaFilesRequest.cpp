#include "GetMetaFilesRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetMetaFilesRequest::GetMetaFilesRequest(
    const std::string& apiKey,
    const long& parentId
) : m_apiKey(apiKey), m_parentId(parentId)
{

}

GetMetaFilesRequest& GetMetaFilesRequest::operator=(const GetMetaFilesRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_parentId = other.m_parentId;
    return *this;
}

GetMetaFilesRequest::~GetMetaFilesRequest()
{

}

const std::string& GetMetaFilesRequest::apiKey() const
{
    return m_apiKey;
}
const long& GetMetaFilesRequest::parentId() const
{
    return m_parentId;
}

GetMetaFilesRequest::Builder& GetMetaFilesRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
GetMetaFilesRequest::Builder& GetMetaFilesRequest::Builder::parentId(const long& parentId)
{
    m_parentId = parentId;
    return *this;
}

GetMetaFilesRequestPtr GetMetaFilesRequest::Builder::build() const
{
    return std::make_shared<GetMetaFilesRequest>(
        m_apiKey,
        m_parentId
    );
}

}
}
}
}
