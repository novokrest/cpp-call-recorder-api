#include "GetPhonesRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetPhonesRequest::GetPhonesRequest(
    const std::string& apiKey
) : m_apiKey(apiKey)
{

}

GetPhonesRequest& GetPhonesRequest::operator=(const GetPhonesRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    return *this;
}

GetPhonesRequest::~GetPhonesRequest()
{

}

const std::string& GetPhonesRequest::apiKey() const
{
    return m_apiKey;
}

GetPhonesRequest::Builder& GetPhonesRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}

GetPhonesRequestPtr GetPhonesRequest::Builder::build() const
{
    return std::make_shared<GetPhonesRequest>(
        m_apiKey
    );
}

}
}
}
}
