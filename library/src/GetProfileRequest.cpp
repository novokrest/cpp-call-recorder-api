#include "GetProfileRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetProfileRequest::GetProfileRequest(
    const std::string& apiKey
) : m_apiKey(apiKey)
{

}

GetProfileRequest& GetProfileRequest::operator=(const GetProfileRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    return *this;
}

GetProfileRequest::~GetProfileRequest()
{

}

const std::string& GetProfileRequest::apiKey() const
{
    return m_apiKey;
}

GetProfileRequest::Builder& GetProfileRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}

GetProfileRequestPtr GetProfileRequest::Builder::build() const
{
    return std::make_shared<GetProfileRequest>(
        m_apiKey
    );
}

}
}
}
}
