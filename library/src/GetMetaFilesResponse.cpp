#include "GetMetaFilesResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetMetaFilesResponse::GetMetaFilesResponse(
    const std::string& status,
    const std::vector<GetMetaFilesResponseMetaFile>& metaFiles
) : m_status(status), m_metaFiles(metaFiles)
{

}

GetMetaFilesResponse& GetMetaFilesResponse::operator=(const GetMetaFilesResponse& other)
{
    this->m_status = other.m_status;
    this->m_metaFiles = other.m_metaFiles;
    return *this;
}

GetMetaFilesResponse::~GetMetaFilesResponse()
{

}

const std::string& GetMetaFilesResponse::status() const
{
    return m_status;
}
const std::vector<GetMetaFilesResponseMetaFile>& GetMetaFilesResponse::metaFiles() const
{
    return m_metaFiles;
}

GetMetaFilesResponse::Builder& GetMetaFilesResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
GetMetaFilesResponse::Builder& GetMetaFilesResponse::Builder::metaFiles(const std::vector<GetMetaFilesResponseMetaFile>& metaFiles)
{
    m_metaFiles = metaFiles;
    return *this;
}

GetMetaFilesResponsePtr GetMetaFilesResponse::Builder::build() const
{
    return std::make_shared<GetMetaFilesResponse>(
        m_status,
        m_metaFiles
    );
}

}
}
}
}
