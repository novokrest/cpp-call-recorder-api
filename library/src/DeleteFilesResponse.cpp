#include "DeleteFilesResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

DeleteFilesResponse::DeleteFilesResponse(
    const std::string& status,
    const std::string& msg
) : m_status(status), m_msg(msg)
{

}

DeleteFilesResponse& DeleteFilesResponse::operator=(const DeleteFilesResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    return *this;
}

DeleteFilesResponse::~DeleteFilesResponse()
{

}

const std::string& DeleteFilesResponse::status() const
{
    return m_status;
}
const std::string& DeleteFilesResponse::msg() const
{
    return m_msg;
}

DeleteFilesResponse::Builder& DeleteFilesResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
DeleteFilesResponse::Builder& DeleteFilesResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}

DeleteFilesResponsePtr DeleteFilesResponse::Builder::build() const
{
    return std::make_shared<DeleteFilesResponse>(
        m_status,
        m_msg
    );
}

}
}
}
}
