#include "VerifyFolderPassResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

VerifyFolderPassResponse::VerifyFolderPassResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code
) : m_status(status), m_msg(msg), m_code(code)
{

}

VerifyFolderPassResponse& VerifyFolderPassResponse::operator=(const VerifyFolderPassResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    return *this;
}

VerifyFolderPassResponse::~VerifyFolderPassResponse()
{

}

const std::string& VerifyFolderPassResponse::status() const
{
    return m_status;
}
const std::string& VerifyFolderPassResponse::msg() const
{
    return m_msg;
}
const std::string& VerifyFolderPassResponse::code() const
{
    return m_code;
}

VerifyFolderPassResponse::Builder& VerifyFolderPassResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
VerifyFolderPassResponse::Builder& VerifyFolderPassResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
VerifyFolderPassResponse::Builder& VerifyFolderPassResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}

VerifyFolderPassResponsePtr VerifyFolderPassResponse::Builder::build() const
{
    return std::make_shared<VerifyFolderPassResponse>(
        m_status,
        m_msg,
        m_code
    );
}

}
}
}
}
