#include "UpdateSettingsResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateSettingsResponse::UpdateSettingsResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code
) : m_status(status), m_msg(msg), m_code(code)
{

}

UpdateSettingsResponse& UpdateSettingsResponse::operator=(const UpdateSettingsResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    return *this;
}

UpdateSettingsResponse::~UpdateSettingsResponse()
{

}

const std::string& UpdateSettingsResponse::status() const
{
    return m_status;
}
const std::string& UpdateSettingsResponse::msg() const
{
    return m_msg;
}
const std::string& UpdateSettingsResponse::code() const
{
    return m_code;
}

UpdateSettingsResponse::Builder& UpdateSettingsResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
UpdateSettingsResponse::Builder& UpdateSettingsResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
UpdateSettingsResponse::Builder& UpdateSettingsResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}

UpdateSettingsResponsePtr UpdateSettingsResponse::Builder::build() const
{
    return std::make_shared<UpdateSettingsResponse>(
        m_status,
        m_msg,
        m_code
    );
}

}
}
}
}
