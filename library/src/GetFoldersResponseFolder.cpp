#include "GetFoldersResponseFolder.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetFoldersResponseFolder::GetFoldersResponseFolder(
    const std::string& id,
    const std::string& name,
    const std::string& created,
    const std::string& updated,
    const std::string& isStar,
    const std::string& orderId
) : m_id(id), m_name(name), m_created(created), m_updated(updated), m_isStar(isStar), m_orderId(orderId)
{

}

GetFoldersResponseFolder& GetFoldersResponseFolder::operator=(const GetFoldersResponseFolder& other)
{
    this->m_id = other.m_id;
    this->m_name = other.m_name;
    this->m_created = other.m_created;
    this->m_updated = other.m_updated;
    this->m_isStar = other.m_isStar;
    this->m_orderId = other.m_orderId;
    return *this;
}

GetFoldersResponseFolder::~GetFoldersResponseFolder()
{

}

const std::string& GetFoldersResponseFolder::id() const
{
    return m_id;
}
const std::string& GetFoldersResponseFolder::name() const
{
    return m_name;
}
const std::string& GetFoldersResponseFolder::created() const
{
    return m_created;
}
const std::string& GetFoldersResponseFolder::updated() const
{
    return m_updated;
}
const std::string& GetFoldersResponseFolder::isStar() const
{
    return m_isStar;
}
const std::string& GetFoldersResponseFolder::orderId() const
{
    return m_orderId;
}

GetFoldersResponseFolder::Builder& GetFoldersResponseFolder::Builder::id(const std::string& id)
{
    m_id = id;
    return *this;
}
GetFoldersResponseFolder::Builder& GetFoldersResponseFolder::Builder::name(const std::string& name)
{
    m_name = name;
    return *this;
}
GetFoldersResponseFolder::Builder& GetFoldersResponseFolder::Builder::created(const std::string& created)
{
    m_created = created;
    return *this;
}
GetFoldersResponseFolder::Builder& GetFoldersResponseFolder::Builder::updated(const std::string& updated)
{
    m_updated = updated;
    return *this;
}
GetFoldersResponseFolder::Builder& GetFoldersResponseFolder::Builder::isStar(const std::string& isStar)
{
    m_isStar = isStar;
    return *this;
}
GetFoldersResponseFolder::Builder& GetFoldersResponseFolder::Builder::orderId(const std::string& orderId)
{
    m_orderId = orderId;
    return *this;
}

GetFoldersResponseFolderPtr GetFoldersResponseFolder::Builder::build() const
{
    return std::make_shared<GetFoldersResponseFolder>(
        m_id,
        m_name,
        m_created,
        m_updated,
        m_isStar,
        m_orderId
    );
}

}
}
}
}
