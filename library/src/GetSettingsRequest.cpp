#include "GetSettingsRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetSettingsRequest::GetSettingsRequest(
    const std::string& apiKey
) : m_apiKey(apiKey)
{

}

GetSettingsRequest& GetSettingsRequest::operator=(const GetSettingsRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    return *this;
}

GetSettingsRequest::~GetSettingsRequest()
{

}

const std::string& GetSettingsRequest::apiKey() const
{
    return m_apiKey;
}

GetSettingsRequest::Builder& GetSettingsRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}

GetSettingsRequestPtr GetSettingsRequest::Builder::build() const
{
    return std::make_shared<GetSettingsRequest>(
        m_apiKey
    );
}

}
}
}
}
