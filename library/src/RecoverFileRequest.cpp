#include "RecoverFileRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

RecoverFileRequest::RecoverFileRequest(
    const std::string& apiKey,
    const long& id,
    const long& folderId
) : m_apiKey(apiKey), m_id(id), m_folderId(folderId)
{

}

RecoverFileRequest& RecoverFileRequest::operator=(const RecoverFileRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_id = other.m_id;
    this->m_folderId = other.m_folderId;
    return *this;
}

RecoverFileRequest::~RecoverFileRequest()
{

}

const std::string& RecoverFileRequest::apiKey() const
{
    return m_apiKey;
}
const long& RecoverFileRequest::id() const
{
    return m_id;
}
const long& RecoverFileRequest::folderId() const
{
    return m_folderId;
}

RecoverFileRequest::Builder& RecoverFileRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
RecoverFileRequest::Builder& RecoverFileRequest::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
RecoverFileRequest::Builder& RecoverFileRequest::Builder::folderId(const long& folderId)
{
    m_folderId = folderId;
    return *this;
}

RecoverFileRequestPtr RecoverFileRequest::Builder::build() const
{
    return std::make_shared<RecoverFileRequest>(
        m_apiKey,
        m_id,
        m_folderId
    );
}

}
}
}
}
