#include "UpdateOrderResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateOrderResponse::UpdateOrderResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code
) : m_status(status), m_msg(msg), m_code(code)
{

}

UpdateOrderResponse& UpdateOrderResponse::operator=(const UpdateOrderResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    return *this;
}

UpdateOrderResponse::~UpdateOrderResponse()
{

}

const std::string& UpdateOrderResponse::status() const
{
    return m_status;
}
const std::string& UpdateOrderResponse::msg() const
{
    return m_msg;
}
const std::string& UpdateOrderResponse::code() const
{
    return m_code;
}

UpdateOrderResponse::Builder& UpdateOrderResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
UpdateOrderResponse::Builder& UpdateOrderResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
UpdateOrderResponse::Builder& UpdateOrderResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}

UpdateOrderResponsePtr UpdateOrderResponse::Builder::build() const
{
    return std::make_shared<UpdateOrderResponse>(
        m_status,
        m_msg,
        m_code
    );
}

}
}
}
}
