#include "FilesPermission.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

std::string FilesPermission::toString(const FilesPermission::Value value)
{
    const std::map<FilesPermission::Value, std::string> FilesPermissionValueToStringMap {
        {
            FilesPermission::Value::PUBLIC,
            "public"
        },
        {
            FilesPermission::Value::PRIVATE,
            "private"
        },
    };

    auto it = FilesPermissionValueToStringMap.find(value);
    return it->second;
}

FilesPermission::Value FilesPermission::fromString(const std::string& value)
{
    const std::map<std::string, FilesPermission::Value> StringToFilesPermissionValueMap {
        {
            "public",
            FilesPermission::Value::PUBLIC
        },
        {
            "private",
            FilesPermission::Value::PRIVATE
        },
    };

    auto it = StringToFilesPermissionValueMap.find(value);
    return it->second;
}

}
}
}
}
