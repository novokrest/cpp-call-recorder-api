#include "VerifyPhoneRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

VerifyPhoneRequest::VerifyPhoneRequest(
    const std::string& token,
    const std::string& phone,
    const std::string& code,
    const std::string& mcc,
    const App::Value& app,
    const std::string& deviceToken,
    const std::string& deviceId,
    const DeviceType::Value& deviceType,
    const std::string& timeZone
) : m_token(token), m_phone(phone), m_code(code), m_mcc(mcc), m_app(app), m_deviceToken(deviceToken), m_deviceId(deviceId), m_deviceType(deviceType), m_timeZone(timeZone)
{

}

VerifyPhoneRequest& VerifyPhoneRequest::operator=(const VerifyPhoneRequest& other)
{
    this->m_token = other.m_token;
    this->m_phone = other.m_phone;
    this->m_code = other.m_code;
    this->m_mcc = other.m_mcc;
    this->m_app = other.m_app;
    this->m_deviceToken = other.m_deviceToken;
    this->m_deviceId = other.m_deviceId;
    this->m_deviceType = other.m_deviceType;
    this->m_timeZone = other.m_timeZone;
    return *this;
}

VerifyPhoneRequest::~VerifyPhoneRequest()
{

}

const std::string& VerifyPhoneRequest::token() const
{
    return m_token;
}
const std::string& VerifyPhoneRequest::phone() const
{
    return m_phone;
}
const std::string& VerifyPhoneRequest::code() const
{
    return m_code;
}
const std::string& VerifyPhoneRequest::mcc() const
{
    return m_mcc;
}
const App::Value& VerifyPhoneRequest::app() const
{
    return m_app;
}
const std::string& VerifyPhoneRequest::deviceToken() const
{
    return m_deviceToken;
}
const std::string& VerifyPhoneRequest::deviceId() const
{
    return m_deviceId;
}
const DeviceType::Value& VerifyPhoneRequest::deviceType() const
{
    return m_deviceType;
}
const std::string& VerifyPhoneRequest::timeZone() const
{
    return m_timeZone;
}

VerifyPhoneRequest::Builder& VerifyPhoneRequest::Builder::token(const std::string& token)
{
    m_token = token;
    return *this;
}
VerifyPhoneRequest::Builder& VerifyPhoneRequest::Builder::phone(const std::string& phone)
{
    m_phone = phone;
    return *this;
}
VerifyPhoneRequest::Builder& VerifyPhoneRequest::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}
VerifyPhoneRequest::Builder& VerifyPhoneRequest::Builder::mcc(const std::string& mcc)
{
    m_mcc = mcc;
    return *this;
}
VerifyPhoneRequest::Builder& VerifyPhoneRequest::Builder::app(const App::Value& app)
{
    m_app = app;
    return *this;
}
VerifyPhoneRequest::Builder& VerifyPhoneRequest::Builder::deviceToken(const std::string& deviceToken)
{
    m_deviceToken = deviceToken;
    return *this;
}
VerifyPhoneRequest::Builder& VerifyPhoneRequest::Builder::deviceId(const std::string& deviceId)
{
    m_deviceId = deviceId;
    return *this;
}
VerifyPhoneRequest::Builder& VerifyPhoneRequest::Builder::deviceType(const DeviceType::Value& deviceType)
{
    m_deviceType = deviceType;
    return *this;
}
VerifyPhoneRequest::Builder& VerifyPhoneRequest::Builder::timeZone(const std::string& timeZone)
{
    m_timeZone = timeZone;
    return *this;
}

VerifyPhoneRequestPtr VerifyPhoneRequest::Builder::build() const
{
    return std::make_shared<VerifyPhoneRequest>(
        m_token,
        m_phone,
        m_code,
        m_mcc,
        m_app,
        m_deviceToken,
        m_deviceId,
        m_deviceType,
        m_timeZone
    );
}

}
}
}
}
