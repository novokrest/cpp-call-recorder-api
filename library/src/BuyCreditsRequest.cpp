#include "BuyCreditsRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

BuyCreditsRequest::BuyCreditsRequest(
    const std::string& apiKey,
    const long& amount,
    const std::string& receipt,
    const long& productId,
    const DeviceType::Value& deviceType
) : m_apiKey(apiKey), m_amount(amount), m_receipt(receipt), m_productId(productId), m_deviceType(deviceType)
{

}

BuyCreditsRequest& BuyCreditsRequest::operator=(const BuyCreditsRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_amount = other.m_amount;
    this->m_receipt = other.m_receipt;
    this->m_productId = other.m_productId;
    this->m_deviceType = other.m_deviceType;
    return *this;
}

BuyCreditsRequest::~BuyCreditsRequest()
{

}

const std::string& BuyCreditsRequest::apiKey() const
{
    return m_apiKey;
}
const long& BuyCreditsRequest::amount() const
{
    return m_amount;
}
const std::string& BuyCreditsRequest::receipt() const
{
    return m_receipt;
}
const long& BuyCreditsRequest::productId() const
{
    return m_productId;
}
const DeviceType::Value& BuyCreditsRequest::deviceType() const
{
    return m_deviceType;
}

BuyCreditsRequest::Builder& BuyCreditsRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
BuyCreditsRequest::Builder& BuyCreditsRequest::Builder::amount(const long& amount)
{
    m_amount = amount;
    return *this;
}
BuyCreditsRequest::Builder& BuyCreditsRequest::Builder::receipt(const std::string& receipt)
{
    m_receipt = receipt;
    return *this;
}
BuyCreditsRequest::Builder& BuyCreditsRequest::Builder::productId(const long& productId)
{
    m_productId = productId;
    return *this;
}
BuyCreditsRequest::Builder& BuyCreditsRequest::Builder::deviceType(const DeviceType::Value& deviceType)
{
    m_deviceType = deviceType;
    return *this;
}

BuyCreditsRequestPtr BuyCreditsRequest::Builder::build() const
{
    return std::make_shared<BuyCreditsRequest>(
        m_apiKey,
        m_amount,
        m_receipt,
        m_productId,
        m_deviceType
    );
}

}
}
}
}
