#include "UpdateStarResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateStarResponse::UpdateStarResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code
) : m_status(status), m_msg(msg), m_code(code)
{

}

UpdateStarResponse& UpdateStarResponse::operator=(const UpdateStarResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    return *this;
}

UpdateStarResponse::~UpdateStarResponse()
{

}

const std::string& UpdateStarResponse::status() const
{
    return m_status;
}
const std::string& UpdateStarResponse::msg() const
{
    return m_msg;
}
const std::string& UpdateStarResponse::code() const
{
    return m_code;
}

UpdateStarResponse::Builder& UpdateStarResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
UpdateStarResponse::Builder& UpdateStarResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
UpdateStarResponse::Builder& UpdateStarResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}

UpdateStarResponsePtr UpdateStarResponse::Builder::build() const
{
    return std::make_shared<UpdateStarResponse>(
        m_status,
        m_msg,
        m_code
    );
}

}
}
}
}
