#include "UpdateFileResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateFileResponse::UpdateFileResponse(
    const std::string& status,
    const std::string& msg
) : m_status(status), m_msg(msg)
{

}

UpdateFileResponse& UpdateFileResponse::operator=(const UpdateFileResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    return *this;
}

UpdateFileResponse::~UpdateFileResponse()
{

}

const std::string& UpdateFileResponse::status() const
{
    return m_status;
}
const std::string& UpdateFileResponse::msg() const
{
    return m_msg;
}

UpdateFileResponse::Builder& UpdateFileResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
UpdateFileResponse::Builder& UpdateFileResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}

UpdateFileResponsePtr UpdateFileResponse::Builder::build() const
{
    return std::make_shared<UpdateFileResponse>(
        m_status,
        m_msg
    );
}

}
}
}
}
