#include "NotifyUserResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

NotifyUserResponse::NotifyUserResponse(
    const std::string& status,
    const std::string& msg
) : m_status(status), m_msg(msg)
{

}

NotifyUserResponse& NotifyUserResponse::operator=(const NotifyUserResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    return *this;
}

NotifyUserResponse::~NotifyUserResponse()
{

}

const std::string& NotifyUserResponse::status() const
{
    return m_status;
}
const std::string& NotifyUserResponse::msg() const
{
    return m_msg;
}

NotifyUserResponse::Builder& NotifyUserResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
NotifyUserResponse::Builder& NotifyUserResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}

NotifyUserResponsePtr NotifyUserResponse::Builder::build() const
{
    return std::make_shared<NotifyUserResponse>(
        m_status,
        m_msg
    );
}

}
}
}
}
