#include "UploadMetaFileResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UploadMetaFileResponse::UploadMetaFileResponse(
    const std::string& status,
    const std::string& msg,
    const long& parentId,
    const long& id
) : m_status(status), m_msg(msg), m_parentId(parentId), m_id(id)
{

}

UploadMetaFileResponse& UploadMetaFileResponse::operator=(const UploadMetaFileResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_parentId = other.m_parentId;
    this->m_id = other.m_id;
    return *this;
}

UploadMetaFileResponse::~UploadMetaFileResponse()
{

}

const std::string& UploadMetaFileResponse::status() const
{
    return m_status;
}
const std::string& UploadMetaFileResponse::msg() const
{
    return m_msg;
}
const long& UploadMetaFileResponse::parentId() const
{
    return m_parentId;
}
const long& UploadMetaFileResponse::id() const
{
    return m_id;
}

UploadMetaFileResponse::Builder& UploadMetaFileResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
UploadMetaFileResponse::Builder& UploadMetaFileResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
UploadMetaFileResponse::Builder& UploadMetaFileResponse::Builder::parentId(const long& parentId)
{
    m_parentId = parentId;
    return *this;
}
UploadMetaFileResponse::Builder& UploadMetaFileResponse::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}

UploadMetaFileResponsePtr UploadMetaFileResponse::Builder::build() const
{
    return std::make_shared<UploadMetaFileResponse>(
        m_status,
        m_msg,
        m_parentId,
        m_id
    );
}

}
}
}
}
