#include "GetProfileResponseProfile.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetProfileResponseProfile::GetProfileResponseProfile(
    const std::string& fName,
    const std::string& lName,
    const std::string& email,
    const std::string& phone,
    const std::string& pic,
    const std::string& language,
    const int& isPublic,
    const int& playBeep,
    const long& maxLength,
    const std::string& timeZone,
    const long& time,
    const std::string& pin
) : m_fName(fName), m_lName(lName), m_email(email), m_phone(phone), m_pic(pic), m_language(language), m_isPublic(isPublic), m_playBeep(playBeep), m_maxLength(maxLength), m_timeZone(timeZone), m_time(time), m_pin(pin)
{

}

GetProfileResponseProfile& GetProfileResponseProfile::operator=(const GetProfileResponseProfile& other)
{
    this->m_fName = other.m_fName;
    this->m_lName = other.m_lName;
    this->m_email = other.m_email;
    this->m_phone = other.m_phone;
    this->m_pic = other.m_pic;
    this->m_language = other.m_language;
    this->m_isPublic = other.m_isPublic;
    this->m_playBeep = other.m_playBeep;
    this->m_maxLength = other.m_maxLength;
    this->m_timeZone = other.m_timeZone;
    this->m_time = other.m_time;
    this->m_pin = other.m_pin;
    return *this;
}

GetProfileResponseProfile::~GetProfileResponseProfile()
{

}

const std::string& GetProfileResponseProfile::fName() const
{
    return m_fName;
}
const std::string& GetProfileResponseProfile::lName() const
{
    return m_lName;
}
const std::string& GetProfileResponseProfile::email() const
{
    return m_email;
}
const std::string& GetProfileResponseProfile::phone() const
{
    return m_phone;
}
const std::string& GetProfileResponseProfile::pic() const
{
    return m_pic;
}
const std::string& GetProfileResponseProfile::language() const
{
    return m_language;
}
const int& GetProfileResponseProfile::isPublic() const
{
    return m_isPublic;
}
const int& GetProfileResponseProfile::playBeep() const
{
    return m_playBeep;
}
const long& GetProfileResponseProfile::maxLength() const
{
    return m_maxLength;
}
const std::string& GetProfileResponseProfile::timeZone() const
{
    return m_timeZone;
}
const long& GetProfileResponseProfile::time() const
{
    return m_time;
}
const std::string& GetProfileResponseProfile::pin() const
{
    return m_pin;
}

GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::fName(const std::string& fName)
{
    m_fName = fName;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::lName(const std::string& lName)
{
    m_lName = lName;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::email(const std::string& email)
{
    m_email = email;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::phone(const std::string& phone)
{
    m_phone = phone;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::pic(const std::string& pic)
{
    m_pic = pic;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::language(const std::string& language)
{
    m_language = language;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::isPublic(const int& isPublic)
{
    m_isPublic = isPublic;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::playBeep(const int& playBeep)
{
    m_playBeep = playBeep;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::maxLength(const long& maxLength)
{
    m_maxLength = maxLength;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::timeZone(const std::string& timeZone)
{
    m_timeZone = timeZone;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::time(const long& time)
{
    m_time = time;
    return *this;
}
GetProfileResponseProfile::Builder& GetProfileResponseProfile::Builder::pin(const std::string& pin)
{
    m_pin = pin;
    return *this;
}

GetProfileResponseProfilePtr GetProfileResponseProfile::Builder::build() const
{
    return std::make_shared<GetProfileResponseProfile>(
        m_fName,
        m_lName,
        m_email,
        m_phone,
        m_pic,
        m_language,
        m_isPublic,
        m_playBeep,
        m_maxLength,
        m_timeZone,
        m_time,
        m_pin
    );
}

}
}
}
}
