#include "FileUtils.h"

#include <cpprest/json.h>
#include <cpprest/details/basic_types.h>

namespace call {
namespace recorder {
namespace api {
namespace utils {

std::string getFileName(const std::string& filePath);

std::shared_ptr<HttpContent> FileUtils::toHttpContent(const std::string& filePath)
{
    std::ifstream ifs(filePath);
    std::string fileContent(
        (std::istreambuf_iterator<char>(ifs)),
        (std::istreambuf_iterator<char>())
    );
    auto fileName = utility::conversions::to_string_t(getFileName(filePath));
    std::shared_ptr<HttpContent> content(new HttpContent);
    content->setName(fileName);
    content->setFileName(fileName);
    content->setContentDisposition(utility::conversions::to_string_t("form-data"));
    content->setContentType(utility::conversions::to_string_t("application/octet-stream"));
    content->setData( std::shared_ptr<std::istream>(new std::stringstream(utility::conversions::to_utf8string(fileContent))));
    return content;
}

std::string getFileName(const std::string& filePath)
{
    std::string fileName = filePath;

    // Remove directory if present.
    // Do this before extension removal incase directory has a period character.
    const size_t last_slash_idx = fileName.find_last_of("/");
    if (std::string::npos != last_slash_idx)
    {
        fileName.erase(0, last_slash_idx + 1);
    }

    // Remove extension if present.
    const size_t period_idx = fileName.rfind('.');
    if (std::string::npos != period_idx)
    {
        fileName.erase(period_idx);
    }

    return fileName;
}

}
}
}
}

