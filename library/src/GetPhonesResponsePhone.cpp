#include "GetPhonesResponsePhone.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetPhonesResponsePhone::GetPhonesResponsePhone(
    const std::string& phoneNumber,
    const std::string& number,
    const std::string& prefix,
    const std::string& friendlyName,
    const std::string& flag,
    const std::string& city,
    const std::string& country
) : m_phoneNumber(phoneNumber), m_number(number), m_prefix(prefix), m_friendlyName(friendlyName), m_flag(flag), m_city(city), m_country(country)
{

}

GetPhonesResponsePhone& GetPhonesResponsePhone::operator=(const GetPhonesResponsePhone& other)
{
    this->m_phoneNumber = other.m_phoneNumber;
    this->m_number = other.m_number;
    this->m_prefix = other.m_prefix;
    this->m_friendlyName = other.m_friendlyName;
    this->m_flag = other.m_flag;
    this->m_city = other.m_city;
    this->m_country = other.m_country;
    return *this;
}

GetPhonesResponsePhone::~GetPhonesResponsePhone()
{

}

const std::string& GetPhonesResponsePhone::phoneNumber() const
{
    return m_phoneNumber;
}
const std::string& GetPhonesResponsePhone::number() const
{
    return m_number;
}
const std::string& GetPhonesResponsePhone::prefix() const
{
    return m_prefix;
}
const std::string& GetPhonesResponsePhone::friendlyName() const
{
    return m_friendlyName;
}
const std::string& GetPhonesResponsePhone::flag() const
{
    return m_flag;
}
const std::string& GetPhonesResponsePhone::city() const
{
    return m_city;
}
const std::string& GetPhonesResponsePhone::country() const
{
    return m_country;
}

GetPhonesResponsePhone::Builder& GetPhonesResponsePhone::Builder::phoneNumber(const std::string& phoneNumber)
{
    m_phoneNumber = phoneNumber;
    return *this;
}
GetPhonesResponsePhone::Builder& GetPhonesResponsePhone::Builder::number(const std::string& number)
{
    m_number = number;
    return *this;
}
GetPhonesResponsePhone::Builder& GetPhonesResponsePhone::Builder::prefix(const std::string& prefix)
{
    m_prefix = prefix;
    return *this;
}
GetPhonesResponsePhone::Builder& GetPhonesResponsePhone::Builder::friendlyName(const std::string& friendlyName)
{
    m_friendlyName = friendlyName;
    return *this;
}
GetPhonesResponsePhone::Builder& GetPhonesResponsePhone::Builder::flag(const std::string& flag)
{
    m_flag = flag;
    return *this;
}
GetPhonesResponsePhone::Builder& GetPhonesResponsePhone::Builder::city(const std::string& city)
{
    m_city = city;
    return *this;
}
GetPhonesResponsePhone::Builder& GetPhonesResponsePhone::Builder::country(const std::string& country)
{
    m_country = country;
    return *this;
}

GetPhonesResponsePhonePtr GetPhonesResponsePhone::Builder::build() const
{
    return std::make_shared<GetPhonesResponsePhone>(
        m_phoneNumber,
        m_number,
        m_prefix,
        m_friendlyName,
        m_flag,
        m_city,
        m_country
    );
}

}
}
}
}
