#include "UpdateDeviceTokenRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateDeviceTokenRequest::UpdateDeviceTokenRequest(
    const std::string& apiKey,
    const std::string& deviceToken,
    const DeviceType::Value& deviceType
) : m_apiKey(apiKey), m_deviceToken(deviceToken), m_deviceType(deviceType)
{

}

UpdateDeviceTokenRequest& UpdateDeviceTokenRequest::operator=(const UpdateDeviceTokenRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_deviceToken = other.m_deviceToken;
    this->m_deviceType = other.m_deviceType;
    return *this;
}

UpdateDeviceTokenRequest::~UpdateDeviceTokenRequest()
{

}

const std::string& UpdateDeviceTokenRequest::apiKey() const
{
    return m_apiKey;
}
const std::string& UpdateDeviceTokenRequest::deviceToken() const
{
    return m_deviceToken;
}
const DeviceType::Value& UpdateDeviceTokenRequest::deviceType() const
{
    return m_deviceType;
}

UpdateDeviceTokenRequest::Builder& UpdateDeviceTokenRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
UpdateDeviceTokenRequest::Builder& UpdateDeviceTokenRequest::Builder::deviceToken(const std::string& deviceToken)
{
    m_deviceToken = deviceToken;
    return *this;
}
UpdateDeviceTokenRequest::Builder& UpdateDeviceTokenRequest::Builder::deviceType(const DeviceType::Value& deviceType)
{
    m_deviceType = deviceType;
    return *this;
}

UpdateDeviceTokenRequestPtr UpdateDeviceTokenRequest::Builder::build() const
{
    return std::make_shared<UpdateDeviceTokenRequest>(
        m_apiKey,
        m_deviceToken,
        m_deviceType
    );
}

}
}
}
}
