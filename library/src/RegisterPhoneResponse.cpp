#include "RegisterPhoneResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

RegisterPhoneResponse::RegisterPhoneResponse(
    const std::string& status,
    const std::string& phone,
    const std::string& code,
    const std::string& msg
) : m_status(status), m_phone(phone), m_code(code), m_msg(msg)
{

}

RegisterPhoneResponse& RegisterPhoneResponse::operator=(const RegisterPhoneResponse& other)
{
    this->m_status = other.m_status;
    this->m_phone = other.m_phone;
    this->m_code = other.m_code;
    this->m_msg = other.m_msg;
    return *this;
}

RegisterPhoneResponse::~RegisterPhoneResponse()
{

}

const std::string& RegisterPhoneResponse::status() const
{
    return m_status;
}
const std::string& RegisterPhoneResponse::phone() const
{
    return m_phone;
}
const std::string& RegisterPhoneResponse::code() const
{
    return m_code;
}
const std::string& RegisterPhoneResponse::msg() const
{
    return m_msg;
}

RegisterPhoneResponse::Builder& RegisterPhoneResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
RegisterPhoneResponse::Builder& RegisterPhoneResponse::Builder::phone(const std::string& phone)
{
    m_phone = phone;
    return *this;
}
RegisterPhoneResponse::Builder& RegisterPhoneResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}
RegisterPhoneResponse::Builder& RegisterPhoneResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}

RegisterPhoneResponsePtr RegisterPhoneResponse::Builder::build() const
{
    return std::make_shared<RegisterPhoneResponse>(
        m_status,
        m_phone,
        m_code,
        m_msg
    );
}

}
}
}
}
