#include "CreateFileResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

CreateFileResponse::CreateFileResponse(
    const std::string& status,
    const long& id,
    const std::string& msg
) : m_status(status), m_id(id), m_msg(msg)
{

}

CreateFileResponse& CreateFileResponse::operator=(const CreateFileResponse& other)
{
    this->m_status = other.m_status;
    this->m_id = other.m_id;
    this->m_msg = other.m_msg;
    return *this;
}

CreateFileResponse::~CreateFileResponse()
{

}

const std::string& CreateFileResponse::status() const
{
    return m_status;
}
const long& CreateFileResponse::id() const
{
    return m_id;
}
const std::string& CreateFileResponse::msg() const
{
    return m_msg;
}

CreateFileResponse::Builder& CreateFileResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
CreateFileResponse::Builder& CreateFileResponse::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}
CreateFileResponse::Builder& CreateFileResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}

CreateFileResponsePtr CreateFileResponse::Builder::build() const
{
    return std::make_shared<CreateFileResponse>(
        m_status,
        m_id,
        m_msg
    );
}

}
}
}
}
