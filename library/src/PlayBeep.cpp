#include "PlayBeep.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

std::string PlayBeep::toString(const PlayBeep::Value value)
{
    const std::map<PlayBeep::Value, std::string> PlayBeepValueToStringMap {
        {
            PlayBeep::Value::YES,
            "yes"
        },
        {
            PlayBeep::Value::NO,
            "no"
        },
    };

    auto it = PlayBeepValueToStringMap.find(value);
    return it->second;
}

PlayBeep::Value PlayBeep::fromString(const std::string& value)
{
    const std::map<std::string, PlayBeep::Value> StringToPlayBeepValueMap {
        {
            "yes",
            PlayBeep::Value::YES
        },
        {
            "no",
            PlayBeep::Value::NO
        },
    };

    auto it = StringToPlayBeepValueMap.find(value);
    return it->second;
}

}
}
}
}
