#include "UpdateProfileRequestData.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateProfileRequestData::UpdateProfileRequestData(
    const std::string& fName,
    const std::string& lName,
    const std::string& email,
    const bool& isPublic,
    const std::string& language
) : m_fName(fName), m_lName(lName), m_email(email), m_isPublic(isPublic), m_language(language)
{

}

UpdateProfileRequestData& UpdateProfileRequestData::operator=(const UpdateProfileRequestData& other)
{
    this->m_fName = other.m_fName;
    this->m_lName = other.m_lName;
    this->m_email = other.m_email;
    this->m_isPublic = other.m_isPublic;
    this->m_language = other.m_language;
    return *this;
}

UpdateProfileRequestData::~UpdateProfileRequestData()
{

}

const std::string& UpdateProfileRequestData::fName() const
{
    return m_fName;
}
const std::string& UpdateProfileRequestData::lName() const
{
    return m_lName;
}
const std::string& UpdateProfileRequestData::email() const
{
    return m_email;
}
const bool& UpdateProfileRequestData::isPublic() const
{
    return m_isPublic;
}
const std::string& UpdateProfileRequestData::language() const
{
    return m_language;
}

UpdateProfileRequestData::Builder& UpdateProfileRequestData::Builder::fName(const std::string& fName)
{
    m_fName = fName;
    return *this;
}
UpdateProfileRequestData::Builder& UpdateProfileRequestData::Builder::lName(const std::string& lName)
{
    m_lName = lName;
    return *this;
}
UpdateProfileRequestData::Builder& UpdateProfileRequestData::Builder::email(const std::string& email)
{
    m_email = email;
    return *this;
}
UpdateProfileRequestData::Builder& UpdateProfileRequestData::Builder::isPublic(const bool& isPublic)
{
    m_isPublic = isPublic;
    return *this;
}
UpdateProfileRequestData::Builder& UpdateProfileRequestData::Builder::language(const std::string& language)
{
    m_language = language;
    return *this;
}

UpdateProfileRequestDataPtr UpdateProfileRequestData::Builder::build() const
{
    return std::make_shared<UpdateProfileRequestData>(
        m_fName,
        m_lName,
        m_email,
        m_isPublic,
        m_language
    );
}

}
}
}
}
