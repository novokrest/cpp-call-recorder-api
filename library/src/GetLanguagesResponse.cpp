#include "GetLanguagesResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetLanguagesResponse::GetLanguagesResponse(
    const std::string& status,
    const std::string& msg,
    const std::vector<Language>& languages
) : m_status(status), m_msg(msg), m_languages(languages)
{

}

GetLanguagesResponse& GetLanguagesResponse::operator=(const GetLanguagesResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_languages = other.m_languages;
    return *this;
}

GetLanguagesResponse::~GetLanguagesResponse()
{

}

const std::string& GetLanguagesResponse::status() const
{
    return m_status;
}
const std::string& GetLanguagesResponse::msg() const
{
    return m_msg;
}
const std::vector<Language>& GetLanguagesResponse::languages() const
{
    return m_languages;
}

GetLanguagesResponse::Builder& GetLanguagesResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
GetLanguagesResponse::Builder& GetLanguagesResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
GetLanguagesResponse::Builder& GetLanguagesResponse::Builder::languages(const std::vector<Language>& languages)
{
    m_languages = languages;
    return *this;
}

GetLanguagesResponsePtr GetLanguagesResponse::Builder::build() const
{
    return std::make_shared<GetLanguagesResponse>(
        m_status,
        m_msg,
        m_languages
    );
}

}
}
}
}
