#include "GetMessagesResponseMsg.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetMessagesResponseMsg::GetMessagesResponseMsg(
    const std::string& id,
    const std::string& title,
    const std::string& body,
    const std::string& time
) : m_id(id), m_title(title), m_body(body), m_time(time)
{

}

GetMessagesResponseMsg& GetMessagesResponseMsg::operator=(const GetMessagesResponseMsg& other)
{
    this->m_id = other.m_id;
    this->m_title = other.m_title;
    this->m_body = other.m_body;
    this->m_time = other.m_time;
    return *this;
}

GetMessagesResponseMsg::~GetMessagesResponseMsg()
{

}

const std::string& GetMessagesResponseMsg::id() const
{
    return m_id;
}
const std::string& GetMessagesResponseMsg::title() const
{
    return m_title;
}
const std::string& GetMessagesResponseMsg::body() const
{
    return m_body;
}
const std::string& GetMessagesResponseMsg::time() const
{
    return m_time;
}

GetMessagesResponseMsg::Builder& GetMessagesResponseMsg::Builder::id(const std::string& id)
{
    m_id = id;
    return *this;
}
GetMessagesResponseMsg::Builder& GetMessagesResponseMsg::Builder::title(const std::string& title)
{
    m_title = title;
    return *this;
}
GetMessagesResponseMsg::Builder& GetMessagesResponseMsg::Builder::body(const std::string& body)
{
    m_body = body;
    return *this;
}
GetMessagesResponseMsg::Builder& GetMessagesResponseMsg::Builder::time(const std::string& time)
{
    m_time = time;
    return *this;
}

GetMessagesResponseMsgPtr GetMessagesResponseMsg::Builder::build() const
{
    return std::make_shared<GetMessagesResponseMsg>(
        m_id,
        m_title,
        m_body,
        m_time
    );
}

}
}
}
}
