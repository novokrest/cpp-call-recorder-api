#include "UpdateProfileResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateProfileResponse::UpdateProfileResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code
) : m_status(status), m_msg(msg), m_code(code)
{

}

UpdateProfileResponse& UpdateProfileResponse::operator=(const UpdateProfileResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    return *this;
}

UpdateProfileResponse::~UpdateProfileResponse()
{

}

const std::string& UpdateProfileResponse::status() const
{
    return m_status;
}
const std::string& UpdateProfileResponse::msg() const
{
    return m_msg;
}
const std::string& UpdateProfileResponse::code() const
{
    return m_code;
}

UpdateProfileResponse::Builder& UpdateProfileResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
UpdateProfileResponse::Builder& UpdateProfileResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
UpdateProfileResponse::Builder& UpdateProfileResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}

UpdateProfileResponsePtr UpdateProfileResponse::Builder::build() const
{
    return std::make_shared<UpdateProfileResponse>(
        m_status,
        m_msg,
        m_code
    );
}

}
}
}
}
