#include "CloneFileRequest.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

CloneFileRequest::CloneFileRequest(
    const std::string& apiKey,
    const long& id
) : m_apiKey(apiKey), m_id(id)
{

}

CloneFileRequest& CloneFileRequest::operator=(const CloneFileRequest& other)
{
    this->m_apiKey = other.m_apiKey;
    this->m_id = other.m_id;
    return *this;
}

CloneFileRequest::~CloneFileRequest()
{

}

const std::string& CloneFileRequest::apiKey() const
{
    return m_apiKey;
}
const long& CloneFileRequest::id() const
{
    return m_id;
}

CloneFileRequest::Builder& CloneFileRequest::Builder::apiKey(const std::string& apiKey)
{
    m_apiKey = apiKey;
    return *this;
}
CloneFileRequest::Builder& CloneFileRequest::Builder::id(const long& id)
{
    m_id = id;
    return *this;
}

CloneFileRequestPtr CloneFileRequest::Builder::build() const
{
    return std::make_shared<CloneFileRequest>(
        m_apiKey,
        m_id
    );
}

}
}
}
}
