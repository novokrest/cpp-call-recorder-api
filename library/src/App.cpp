#include "App.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

std::string App::toString(const App::Value value)
{
    const std::map<App::Value, std::string> AppValueToStringMap {
        {
            App::Value::FREE,
            "free"
        },
        {
            App::Value::REM,
            "rem"
        },
        {
            App::Value::REC,
            "rec"
        },
    };

    auto it = AppValueToStringMap.find(value);
    return it->second;
}

App::Value App::fromString(const std::string& value)
{
    const std::map<std::string, App::Value> StringToAppValueMap {
        {
            "free",
            App::Value::FREE
        },
        {
            "rem",
            App::Value::REM
        },
        {
            "rec",
            App::Value::REC
        },
    };

    auto it = StringToAppValueMap.find(value);
    return it->second;
}

}
}
}
}
