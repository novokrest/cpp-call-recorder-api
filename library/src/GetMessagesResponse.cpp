#include "GetMessagesResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

GetMessagesResponse::GetMessagesResponse(
    const std::string& status,
    const std::vector<GetMessagesResponseMsg>& msgs
) : m_status(status), m_msgs(msgs)
{

}

GetMessagesResponse& GetMessagesResponse::operator=(const GetMessagesResponse& other)
{
    this->m_status = other.m_status;
    this->m_msgs = other.m_msgs;
    return *this;
}

GetMessagesResponse::~GetMessagesResponse()
{

}

const std::string& GetMessagesResponse::status() const
{
    return m_status;
}
const std::vector<GetMessagesResponseMsg>& GetMessagesResponse::msgs() const
{
    return m_msgs;
}

GetMessagesResponse::Builder& GetMessagesResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
GetMessagesResponse::Builder& GetMessagesResponse::Builder::msgs(const std::vector<GetMessagesResponseMsg>& msgs)
{
    m_msgs = msgs;
    return *this;
}

GetMessagesResponsePtr GetMessagesResponse::Builder::build() const
{
    return std::make_shared<GetMessagesResponse>(
        m_status,
        m_msgs
    );
}

}
}
}
}
