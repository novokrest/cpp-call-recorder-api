#include "UpdateFolderResponse.h"

namespace call {
namespace recorder {
namespace api {
namespace models {

UpdateFolderResponse::UpdateFolderResponse(
    const std::string& status,
    const std::string& msg,
    const std::string& code
) : m_status(status), m_msg(msg), m_code(code)
{

}

UpdateFolderResponse& UpdateFolderResponse::operator=(const UpdateFolderResponse& other)
{
    this->m_status = other.m_status;
    this->m_msg = other.m_msg;
    this->m_code = other.m_code;
    return *this;
}

UpdateFolderResponse::~UpdateFolderResponse()
{

}

const std::string& UpdateFolderResponse::status() const
{
    return m_status;
}
const std::string& UpdateFolderResponse::msg() const
{
    return m_msg;
}
const std::string& UpdateFolderResponse::code() const
{
    return m_code;
}

UpdateFolderResponse::Builder& UpdateFolderResponse::Builder::status(const std::string& status)
{
    m_status = status;
    return *this;
}
UpdateFolderResponse::Builder& UpdateFolderResponse::Builder::msg(const std::string& msg)
{
    m_msg = msg;
    return *this;
}
UpdateFolderResponse::Builder& UpdateFolderResponse::Builder::code(const std::string& code)
{
    m_code = code;
    return *this;
}

UpdateFolderResponsePtr UpdateFolderResponse::Builder::build() const
{
    return std::make_shared<UpdateFolderResponse>(
        m_status,
        m_msg,
        m_code
    );
}

}
}
}
}
