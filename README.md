# cpp-call-recorder-api

C++ library to use [Call Recorder API](docs/api.pdf)

### Build
```bash
./build.sh
```

The result of build is a library `libCallRecorderApi.a` in `bin/` directory.

### Usage
```java
CallRecorderApiPtr api = CallRecorderApi::create("https://app2.virtualbrix.net");

RegisterPhoneRequestPtr request = RegisterPhoneRequest::Builder{}
    .token(API_TOKEN)
    .phone(PHONE_NUMBER)
    .build();

RegisterPhoneResponsePtr registerPhoneResponse = api->registerPhone(*request);
std::string code = registerPhoneResponse->code();
std::cout << "Verification code: " << code << std::endl;
```

