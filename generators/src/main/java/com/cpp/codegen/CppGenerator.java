package com.cpp.codegen;

import io.swagger.codegen.v3.*;
import io.swagger.codegen.v3.generators.DefaultCodegenConfig;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.MapSchema;
import io.swagger.v3.oas.models.media.ObjectSchema;
import io.swagger.v3.oas.models.media.Schema;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.io.File;
import java.util.regex.Pattern;

public class CppGenerator extends DefaultCodegenConfig {

  private static final Logger LOGGER = LoggerFactory.getLogger(CppGenerator.class);

  // source folder where to write the files
  protected String sourceFolder = "src";
  protected String apiVersion = "1.0.0";

  private Map<String, Object> models;

  /**
   * Configures the type of generator.
   *
   * @return  the CodegenType for this generator
   * @see     io.swagger.codegen.CodegenType
   */
  public CodegenType getTag() {
    return CodegenType.CLIENT;
  }

  /**
   * Configures a friendly name for the generator.  This will be used by the generator
   * to select the library with the -l flag.
   *
   * @return the friendly name for the generator
   */
  public String getName() {
    return "cpp";
  }

  /**
   * Returns human-friendly help for the generator.  Provide the consumer with help
   * tips, parameters here
   *
   * @return A string value for the help message
   */
  public String getHelp() {
    return "Generates a cpp client library.";
  }

  public CppGenerator() {
    super();

    supportsInheritance = true;

    setReservedWordsLowerCase(
        Arrays.asList(
            // used as internal variables, can collide with parameter names
            "localVarPath", "localVarQueryParams", "localVarCollectionQueryParams",
            "localVarHeaderParams", "localVarFormParams", "localVarPostBody",
            "localVarAccepts", "localVarAccept", "localVarContentTypes",
            "localVarContentType", "localVarAuthNames", "localReturnType",
            "ApiClient", "ApiException", "ApiResponse", "Configuration", "StringUtil",

            // language reserved words
            "continue", "for", "new", "switch", "assert",
            "default", "if", "boolean", "do", "goto", "private",
            "this", "break", "double", "protected", "throw", "byte", "else",
            "import", "public", "case", "enum", "return",
            "catch", "int", "short", "try", "char", "final", "static",
            "void", "class", "finally", "long", "struct", "const", "float",
            "while", "null")
    );

    languageSpecificPrimitives = new HashSet<>(
        Arrays.asList(
            "string",
            "boolean",
            "bool",
            "double",
            "integer",
            "int",
            "long",
            "float",
            "array"
        )
    );

    instantiationTypes.put("array", "std::vector");
    instantiationTypes.put("map", "std::map");

    typeMapping.put("date", "std::string");
    typeMapping.put("Date", "std::string");
    typeMapping.put("date-time", "std::string");
    typeMapping.put("file", "std::vector<char>");
    typeMapping.put("binary", "std::vector<char>");
    typeMapping.put("array", "std::vector");
    typeMapping.put("map", "std::map");
    typeMapping.put("string", "std::string");
    typeMapping.put("long", "long");
    typeMapping.put("boolean", "bool");
    typeMapping.put("integer", "int");

    modelTemplateFiles.put("model.h.mustache", ".h");
    modelTemplateFiles.put("model.cpp.mustache", ".cpp");

    apiTemplateFiles.put("api.h.mustache", ".h");
    apiTemplateFiles.put("api.cpp.mustache", ".cpp");
    apiTemplateFiles.put("api.tmp.mustache", ".tmp");
    apiTemplateFiles.put("test.cpp.mustache", ".test");

    templateDir = "cpp";

    outputFolder = "generated-code/cpp";

  }

  @Override
  public void processOpts() {
    super.processOpts();
    importMapping.remove("Date");
  }

  /**
   * Escapes a reserved word as defined in the `reservedWords` array. Handle escaping
   * those terms here.  This logic is only called if a variable matches the reserved words
   *
   * @return the escaped term
   */
  @Override
  public String escapeReservedWord(String name) {
    if(this.reservedWordsMappings().containsKey(name)) {
      return this.reservedWordsMappings().get(name);
    }
    return "_" + name;
  }

  /**
   * Location to write model files.  You can use the modelPackage() as defined when the class is
   * instantiated
   */
  public String modelFileFolder() {
    return outputFolder + "/" + sourceFolder + "/" + modelPackage().replace('.', File.separatorChar);
  }

  /**
   * Location to write api files.  You can use the apiPackage() as defined when the class is
   * instantiated
   */
  @Override
  public String apiFileFolder() {
    return outputFolder + "/" + sourceFolder + "/" + apiPackage().replace('.', File.separatorChar);
  }

  @Override
  public String getArgumentsLocation() {
    return null;
  }

  @Override
  public String getDefaultTemplateDir() {
    return templateDir;
  }

  @Override
  public String toVarName(String name) {
    // sanitize name
    name = sanitizeVarName(name); // FIXME: a parameter should not be assigned. Also declare the methods parameters as 'final'.

    if (name.toLowerCase().matches("^_*class$")) {
      return "propertyClass";
    }

    if("_".equals(name)) {
      name = "_u";
    }

    // if it's all uppper case, do nothing
    if (name.matches("^[A-Z_]*$")) {
      return name;
    }

    if(startsWithTwoUppercaseLetters(name)){
      name = name.substring(0, 2).toLowerCase() + name.substring(2);
    }

    // camelize (lower first character) the variable name
    // pet_id => petId
    name = camelizeVarName(name, true);

    // for reserved word or word starting with number, append _
    if (isReservedWord(name) || name.matches("^\\d.*")) {
      name = escapeReservedWord(name);
    }

    return name;
  }

  public String sanitizeVarName(String name) {
    if (name == null) {
      LOGGER.warn("String to be sanitized is null. Default to " + Object.class.getSimpleName());
      return Object.class.getSimpleName();
    }
    if ("$".equals(name)) {
      return "value";
    }
    name = name.replaceAll("\\[\\]", StringUtils.EMPTY);
    name = name.replaceAll("\\[", "_")
        .replaceAll("\\]", "")
        .replaceAll("\\(", "_")
        .replaceAll("\\)", StringUtils.EMPTY)
        .replaceAll("\\.", "_")
        .replaceAll("@", "_at_")
        .replaceAll("-", "_")
        .replaceAll(" ", "_");

    // remove everything else other than word, number and _
    // $php_variable => php_variable
    if (allowUnicodeIdentifiers) { //could be converted to a single line with ?: operator
      name = Pattern.compile("\\W-[\\$]", Pattern.UNICODE_CHARACTER_CLASS).matcher(name).replaceAll(StringUtils.EMPTY);
    }
    else {
      name = name.replaceAll("\\W-[\\$]", StringUtils.EMPTY);
    }
    return name;
  }

  public String camelizeVarName(String word, boolean lowercaseFirstLetter) {
    if (word.startsWith("_") && word.length() > 1 && !word.equals("_u") ) {
      word = "_" + DefaultCodegenConfig.camelize(word, lowercaseFirstLetter);
    } else {
      word  = DefaultCodegenConfig.camelize(word, lowercaseFirstLetter);
    }

    if (!word.startsWith("$") || word.length() <= 1) {
      return word;
    }
    final String letter = String.valueOf(word.charAt(1));
    if (!StringUtils.isAllUpperCase(letter)) {
      return word;
    }
    word = word.replaceFirst(letter, letter.toLowerCase());
    return word;
  }

  private boolean startsWithTwoUppercaseLetters(String name) {
    boolean startsWithTwoUppercaseLetters = false;
    if(name.length() > 1) {
      startsWithTwoUppercaseLetters = name.substring(0, 2).equals(name.substring(0, 2).toUpperCase());
    }
    return startsWithTwoUppercaseLetters;
  }

  @Override
  public Map<String, Object> postProcessModels(Map<String, Object> objs) {
    return postProcessModelsEnum(objs);
  }

  @Override
  public String getTypeDeclaration(Schema propertySchema) {
    if (propertySchema instanceof ArraySchema) {
      ArraySchema arraySchema = (ArraySchema) propertySchema;
      Schema inner = arraySchema.getItems();
      if (inner == null) {
        LOGGER.warn(arraySchema.getName() + "(array property) does not have a proper inner type defined");
        // TODO maybe better defaulting to StringProperty than returning null
        return null;
      }
      return String.format("%s<%s>", getSchemaType(propertySchema), getTypeDeclaration(inner));
      // return getSwaggerType(propertySchema) + "<" + getTypeDeclaration(inner) + ">";
    } else if (propertySchema instanceof MapSchema && hasSchemaProperties(propertySchema)) {
      Schema inner = (Schema) propertySchema.getAdditionalProperties();
      if (inner == null) {
        LOGGER.warn(propertySchema.getName() + "(map property) does not have a proper inner type defined");
        // TODO maybe better defaulting to StringProperty than returning null
        return null;
      }
      return getSchemaType(propertySchema) + "<std::string, " + getTypeDeclaration(inner) + ">";
    } else if (propertySchema instanceof MapSchema && hasTrueAdditionalProperties(propertySchema)) {
      Schema inner = new ObjectSchema();
      return getSchemaType(propertySchema) + "<std::string, " + getTypeDeclaration(inner) + ">";
    }
    return super.getTypeDeclaration(propertySchema);
  }

  @Override
  public String getSchemaType(Schema schema) {
    String schemaType = super.getSchemaType(schema);

    schemaType = getAlias(schemaType);

    // don't apply renaming on types from the typeMapping
    if (typeMapping.containsKey(schemaType)) {
      return typeMapping.get(schemaType);
    }

    if (null == schemaType) {
      if (schema.getName() != null) {
        LOGGER.warn("No Type defined for Property " + schema.getName());
      } else {
        // LOGGER.error("No Type defined.", new Exception());
      }
    }
    return toModelName(schemaType);
  }

  @Override
  public String getAlias(String name) {
    if (typeAliases != null && typeAliases.containsKey(name)) {
      return typeAliases.get(name);
    }
    return name;
  }

  @Override
  public String toModelName(final String name) {
    // We need to check if import-mapping has a different model for this class, so we use it
    // instead of the auto-generated one.
    if (importMapping.containsKey(name)) {
      return importMapping.get(name);
    }

    final String sanitizedName = sanitizeName(name);

    String nameWithPrefixSuffix = sanitizedName;
    if (!StringUtils.isEmpty(modelNamePrefix)) {
      // add '_' so that model name can be camelized correctly
      nameWithPrefixSuffix = modelNamePrefix + "_" + nameWithPrefixSuffix;
    }

    if (!StringUtils.isEmpty(modelNameSuffix)) {
      // add '_' so that model name can be camelized correctly
      nameWithPrefixSuffix = nameWithPrefixSuffix + "_" + modelNameSuffix;
    }

    // camelize the model name
    // phone_number => PhoneNumber
    final String camelizedName = camelize(nameWithPrefixSuffix);

    // model name cannot use reserved keyword, e.g. return
    if (isReservedWord(camelizedName)) {
      final String modelName = "Model" + camelizedName;
      LOGGER.warn(camelizedName + " (reserved word) cannot be used as model name. Renamed to " + modelName);
      return modelName;
    }

    // model name starts with number
    if (camelizedName.matches("^\\d.*")) {
      final String modelName = "Model" + camelizedName; // e.g. 200Response => Model200Response (after camelize)
      LOGGER.warn(name + " (model name starts with number) cannot be used as model name. Renamed to " + modelName);
      return modelName;
    }

    return camelizedName;
  }

  @Override
  public CodegenModel fromModel(String name, Schema schema, Map<String, Schema> allDefinitions) {
    CodegenModel model = super.fromModel(name, schema, allDefinitions);
    model.imports.remove("Date");
    model.imports.remove("std::vector");
    model.imports.remove("std::string");
    model.imports.remove("std::vector<char>");
    model.imports.remove("std::map");
    return model;
  }

  @Override
  public Map<String, Object> postProcessAllModels(Map<String, Object> processedModels) {
    models = super.postProcessAllModels(processedModels);
    LOGGER.warn("AA2: {}", models);
    return models;
  }

  @Override
  public String toApiName(String name) {
    return "CallRecorderApi";
  }

  @Override
  public Map<String, Object> postProcessOperations(Map<String, Object> objs) {
    Map<String, Object> res = super.postProcessOperations(objs);
    List<Map<String, Object>> imports = (List<Map<String, Object>>) res.get("imports");
    Arrays.stream((new String[]{
        "Date",
        "std::string",
        "std::vector",
        "std::vector<char>"
    })).forEach(excludedImport ->
        imports.removeIf(importInfo -> importInfo.get("import").equals(excludedImport))
    );

    Map<String, Object> operations = (Map<String, Object>) res.get("operations");
    LOGGER.warn("OPS: {}", operations);

    List<CodegenOperation> operationList = (List<CodegenOperation>) operations.get("operation");
    operationList.forEach(operation -> LOGGER.warn("OP: {}", operation));

    operationList.forEach(operation -> {
      Object responseModel = ((List<Map<String, Object>>) ((Map<String, Object>) models.get(operation.returnBaseType)).get("models")).get(0).get("model");
      LOGGER.warn("AAA1: {}, {}, {}", operation.returnBaseType, responseModel.getClass(), responseModel);
      operation.vendorExtensions.put("x-response", responseModel);

      String nameInCamelCase = camelize(operation.path, true);
      String nameInPascalCase = nameInCamelCase.substring(0, 1).toUpperCase() + nameInCamelCase.substring(1);

      String requestModelName = nameInPascalCase + "Request";
      Map<String, Object> requestModels = (Map<String, Object>) models.get(requestModelName);
      if (requestModels != null) {
        Object requestModel = ((List<Map<String, Object>>) requestModels.get("models")).get(0).get("model");
        LOGGER.warn("AAA1: {}, {}, {}", requestModelName, requestModel.getClass(), requestModel);
        operation.vendorExtensions.put("x-request", requestModel);
      }
    });

    return res;
  }

  @Override
  public CodegenOperation fromOperation(String path, String httpMethod, Operation operation, Map<String, Schema> schemas, OpenAPI openAPI) {
    CodegenOperation codegenOperation = super.fromOperation(path, httpMethod, operation, schemas, openAPI);
    String nameInCamelCase = camelize(path, true);
    String nameInPascalCase = nameInCamelCase.substring(0, 1).toUpperCase() + nameInCamelCase.substring(1);
    codegenOperation.vendorExtensions.put("x-name-in-camel-case", nameInCamelCase);
    codegenOperation.vendorExtensions.put("x-name-in-pascal-case", nameInPascalCase);
    return codegenOperation;
  }

}