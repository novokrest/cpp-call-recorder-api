#!/bin/bash

set -x

sed -i -e "s/io.swagger.client.model.//g" tmp/src/io/swagger/client/model/*.h
cp tmp/src/*.h ../library/include
cp tmp/src/*.cpp ../library/src
mv ../library/include/CallRecorderApi.h ../library/include/api/CallRecorderApi.h
mv ../library/src/CallRecorderApi.cpp ../library/src/api/CallRecorderApi.cpp

