sed -i -e "s/&lt;/</g" tmp/src/*
sed -i -e "s/&gt;/>/g" tmp/src/*
sed -i -e "s/&quot;/\"/g" tmp/src/*
sed -i -e "s/Integer/int/g" tmp/src/*
sed -i -e "s/GetMsgsRequest/GetMessagesRequest/g" tmp/src/CallRecorderApi.*
sed -i -e "s/GetMsgsResponse/GetMessagesResponse/g" tmp/src/CallRecorderApi.*
sed -i -e "s/NotifyUserCustomRequest/NotifyUserRequest/g" tmp/src/CallRecorderApi.*
sed -i -e "s/NotifyUserCustomResponse/NotifyUserResponse/g" tmp/src/CallRecorderApi.*
rm -f tmp/src/*-e
