rm -rf tmp
java \
    -DdebugOperations \
   -DgenerateModels=true \
   -DgenerateApis=true \
    -cp ../tools/swagger-codegen-cli.jar:target/cpp-swagger-codegen-1.0.0.jar io.swagger.codegen.v3.cli.SwaggerCodegen generate -l cpp -i ../docs/spec.yml -o ./tmp

