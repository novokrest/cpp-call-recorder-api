#!/bin/bash

SWAGGER_CODEGEN_JAR=tools/swagger-codegen-cli.jar
OUTPUT_DIR=generated

set -x
rm -rf $OUTPUT_DIR
java -jar $SWAGGER_CODEGEN_JAR generate \
  -i docs/spec.yml \
  -l c \
  -o $OUTPUT_DIR \
  -c config.json
